/*
 * Reachable_workspace_relaxed.cpp
 *
 *  Created on: May 2, 2019
 *      Author: jpickard
 */

#include "ibex.h"
#include "vibes.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <utility>
#include <ctime>
#include <stdlib.h>
#include <sstream>
#include <chrono>

/**
 * My own linear relaxation of a system
 */
class LinearRelax : public ibex::Linearizer {
public:
	/**
	 * We actually only accept linear systems Ax<=b :)
	 */
	LinearRelax(const ibex::Matrix& A, const ibex::Vector& b) : Linearizer(2), A(A), b(b) { }

	virtual int linearize(const ibex::IntervalVector & box, ibex::LPSolver& lp_solver)  {
		for (int i=0; i<A.nb_rows(); i++)
			// add the constraint in the LP solver
			lp_solver.add_constraint(A[i],ibex::LEQ,b[i]);

		// we return the number of constraints
		return A.nb_rows();
	}

	ibex::Matrix A;
	ibex::Vector b;
};

int main(int argc, char **argv) {

	// Verbose flag (0: warnings only, 1: general messages, 2: more information)
	int verbose = 1;

	// Clear directory for new list
	int dir_clean = system("exec rm -r manifold/list/*");

	// File for exporting solution results
	std::string fileExport("manifold/workspace.txt");
	// Clear fileExport
	std::ofstream ofs;
	ofs.open(fileExport, std::ofstream::out | std::ofstream::trunc);
	ofs.close();

	/* ################################################
	GENERATE THE EXPRESSIONS
	################################################ */
	ibex::Array<const ibex::ExprSymbol> args;
	const ibex::ExprSymbol& Rux1=ibex::ExprSymbol::new_(); args.add(Rux1);
	const ibex::ExprSymbol& Rux2=ibex::ExprSymbol::new_(); args.add(Rux2);
	const ibex::ExprSymbol& Rux3=ibex::ExprSymbol::new_(); args.add(Rux3);
	const ibex::ExprSymbol& Rux4=ibex::ExprSymbol::new_(); args.add(Rux4);
	const ibex::ExprSymbol& Rux5=ibex::ExprSymbol::new_(); args.add(Rux5);
	const ibex::ExprSymbol& Rux6=ibex::ExprSymbol::new_(); args.add(Rux6);
	const ibex::ExprSymbol& Rux7=ibex::ExprSymbol::new_(); args.add(Rux7);
	const ibex::ExprSymbol& Rux8=ibex::ExprSymbol::new_(); args.add(Rux8);

	const ibex::ExprSymbol& Ruy1=ibex::ExprSymbol::new_(); args.add(Ruy1);
	const ibex::ExprSymbol& Ruy2=ibex::ExprSymbol::new_(); args.add(Ruy2);
	const ibex::ExprSymbol& Ruy3=ibex::ExprSymbol::new_(); args.add(Ruy3);
	const ibex::ExprSymbol& Ruy4=ibex::ExprSymbol::new_(); args.add(Ruy4);
	const ibex::ExprSymbol& Ruy5=ibex::ExprSymbol::new_(); args.add(Ruy5);
	const ibex::ExprSymbol& Ruy6=ibex::ExprSymbol::new_(); args.add(Ruy6);
	const ibex::ExprSymbol& Ruy7=ibex::ExprSymbol::new_(); args.add(Ruy7);
	const ibex::ExprSymbol& Ruy8=ibex::ExprSymbol::new_(); args.add(Ruy8);

	const ibex::ExprSymbol& Ruz1=ibex::ExprSymbol::new_(); args.add(Ruz1);
	const ibex::ExprSymbol& Ruz2=ibex::ExprSymbol::new_(); args.add(Ruz2);
	const ibex::ExprSymbol& Ruz3=ibex::ExprSymbol::new_(); args.add(Ruz3);
	const ibex::ExprSymbol& Ruz4=ibex::ExprSymbol::new_(); args.add(Ruz4);
	const ibex::ExprSymbol& Ruz5=ibex::ExprSymbol::new_(); args.add(Ruz5);
	const ibex::ExprSymbol& Ruz6=ibex::ExprSymbol::new_(); args.add(Ruz6);
	const ibex::ExprSymbol& Ruz7=ibex::ExprSymbol::new_(); args.add(Ruz7);
	const ibex::ExprSymbol& Ruz8=ibex::ExprSymbol::new_(); args.add(Ruz8);

	const ibex::ExprSymbol& Rvx1=ibex::ExprSymbol::new_(); args.add(Rvx1);
	const ibex::ExprSymbol& Rvx2=ibex::ExprSymbol::new_(); args.add(Rvx2);
	const ibex::ExprSymbol& Rvx3=ibex::ExprSymbol::new_(); args.add(Rvx3);
	const ibex::ExprSymbol& Rvx4=ibex::ExprSymbol::new_(); args.add(Rvx4);
	const ibex::ExprSymbol& Rvx5=ibex::ExprSymbol::new_(); args.add(Rvx5);
	const ibex::ExprSymbol& Rvx6=ibex::ExprSymbol::new_(); args.add(Rvx6);
	const ibex::ExprSymbol& Rvx7=ibex::ExprSymbol::new_(); args.add(Rvx7);
	const ibex::ExprSymbol& Rvx8=ibex::ExprSymbol::new_(); args.add(Rvx8);

	const ibex::ExprSymbol& Rvy1=ibex::ExprSymbol::new_(); args.add(Rvy1);
	const ibex::ExprSymbol& Rvy2=ibex::ExprSymbol::new_(); args.add(Rvy2);
	const ibex::ExprSymbol& Rvy3=ibex::ExprSymbol::new_(); args.add(Rvy3);
	const ibex::ExprSymbol& Rvy4=ibex::ExprSymbol::new_(); args.add(Rvy4);
	const ibex::ExprSymbol& Rvy5=ibex::ExprSymbol::new_(); args.add(Rvy5);
	const ibex::ExprSymbol& Rvy6=ibex::ExprSymbol::new_(); args.add(Rvy6);
	const ibex::ExprSymbol& Rvy7=ibex::ExprSymbol::new_(); args.add(Rvy7);
	const ibex::ExprSymbol& Rvy8=ibex::ExprSymbol::new_(); args.add(Rvy8);

	const ibex::ExprSymbol& Rvz1=ibex::ExprSymbol::new_(); args.add(Rvz1);
	const ibex::ExprSymbol& Rvz2=ibex::ExprSymbol::new_(); args.add(Rvz2);
	const ibex::ExprSymbol& Rvz3=ibex::ExprSymbol::new_(); args.add(Rvz3);
	const ibex::ExprSymbol& Rvz4=ibex::ExprSymbol::new_(); args.add(Rvz4);
	const ibex::ExprSymbol& Rvz5=ibex::ExprSymbol::new_(); args.add(Rvz5);
	const ibex::ExprSymbol& Rvz6=ibex::ExprSymbol::new_(); args.add(Rvz6);
	const ibex::ExprSymbol& Rvz7=ibex::ExprSymbol::new_(); args.add(Rvz7);
	const ibex::ExprSymbol& Rvz8=ibex::ExprSymbol::new_(); args.add(Rvz8);

	const ibex::ExprSymbol& Rwx1=ibex::ExprSymbol::new_(); args.add(Rwx1);
	const ibex::ExprSymbol& Rwx2=ibex::ExprSymbol::new_(); args.add(Rwx2);
	const ibex::ExprSymbol& Rwx3=ibex::ExprSymbol::new_(); args.add(Rwx3);
	const ibex::ExprSymbol& Rwx4=ibex::ExprSymbol::new_(); args.add(Rwx4);
	const ibex::ExprSymbol& Rwx5=ibex::ExprSymbol::new_(); args.add(Rwx5);
	const ibex::ExprSymbol& Rwx6=ibex::ExprSymbol::new_(); args.add(Rwx6);
	const ibex::ExprSymbol& Rwx7=ibex::ExprSymbol::new_(); args.add(Rwx7);
	const ibex::ExprSymbol& Rwx8=ibex::ExprSymbol::new_(); args.add(Rwx8);

	const ibex::ExprSymbol& Rwy1=ibex::ExprSymbol::new_(); args.add(Rwy1);
	const ibex::ExprSymbol& Rwy2=ibex::ExprSymbol::new_(); args.add(Rwy2);
	const ibex::ExprSymbol& Rwy3=ibex::ExprSymbol::new_(); args.add(Rwy3);
	const ibex::ExprSymbol& Rwy4=ibex::ExprSymbol::new_(); args.add(Rwy4);
	const ibex::ExprSymbol& Rwy5=ibex::ExprSymbol::new_(); args.add(Rwy5);
	const ibex::ExprSymbol& Rwy6=ibex::ExprSymbol::new_(); args.add(Rwy6);
	const ibex::ExprSymbol& Rwy7=ibex::ExprSymbol::new_(); args.add(Rwy7);
	const ibex::ExprSymbol& Rwy8=ibex::ExprSymbol::new_(); args.add(Rwy8);

	const ibex::ExprSymbol& Rwz1=ibex::ExprSymbol::new_(); args.add(Rwz1);
	const ibex::ExprSymbol& Rwz2=ibex::ExprSymbol::new_(); args.add(Rwz2);
	const ibex::ExprSymbol& Rwz3=ibex::ExprSymbol::new_(); args.add(Rwz3);
	const ibex::ExprSymbol& Rwz4=ibex::ExprSymbol::new_(); args.add(Rwz4);
	const ibex::ExprSymbol& Rwz5=ibex::ExprSymbol::new_(); args.add(Rwz5);
	const ibex::ExprSymbol& Rwz6=ibex::ExprSymbol::new_(); args.add(Rwz6);
	const ibex::ExprSymbol& Rwz7=ibex::ExprSymbol::new_(); args.add(Rwz7);
	const ibex::ExprSymbol& Rwz8=ibex::ExprSymbol::new_(); args.add(Rwz8);

	const ibex::ExprSymbol& rx2=ibex::ExprSymbol::new_(); args.add(rx2);
	const ibex::ExprSymbol& rx3=ibex::ExprSymbol::new_(); args.add(rx3);
	const ibex::ExprSymbol& rx4=ibex::ExprSymbol::new_(); args.add(rx4);
	const ibex::ExprSymbol& rx5=ibex::ExprSymbol::new_(); args.add(rx5);
	const ibex::ExprSymbol& rx6=ibex::ExprSymbol::new_(); args.add(rx6);
	const ibex::ExprSymbol& rx7=ibex::ExprSymbol::new_(); args.add(rx7);

	const ibex::ExprSymbol& ry2=ibex::ExprSymbol::new_(); args.add(ry2);
	const ibex::ExprSymbol& ry3=ibex::ExprSymbol::new_(); args.add(ry3);
	const ibex::ExprSymbol& ry4=ibex::ExprSymbol::new_(); args.add(ry4);
	const ibex::ExprSymbol& ry5=ibex::ExprSymbol::new_(); args.add(ry5);
	const ibex::ExprSymbol& ry6=ibex::ExprSymbol::new_(); args.add(ry6);
	const ibex::ExprSymbol& ry7=ibex::ExprSymbol::new_(); args.add(ry7);

	const ibex::ExprSymbol& rz2=ibex::ExprSymbol::new_(); args.add(rz2);
	const ibex::ExprSymbol& rz3=ibex::ExprSymbol::new_(); args.add(rz3);
	const ibex::ExprSymbol& rz4=ibex::ExprSymbol::new_(); args.add(rz4);
	const ibex::ExprSymbol& rz5=ibex::ExprSymbol::new_(); args.add(rz5);
	const ibex::ExprSymbol& rz6=ibex::ExprSymbol::new_(); args.add(rz6);
	const ibex::ExprSymbol& rz7=ibex::ExprSymbol::new_(); args.add(rz7);

	const ibex::ExprSymbol& qux1=ibex::ExprSymbol::new_(); args.add(qux1);
	const ibex::ExprSymbol& quy1=ibex::ExprSymbol::new_(); args.add(quy1);
	const ibex::ExprSymbol& quz1=ibex::ExprSymbol::new_(); args.add(quz1);
	const ibex::ExprSymbol& qvx1=ibex::ExprSymbol::new_(); args.add(qvx1);
	const ibex::ExprSymbol& qvy1=ibex::ExprSymbol::new_(); args.add(qvy1);
	const ibex::ExprSymbol& qvz1=ibex::ExprSymbol::new_(); args.add(qvz1);

	const ibex::ExprSymbol& qux2=ibex::ExprSymbol::new_(); args.add(qux2);
	const ibex::ExprSymbol& quy2=ibex::ExprSymbol::new_(); args.add(quy2);
	const ibex::ExprSymbol& quz2=ibex::ExprSymbol::new_(); args.add(quz2);
	const ibex::ExprSymbol& qvx2=ibex::ExprSymbol::new_(); args.add(qvx2);
	const ibex::ExprSymbol& qvy2=ibex::ExprSymbol::new_(); args.add(qvy2);
	const ibex::ExprSymbol& qvz2=ibex::ExprSymbol::new_(); args.add(qvz2);

	const ibex::ExprSymbol& qux3=ibex::ExprSymbol::new_(); args.add(qux3);
	const ibex::ExprSymbol& quy3=ibex::ExprSymbol::new_(); args.add(quy3);
	const ibex::ExprSymbol& quz3=ibex::ExprSymbol::new_(); args.add(quz3);
	const ibex::ExprSymbol& qvx3=ibex::ExprSymbol::new_(); args.add(qvx3);
	const ibex::ExprSymbol& qvy3=ibex::ExprSymbol::new_(); args.add(qvy3);
	const ibex::ExprSymbol& qvz3=ibex::ExprSymbol::new_(); args.add(qvz3);

	const ibex::ExprSymbol& qux4=ibex::ExprSymbol::new_(); args.add(qux4);
	const ibex::ExprSymbol& quy4=ibex::ExprSymbol::new_(); args.add(quy4);
	const ibex::ExprSymbol& quz4=ibex::ExprSymbol::new_(); args.add(quz4);
	const ibex::ExprSymbol& qvx4=ibex::ExprSymbol::new_(); args.add(qvx4);
	const ibex::ExprSymbol& qvy4=ibex::ExprSymbol::new_(); args.add(qvy4);
	const ibex::ExprSymbol& qvz4=ibex::ExprSymbol::new_(); args.add(qvz4);

	const ibex::ExprSymbol& qux5=ibex::ExprSymbol::new_(); args.add(qux5);
	const ibex::ExprSymbol& quy5=ibex::ExprSymbol::new_(); args.add(quy5);
	const ibex::ExprSymbol& quz5=ibex::ExprSymbol::new_(); args.add(quz5);
	const ibex::ExprSymbol& qvx5=ibex::ExprSymbol::new_(); args.add(qvx5);
	const ibex::ExprSymbol& qvy5=ibex::ExprSymbol::new_(); args.add(qvy5);
	const ibex::ExprSymbol& qvz5=ibex::ExprSymbol::new_(); args.add(qvz5);

	const ibex::ExprSymbol& qux6=ibex::ExprSymbol::new_(); args.add(qux6);
	const ibex::ExprSymbol& quy6=ibex::ExprSymbol::new_(); args.add(quy6);
	const ibex::ExprSymbol& quz6=ibex::ExprSymbol::new_(); args.add(quz6);
	const ibex::ExprSymbol& qvx6=ibex::ExprSymbol::new_(); args.add(qvx6);
	const ibex::ExprSymbol& qvy6=ibex::ExprSymbol::new_(); args.add(qvy6);
	const ibex::ExprSymbol& qvz6=ibex::ExprSymbol::new_(); args.add(qvz6);

	const ibex::ExprSymbol& qux7=ibex::ExprSymbol::new_(); args.add(qux7);
	const ibex::ExprSymbol& quy7=ibex::ExprSymbol::new_(); args.add(quy7);
	const ibex::ExprSymbol& quz7=ibex::ExprSymbol::new_(); args.add(quz7);
	const ibex::ExprSymbol& qvx7=ibex::ExprSymbol::new_(); args.add(qvx7);
	const ibex::ExprSymbol& qvy7=ibex::ExprSymbol::new_(); args.add(qvy7);
	const ibex::ExprSymbol& qvz7=ibex::ExprSymbol::new_(); args.add(qvz7);

	const ibex::ExprSymbol& hux1vx1=ibex::ExprSymbol::new_(); args.add(hux1vx1);
	const ibex::ExprSymbol& hux1vy1=ibex::ExprSymbol::new_(); args.add(hux1vy1);
	const ibex::ExprSymbol& hux1vz1=ibex::ExprSymbol::new_(); args.add(hux1vz1);
	const ibex::ExprSymbol& huy1vx1=ibex::ExprSymbol::new_(); args.add(huy1vx1);
	const ibex::ExprSymbol& huy1vy1=ibex::ExprSymbol::new_(); args.add(huy1vy1);
	const ibex::ExprSymbol& huy1vz1=ibex::ExprSymbol::new_(); args.add(huy1vz1);
	const ibex::ExprSymbol& huz1vx1=ibex::ExprSymbol::new_(); args.add(huz1vx1);
	const ibex::ExprSymbol& huz1vy1=ibex::ExprSymbol::new_(); args.add(huz1vy1);
	const ibex::ExprSymbol& huz1vz1=ibex::ExprSymbol::new_(); args.add(huz1vz1);

	const ibex::ExprSymbol& hux2vx2=ibex::ExprSymbol::new_(); args.add(hux2vx2);
	const ibex::ExprSymbol& hux2vy2=ibex::ExprSymbol::new_(); args.add(hux2vy2);
	const ibex::ExprSymbol& hux2vz2=ibex::ExprSymbol::new_(); args.add(hux2vz2);
	const ibex::ExprSymbol& huy2vx2=ibex::ExprSymbol::new_(); args.add(huy2vx2);
	const ibex::ExprSymbol& huy2vy2=ibex::ExprSymbol::new_(); args.add(huy2vy2);
	const ibex::ExprSymbol& huy2vz2=ibex::ExprSymbol::new_(); args.add(huy2vz2);
	const ibex::ExprSymbol& huz2vx2=ibex::ExprSymbol::new_(); args.add(huz2vx2);
	const ibex::ExprSymbol& huz2vy2=ibex::ExprSymbol::new_(); args.add(huz2vy2);
	const ibex::ExprSymbol& huz2vz2=ibex::ExprSymbol::new_(); args.add(huz2vz2);

	const ibex::ExprSymbol& hux3vx3=ibex::ExprSymbol::new_(); args.add(hux3vx3);
	const ibex::ExprSymbol& hux3vy3=ibex::ExprSymbol::new_(); args.add(hux3vy3);
	const ibex::ExprSymbol& hux3vz3=ibex::ExprSymbol::new_(); args.add(hux3vz3);
	const ibex::ExprSymbol& huy3vx3=ibex::ExprSymbol::new_(); args.add(huy3vx3);
	const ibex::ExprSymbol& huy3vy3=ibex::ExprSymbol::new_(); args.add(huy3vy3);
	const ibex::ExprSymbol& huy3vz3=ibex::ExprSymbol::new_(); args.add(huy3vz3);
	const ibex::ExprSymbol& huz3vx3=ibex::ExprSymbol::new_(); args.add(huz3vx3);
	const ibex::ExprSymbol& huz3vy3=ibex::ExprSymbol::new_(); args.add(huz3vy3);
	const ibex::ExprSymbol& huz3vz3=ibex::ExprSymbol::new_(); args.add(huz3vz3);

	const ibex::ExprSymbol& hux4vx4=ibex::ExprSymbol::new_(); args.add(hux4vx4);
	const ibex::ExprSymbol& hux4vy4=ibex::ExprSymbol::new_(); args.add(hux4vy4);
	const ibex::ExprSymbol& hux4vz4=ibex::ExprSymbol::new_(); args.add(hux4vz4);
	const ibex::ExprSymbol& huy4vx4=ibex::ExprSymbol::new_(); args.add(huy4vx4);
	const ibex::ExprSymbol& huy4vy4=ibex::ExprSymbol::new_(); args.add(huy4vy4);
	const ibex::ExprSymbol& huy4vz4=ibex::ExprSymbol::new_(); args.add(huy4vz4);
	const ibex::ExprSymbol& huz4vx4=ibex::ExprSymbol::new_(); args.add(huz4vx4);
	const ibex::ExprSymbol& huz4vy4=ibex::ExprSymbol::new_(); args.add(huz4vy4);
	const ibex::ExprSymbol& huz4vz4=ibex::ExprSymbol::new_(); args.add(huz4vz4);

	const ibex::ExprSymbol& hux5vx5=ibex::ExprSymbol::new_(); args.add(hux5vx5);
	const ibex::ExprSymbol& hux5vy5=ibex::ExprSymbol::new_(); args.add(hux5vy5);
	const ibex::ExprSymbol& hux5vz5=ibex::ExprSymbol::new_(); args.add(hux5vz5);
	const ibex::ExprSymbol& huy5vx5=ibex::ExprSymbol::new_(); args.add(huy5vx5);
	const ibex::ExprSymbol& huy5vy5=ibex::ExprSymbol::new_(); args.add(huy5vy5);
	const ibex::ExprSymbol& huy5vz5=ibex::ExprSymbol::new_(); args.add(huy5vz5);
	const ibex::ExprSymbol& huz5vx5=ibex::ExprSymbol::new_(); args.add(huz5vx5);
	const ibex::ExprSymbol& huz5vy5=ibex::ExprSymbol::new_(); args.add(huz5vy5);
	const ibex::ExprSymbol& huz5vz5=ibex::ExprSymbol::new_(); args.add(huz5vz5);

	const ibex::ExprSymbol& hux6vx6=ibex::ExprSymbol::new_(); args.add(hux6vx6);
	const ibex::ExprSymbol& hux6vy6=ibex::ExprSymbol::new_(); args.add(hux6vy6);
	const ibex::ExprSymbol& hux6vz6=ibex::ExprSymbol::new_(); args.add(hux6vz6);
	const ibex::ExprSymbol& huy6vx6=ibex::ExprSymbol::new_(); args.add(huy6vx6);
	const ibex::ExprSymbol& huy6vy6=ibex::ExprSymbol::new_(); args.add(huy6vy6);
	const ibex::ExprSymbol& huy6vz6=ibex::ExprSymbol::new_(); args.add(huy6vz6);
	const ibex::ExprSymbol& huz6vx6=ibex::ExprSymbol::new_(); args.add(huz6vx6);
	const ibex::ExprSymbol& huz6vy6=ibex::ExprSymbol::new_(); args.add(huz6vy6);
	const ibex::ExprSymbol& huz6vz6=ibex::ExprSymbol::new_(); args.add(huz6vz6);

	const ibex::ExprSymbol& hux7vx7=ibex::ExprSymbol::new_(); args.add(hux7vx7);
	const ibex::ExprSymbol& hux7vy7=ibex::ExprSymbol::new_(); args.add(hux7vy7);
	const ibex::ExprSymbol& hux7vz7=ibex::ExprSymbol::new_(); args.add(hux7vz7);
	const ibex::ExprSymbol& huy7vx7=ibex::ExprSymbol::new_(); args.add(huy7vx7);
	const ibex::ExprSymbol& huy7vy7=ibex::ExprSymbol::new_(); args.add(huy7vy7);
	const ibex::ExprSymbol& huy7vz7=ibex::ExprSymbol::new_(); args.add(huy7vz7);
	const ibex::ExprSymbol& huz7vx7=ibex::ExprSymbol::new_(); args.add(huz7vx7);
	const ibex::ExprSymbol& huz7vy7=ibex::ExprSymbol::new_(); args.add(huz7vy7);
	const ibex::ExprSymbol& huz7vz7=ibex::ExprSymbol::new_(); args.add(huz7vz7);

	// Known variables
	ibex::Interval d3(28.74,28.74); /* upper arm length */
	ibex::Interval d5(27.15,27.15); /* forearm length */
	ibex::Interval d8(3.76,3.76); /* hand length */
	ibex::Interval rx1(0,0);
	ibex::Interval ry1(0,0);
	ibex::Interval rz1(0,0);
	ibex::Interval rx8(20,20);
	ibex::Interval ry8(0,0);
	ibex::Interval rz8(-20,-20);

	// Expressions
	ibex::Array<const ibex::ExprNode> expressions;
	/* LINEAR */
	expressions.add(rx1 + Rvx1 - rx2 + Rwx2);
	expressions.add(ry1 + Rvy1 - ry2 + Rwy2);
	expressions.add(rz1 + Rvz1 - rz2 + Rwz2);
	expressions.add(rx2 + Rvx2 * (d3 - 1) - rx3 + Rwx3);
	expressions.add(ry2 + Rvy2 * (d3 - 1) - ry3 + Rwy3);
	expressions.add(rz2 + Rvz2 * (d3 - 1) - rz3 + Rwz3);
	expressions.add(rx3 + Rvx3 - rx4 + Rwx4);
	expressions.add(ry3 + Rvy3 - ry4 + Rwy4);
	expressions.add(rz3 + Rvz3 - rz4 + Rwz4);
	expressions.add(rx4 + Rvx4 * (d5 - 1) - rx5 + Rwx5);
	expressions.add(ry4 + Rvy4 * (d5 - 1) - ry5 + Rwy5);
	expressions.add(rz4 + Rvz4 * (d5 - 1) - rz5 + Rwz5);
	expressions.add(rx5 + Rvx5 - rx6 + Rwx6);
	expressions.add(ry5 + Rvy5 - ry6 + Rwy6);
	expressions.add(rz5 + Rvz5 - rz6 + Rwz6);
	expressions.add(rx6 + Rvx6 - rx7 + Rwx7);
	expressions.add(ry6 + Rvy6 - ry7 + Rwy7);
	expressions.add(rz6 + Rvz6 - rz7 + Rwz7);
	expressions.add(rx7 + Rvx7 * (d8 - 1) - rx8 + Rwx8);
	expressions.add(ry7 + Rvy7 * (d8 - 1) - ry8 + Rwy8);
	expressions.add(rz7 + Rvz7 * (d8 - 1) - rz8 + Rwz8);
	expressions.add(-Rvx1 - Rwx2);
	expressions.add(-Rvy1 - Rwy2);
	expressions.add(-Rvz1 - Rwz2);
	expressions.add(Rvx2 - Rwx3);
	expressions.add(Rvy2 - Rwy3);
	expressions.add(Rvz2 - Rwz3);
	expressions.add(-Rvx3 - Rwx4);
	expressions.add(-Rvy3 - Rwy4);
	expressions.add(-Rvz3 - Rwz4);
	expressions.add(Rvx4 - Rwx5);
	expressions.add(Rvy4 - Rwy5);
	expressions.add(Rvz4 - Rwz5);
	expressions.add(-Rvx5 - Rwx6);
	expressions.add(-Rvy5 - Rwy6);
	expressions.add(-Rvz5 - Rwz6);
	expressions.add(-Rvx6 - Rwx7);
	expressions.add(-Rvy6 - Rwy7);
	expressions.add(-Rvz6 - Rwz7);
	expressions.add(Rwx7 - Rux8);
	expressions.add(Rwy7 - Ruy8);
	expressions.add(Rwz7 - Ruz8);
	expressions.add(Rux7 - Rvx8);
	expressions.add(Ruy7 - Rvy8);
	expressions.add(Ruz7 - Rvz8);
	expressions.add(Rvx7 - Rwx8);
	expressions.add(Rvy7 - Rwy8);
	expressions.add(Rvz7 - Rwz8);
	/* PARABOLIC LINEARISED */
	expressions.add(qux1 + quy1 + quz1 - 1);
	expressions.add(qvx1 + qvy1 + qvz1 - 1);
	expressions.add(qux2 + quy2 + quz2 - 1);
	expressions.add(qvx2 + qvy2 + qvz2 - 1);
	expressions.add(qux3 + quy3 + quz3 - 1);
	expressions.add(qvx3 + qvy3 + qvz3 - 1);
	expressions.add(qux4 + quy4 + quz4 - 1);
	expressions.add(qvx4 + qvy4 + qvz4 - 1);
	expressions.add(qux5 + quy5 + quz5 - 1);
	expressions.add(qvx5 + qvy5 + qvz5 - 1);
	expressions.add(qux6 + quy6 + quz6 - 1);
	expressions.add(qvx6 + qvy6 + qvz6 - 1);
	expressions.add(qux7 + quy7 + quz7 - 1);
	expressions.add(qvx7 + qvy7 + qvz7 - 1);
	/* HYPERBOLIC LINEARISED */
	expressions.add(hux1vx1 + huy1vy1 + huz1vz1);
	expressions.add(huy1vz1 - huz1vy1 - Rwx1);
	expressions.add(-hux1vz1 + huz1vx1 - Rwy1);
	expressions.add(hux1vy1 - huy1vx1 - Rwz1);
	expressions.add(hux2vx2 + huy2vy2 + huz2vz2);
	expressions.add(huy2vz2 - huz2vy2 - Rwx2);
	expressions.add(-hux2vz2 + huz2vx2 - Rwy2);
	expressions.add(hux2vy2 - huy2vx2 - Rwz2);
	expressions.add(hux3vx3 + huy3vy3 + huz3vz3);
	expressions.add(huy3vz3 - huz3vy3 - Rwx3);
	expressions.add(-hux3vz3 + huz3vx3 - Rwy3);
	expressions.add(hux3vy3 - huy3vx3 - Rwz3);
	expressions.add(hux4vx4 + huy4vy4 + huz4vz4);
	expressions.add(huy4vz4 - huz4vy4 - Rwx4);
	expressions.add(-hux4vz4 + huz4vx4 - Rwy4);
	expressions.add(hux4vy4 - huy4vx4 - Rwz4);
	expressions.add(hux5vx5 + huy5vy5 + huz5vz5);
	expressions.add(huy5vz5 - huz5vy5 - Rwx5);
	expressions.add(-hux5vz5 + huz5vx5 - Rwy5);
	expressions.add(hux5vy5 - huy5vx5 - Rwz5);
	expressions.add(hux6vx6 + huy6vy6 + huz6vz6);
	expressions.add(huy6vz6 - huz6vy6 - Rwx6);
	expressions.add(-hux6vz6 + huz6vx6 - Rwy6);
	expressions.add(hux6vy6 - huy6vx6 - Rwz6);
	expressions.add(hux7vx7 + huy7vy7 + huz7vz7);
	expressions.add(huy7vz7 - huz7vy7 - Rwx7);
	expressions.add(-hux7vz7 + huz7vx7 - Rwy7);
	expressions.add(hux7vy7 - huy7vx7 - Rwz7);
	/* PARABOLIC */
	expressions.add(qux1 - sqr(Rux1));
	expressions.add(quy1 - sqr(Ruy1));
	expressions.add(quz1 - sqr(Ruz1));
	expressions.add(qvx1 - sqr(Rvx1));
	expressions.add(qvy1 - sqr(Rvy1));
	expressions.add(qvz1 - sqr(Rvz1));
	expressions.add(qux2 - sqr(Rux2));
	expressions.add(quy2 - sqr(Ruy2));
	expressions.add(quz2 - sqr(Ruz2));
	expressions.add(qvx2 - sqr(Rvx2));
	expressions.add(qvy2 - sqr(Rvy2));
	expressions.add(qvz2 - sqr(Rvz2));
	expressions.add(qux3 - sqr(Rux3));
	expressions.add(quy3 - sqr(Ruy3));
	expressions.add(quz3 - sqr(Ruz3));
	expressions.add(qvx3 - sqr(Rvx3));
	expressions.add(qvy3 - sqr(Rvy3));
	expressions.add(qvz3 - sqr(Rvz3));
	expressions.add(qux4 - sqr(Rux4));
	expressions.add(quy4 - sqr(Ruy4));
	expressions.add(quz4 - sqr(Ruz4));
	expressions.add(qvx4 - sqr(Rvx4));
	expressions.add(qvy4 - sqr(Rvy4));
	expressions.add(qvz4 - sqr(Rvz4));
	expressions.add(qux5 - sqr(Rux5));
	expressions.add(quy5 - sqr(Ruy5));
	expressions.add(quz5 - sqr(Ruz5));
	expressions.add(qvx5 - sqr(Rvx5));
	expressions.add(qvy5 - sqr(Rvy5));
	expressions.add(qvz5 - sqr(Rvz5));
	expressions.add(qux6 - sqr(Rux6));
	expressions.add(quy6 - sqr(Ruy6));
	expressions.add(quz6 - sqr(Ruz6));
	expressions.add(qvx6 - sqr(Rvx6));
	expressions.add(qvy6 - sqr(Rvy6));
	expressions.add(qvz6 - sqr(Rvz6));
	expressions.add(qux7 - sqr(Rux7));
	expressions.add(quy7 - sqr(Ruy7));
	expressions.add(quz7 - sqr(Ruz7));
	expressions.add(qvx7 - sqr(Rvx7));
	expressions.add(qvy7 - sqr(Rvy7));
	expressions.add(qvz7 - sqr(Rvz7));
	/* HYPERBOLIC */
	expressions.add(hux1vx1 - Rux1 * Rvx1);
	expressions.add(hux1vy1 - Rux1 * Rvy1);
	expressions.add(hux1vz1 - Rux1 * Rvz1);
	expressions.add(huy1vx1 - Ruy1 * Rvx1);
	expressions.add(huy1vy1 - Ruy1 * Rvy1);
	expressions.add(huy1vz1 - Ruy1 * Rvz1);
	expressions.add(huz1vx1 - Ruz1 * Rvx1);
	expressions.add(huz1vy1 - Ruz1 * Rvy1);
	expressions.add(huz1vz1 - Ruz1 * Rvz1);
	expressions.add(hux2vx2 - Rux2 * Rvx2);
	expressions.add(hux2vy2 - Rux2 * Rvy2);
	expressions.add(hux2vz2 - Rux2 * Rvz2);
	expressions.add(huy2vx2 - Ruy2 * Rvx2);
	expressions.add(huy2vy2 - Ruy2 * Rvy2);
	expressions.add(huy2vz2 - Ruy2 * Rvz2);
	expressions.add(huz2vx2 - Ruz2 * Rvx2);
	expressions.add(huz2vy2 - Ruz2 * Rvy2);
	expressions.add(huz2vz2 - Ruz2 * Rvz2);
	expressions.add(hux3vx3 - Rux3 * Rvx3);
	expressions.add(hux3vy3 - Rux3 * Rvy3);
	expressions.add(hux3vz3 - Rux3 * Rvz3);
	expressions.add(huy3vx3 - Ruy3 * Rvx3);
	expressions.add(huy3vy3 - Ruy3 * Rvy3);
	expressions.add(huy3vz3 - Ruy3 * Rvz3);
	expressions.add(huz3vx3 - Ruz3 * Rvx3);
	expressions.add(huz3vy3 - Ruz3 * Rvy3);
	expressions.add(huz3vz3 - Ruz3 * Rvz3);
	expressions.add(hux4vx4 - Rux4 * Rvx4);
	expressions.add(hux4vy4 - Rux4 * Rvy4);
	expressions.add(hux4vz4 - Rux4 * Rvz4);
	expressions.add(huy4vx4 - Ruy4 * Rvx4);
	expressions.add(huy4vy4 - Ruy4 * Rvy4);
	expressions.add(huy4vz4 - Ruy4 * Rvz4);
	expressions.add(huz4vx4 - Ruz4 * Rvx4);
	expressions.add(huz4vy4 - Ruz4 * Rvy4);
	expressions.add(huz4vz4 - Ruz4 * Rvz4);
	expressions.add(hux5vx5 - Rux5 * Rvx5);
	expressions.add(hux5vy5 - Rux5 * Rvy5);
	expressions.add(hux5vz5 - Rux5 * Rvz5);
	expressions.add(huy5vx5 - Ruy5 * Rvx5);
	expressions.add(huy5vy5 - Ruy5 * Rvy5);
	expressions.add(huy5vz5 - Ruy5 * Rvz5);
	expressions.add(huz5vx5 - Ruz5 * Rvx5);
	expressions.add(huz5vy5 - Ruz5 * Rvy5);
	expressions.add(huz5vz5 - Ruz5 * Rvz5);
	expressions.add(hux6vx6 - Rux6 * Rvx6);
	expressions.add(hux6vy6 - Rux6 * Rvy6);
	expressions.add(hux6vz6 - Rux6 * Rvz6);
	expressions.add(huy6vx6 - Ruy6 * Rvx6);
	expressions.add(huy6vy6 - Ruy6 * Rvy6);
	expressions.add(huy6vz6 - Ruy6 * Rvz6);
	expressions.add(huz6vx6 - Ruz6 * Rvx6);
	expressions.add(huz6vy6 - Ruz6 * Rvy6);
	expressions.add(huz6vz6 - Ruz6 * Rvz6);
	expressions.add(hux7vx7 - Rux7 * Rvx7);
	expressions.add(hux7vy7 - Rux7 * Rvy7);
	expressions.add(hux7vz7 - Rux7 * Rvz7);
	expressions.add(huy7vx7 - Ruy7 * Rvx7);
	expressions.add(huy7vy7 - Ruy7 * Rvy7);
	expressions.add(huy7vz7 - Ruy7 * Rvz7);
	expressions.add(huz7vx7 - Ruz7 * Rvx7);
	expressions.add(huz7vy7 - Ruz7 * Rvy7);
	expressions.add(huz7vz7 - Ruz7 * Rvz7);

	/* ################################################
	BUILD THE SYSTEM
	################################################ */
	// Generate ibex::System
	ibex::SystemFactory systemfactory;
	systemfactory.add_var(args);
	for (int i=0; i<expressions.size(); i++)
		systemfactory.add_ctr_eq(expressions[i]);
	ibex::System system_from_factory(systemfactory);
	std::cout << system_from_factory << std::endl;

	/* ################################################
	FIND LINEAR EQUATIONS IN THE SYSTEM
	################################################ */
	// Check for linear expressions and generate linear ibex::System
	ibex::IntervalMatrix lp_a(args.size(),args.size());
	ibex::IntervalVector lp_b(args.size());
	ibex::SystemFactory systemfactory_linear;
	systemfactory_linear.add_var(args);
	for (int i=0; i<expressions.size(); i++){
		ibex::ExprLinearity lin(args,expressions[i]);
		if (lin.is_linear(expressions[i])){
			systemfactory_linear.add_ctr_eq(expressions[i]);
			ibex::IntervalVector coeffs = lin.coeff_vector(expressions[i]);
			lp_a.set_row(i, coeffs.subvector(0,coeffs.size()-2));
			lp_b[i] = coeffs[coeffs.size()-1];
		}
		std::cout << lin.is_linear(expressions[i]) << std::endl;
	}
	ibex::System system_from_factory_linear(systemfactory_linear);
	std::cout << system_from_factory_linear << std::endl;
	std::cout << lp_a << std::endl;
	std::cout << lp_b << std::endl;
	cleanup(expressions,true);

	/* ################################################
	LOAD SYSTEMS FROM MINIBEX FILES
	################################################ */
	// Load system
//	ibex::System system("minibex/human_arm/kinematics_ZYX_workspace_relaxed.mbx");
//	std::cout << system << std::endl;
//	std::cout << system.ctrs.size() << '\t' << system.box.size() << std::endl;

//	//TODO: how to extract equations from system
//	ibex::System system("minibex/human_arm/kinematics_ZYX_workspace_relaxed.mbx");
//	std::cout << system.ctrs.size() << '\t' << system.box.size() << std::endl;
//
//	ibex::Array<const ibex::ExprSymbol> args2;
//	for (int i=0; i<system.args.size(); i++){
//		const ibex::ExprSymbol& s = system.args[i];
//		args2.add(s);
//	}
//	std::cout << "1" << std::endl;
//
//	for (int i=0; i<args2.size(); i++){
//		std::cout << args2[i] << std::endl;
//	}
//
//	ibex::Array<const ibex::ExprNode> expressions2;
//	for (int i=0; i<system.ctrs.size(); i++){
//		const ibex::ExprNode& e = system.ctrs[i].f.expr();
//		expressions2.add(e);
//	}
//	std::cout << "2" << std::endl;
//
//	for (int i=0; i<expressions2.size(); i++){
//		std::cout << expressions2[i] << std::endl;
//	}
//
//	for (int i=0; i<expressions2.size(); i++){
//		ibex::ExprLinearity lin(args2,expressions2[i]);
//		std::cout << lin.is_linear(expressions2[i]) << '\t' << lin.coeff_vector(expressions2[i]) << std::endl;
//	}
//	std::cout << "3" << std::endl;
//	cleanup(expressions2,true);

//
//	std::vector<std::string> variable_list;
//	ibex::Array<const ibex::ExprSymbol> exprSymbols;
//	for (int vars_i=0; vars_i<system.args.size(); vars_i++){
//		const ibex::ExprSymbol& x=ibex::ExprSymbol::new_();
//		x = system.args[vars_i];
//		exprSymbols.add(x);
//		variable_list.push_back(system.args[vars_i].name);
//		std::cout << system.args[vars_i].name << std::endl;
//	}
//	std::cout << variable_list.size() << std::endl;
//
//	std::vector<ibex::ExprNode> constraint_list;
//	for (int ctrs_i=0; ctrs_i<system.ctrs.size(); ctrs_i++){
////		constraint_list.push_back(system.ctrs[ctrs_i].f.expr());
//		std::cout << system.ctrs[ctrs_i].f.expr() << std::endl;
//		const ibex::ExprNode& constraint = system.ctrs[ctrs_i].f.expr();
//		std::cout << "here" << std::endl;
//		ibex::ExprLinearity lin(exprSymbols,constraint);
//		std::cout << "here" << std::endl;
//		bool is_linear = lin.is_linear(constraint);
//		std::cout << "here" << std::endl;
//		ibex::cleanup(constraint,true);
//		std::cout << is_linear << std::endl;
//		std::vector<int> used_vars = system.ctrs[ctrs_i].f.used_vars;
//		for (int index=0; index<used_vars.size(); index++){
//			int var_i = used_vars[index];
//			std::cout << variable_list[var_i] << std::endl;
//		}
//	}
//	std::cout << "All constraints" << std::endl;
//
//
//	auto start = std::chrono::steady_clock::now();
//	// Build contractors (rebuilding in loop improves contraction performance)
//	ibex::CtcHC4 hc4(system,0.01);
//	ibex::CtcHC4 hc4_2(system,0.1,true);
//	ibex::CtcAcid acid(system, hc4_2);
//	ibex::CtcNewton newton(system.f_ctrs, 5e+08, 1e-07, 1e-04);
//	ibex::LinearizerCombo linear_relax(system,ibex::LinearizerCombo::XNEWTON);
//	ibex::CtcPolytopeHull polytope(linear_relax);
//	ibex::CtcCompo polytope_hc4(polytope, hc4);
//	ibex::CtcFixPoint fixpoint(polytope_hc4);
//	ibex::CtcCompo compo(hc4,acid,newton,fixpoint);
//
//	// Linear system
//
//	// Build bisector
//	ibex::SmearSumRelative bisector(system, 1e-07);
//
//	std::vector<ibex::IntervalVector> list;
//	list.push_back(system.box);
//
//	// Loop over list to find solutions
//	int i = 0;
//	while(list.size()>0){
//		compo.contract(list.back());
//
//		// Bisection
//		if (!list.back().is_empty()){
//			try{
//				std::pair<ibex::IntervalVector, ibex::IntervalVector> bisection = bisector.bisect(list.back());
//				list.pop_back();
//				list.push_back(bisection.first);
//				list.push_back(bisection.second);
//			} catch(...) {
//				std::cout << "Boundary " << i++ << std::endl;
//				std::cout << list.back() << std::endl;
//				list.pop_back();
//			}
//		} else {
//			list.pop_back();
//		}
//	}
//	auto end = std::chrono::steady_clock::now();
//	auto diff = end - start;
//	std::cout << "timing: " << std::chrono::duration <double, std::milli> (diff).count() << std::endl;
//
////	/* Build a default solver for the system and with a precision set to 1e-07 */
////	ibex::DefaultSolver solver(system,1e-01);
////	solver.trace = 1;
////
////	solver.solve(system.box); // Run the solver
////
////	std::cout << solver.get_manifold() << std::endl;
////
////	auto end2 = std::chrono::steady_clock::now();
////	auto diff2 = end2 - end;
////	std::cout << "timing2: " << std::chrono::duration <double, std::milli> (diff2).count() << std::endl;

	return 0;
}
