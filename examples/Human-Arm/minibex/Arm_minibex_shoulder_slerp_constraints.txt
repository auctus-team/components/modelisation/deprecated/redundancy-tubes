Constants
  /* Perpendicular axis of twist rotation */
  AX=0;
  AY=0;
  AZ=1;
  /* Hand position */
  XE=10;
  YE=10;
  ZE=-20;
  /* Length upper arm */
  d3=28.74;
  /* Length forearm */
  d5=27.15;
  /* Length hand */
  d8=3.76;
  /* Spherical polygon bounds */
  thetaswing[8]=(1.04719755119660;	2.26892802759263;	0.872664625997165;	0.523598775598299;	2.09439510239320;	1.74532925199433;	2.09439510239320;	1.04719755119660);
  phiswing[8]=(-3.14159265358979;	-2.79252680319093;	-1.91986217719376;	-0.872664625997165;	0;	0.872664625997165;	1.91986217719376;	3.14159265358979);

Variables
  theta1 in [-pi+0.001,pi];
  theta2 in [-pi+0.001,pi];
  theta3 in [-pi+0.001,pi];
  theta4 in [-pi+0.001,pi];
  theta5 in [-pi,pi];
  theta6 in [-pi/2,pi/2];
  theta7 in [-pi/2,pi/2];
  /* Distance shoulder-wrist */
  lsw in [0,oo];
  /* Distance shoulder-circle center */
  lsc in [0,oo];
  /* Circle radius */
  rc in [0,oo];
  /* Wrist position */
  XW in [-oo,oo];
  YW in [-oo,oo];
  ZW in [-oo,oo];
  /* Shoulder-wrist unit vector */
  nX in [-oo,oo];
  nY in [-oo,oo];
  nZ in [-oo,oo];
  /* Circle center position */
  XC in [-oo,oo];
  YC in [-oo,oo];
  ZC in [-oo,oo];
  /* Swivel plane frame */
  uX in [-oo,oo];
  uY in [-oo,oo];
  uZ in [-oo,oo];
  vX in [-oo,oo];
  vY in [-oo,oo];
  vZ in [-oo,oo];
  /* Elbow position */
  XEL in [-oo,oo];
  YEL in [-oo,oo];
  ZEL in [-oo,oo];
  /* swivel angle  */
  psi in [0.0,0.0];
  /* twist angle  */
  phi in [-2.0,-2.0];
  /* EE orientation */
  R11 in [-1,1];
  R12 in [-1,1];
  R13 in [-1,1];
  R21 in [-1,1];
  R22 in [-1,1];
  R23 in [-1,1];
  R31 in [-1,1];
  R32 in [-1,1];
  R33 in [-1,1];
  /* Spherical polygon angles */
  q1[4];
  q2[4];
  q3[4];
  q4[4];
  q5[4];
  q6[4];
  q7[4];
  q8[4];
  Kqs;
  dotq1q2;
  q1q2t[4];
  q1q2tunit[4];
  t1 in [0,1];
  //dotq2q3;
  //q2q3t[4];
  //q2q3tunit[4];
  //t2 in [0,1];
  
function quaternion(ctheta,cphi)
  return ((cos((1/2)*sign(ctheta)*ctheta)); (sin(cphi)*sin((1/2)*ctheta)); (0); (cos(cphi)*sin((1/2)*ctheta)));
end

Constraints
/* EE orientation constraints  */
  R11 = (cos(phi) * (-AX ^ 2 + AZ + 1) + sin(phi) * AX * (1 + AZ)) / (1 + AZ);
  R12 = -1 / (1 + AZ) * AX * AY;
  R13 = ((AX ^ 2 - AZ - 1) * sin(phi) + cos(phi) * AX * (1 + AZ)) / (1 + AZ);
  R21 = -AY * ((-AZ - 1) * sin(phi) + cos(phi) * AX) / (1 + AZ);
  R22 = (-AY ^ 2 + AZ + 1) / (1 + AZ);
  R23 = AY * (sin(phi) * AX + (1 + AZ) * cos(phi)) / (1 + AZ);
  R31 = ((-AX ^ 2 - AY ^ 2 + AZ + 1) * sin(phi) - cos(phi) * AX * (1 + AZ)) / (1 + AZ);
  R32 = -AY;
  R33 = ((-AX ^ 2 - AY ^ 2 + AZ + 1) * cos(phi) + sin(phi) * AX * (1 + AZ)) / (1 + AZ);
/* 7DOF Arm kinematics constraints */
  R11 = ((((cos(theta5) * cos(theta6) * cos(theta4) - sin(theta4) * sin(theta6)) * cos(theta3) - cos(theta6) * sin(theta3) * sin(theta5)) * cos(theta2) - sin(theta2) * (cos(theta5) * cos(theta6) * sin(theta4) + sin(theta6) * cos(theta4))) * cos(theta7) - ((sin(theta5) * cos(theta3) * cos(theta4) + cos(theta5) * sin(theta3)) * cos(theta2) - sin(theta2) * sin(theta4) * sin(theta5)) * sin(theta7)) * cos(theta1) - ((cos(theta3) * cos(theta6) * sin(theta5) + sin(theta3) * (cos(theta5) * cos(theta6) * cos(theta4) - sin(theta4) * sin(theta6))) * cos(theta7) + sin(theta7) * (-sin(theta3) * sin(theta5) * cos(theta4) + cos(theta5) * cos(theta3))) * sin(theta1);
  R12 = ((((-cos(theta5) * cos(theta6) * cos(theta4) + sin(theta4) * sin(theta6)) * cos(theta3) + cos(theta6) * sin(theta3) * sin(theta5)) * cos(theta2) + sin(theta2) * (cos(theta5) * cos(theta6) * sin(theta4) + sin(theta6) * cos(theta4))) * sin(theta7) - ((sin(theta5) * cos(theta3) * cos(theta4) + cos(theta5) * sin(theta3)) * cos(theta2) - sin(theta2) * sin(theta4) * sin(theta5)) * cos(theta7)) * cos(theta1) - sin(theta1) * ((-cos(theta3) * cos(theta6) * sin(theta5) - sin(theta3) * (cos(theta5) * cos(theta6) * cos(theta4) - sin(theta4) * sin(theta6))) * sin(theta7) + cos(theta7) * (-sin(theta3) * sin(theta5) * cos(theta4) + cos(theta5) * cos(theta3)));
  R13 = (((-cos(theta3) * cos(theta4) * cos(theta5) + sin(theta5) * sin(theta3)) * cos(theta2) + cos(theta5) * sin(theta2) * sin(theta4)) * sin(theta6) - cos(theta6) * (sin(theta4) * cos(theta2) * cos(theta3) + cos(theta4) * sin(theta2))) * cos(theta1) + ((cos(theta5) * sin(theta3) * cos(theta4) + sin(theta5) * cos(theta3)) * sin(theta6) + cos(theta6) * sin(theta3) * sin(theta4)) * sin(theta1);
  R21 = ((((cos(theta5) * cos(theta6) * cos(theta4) - sin(theta4) * sin(theta6)) * cos(theta3) - cos(theta6) * sin(theta3) * sin(theta5)) * cos(theta2) - sin(theta2) * (cos(theta5) * cos(theta6) * sin(theta4) + sin(theta6) * cos(theta4))) * sin(theta1) + (cos(theta3) * cos(theta6) * sin(theta5) + sin(theta3) * (cos(theta5) * cos(theta6) * cos(theta4) - sin(theta4) * sin(theta6))) * cos(theta1)) * cos(theta7) - sin(theta7) * (((sin(theta5) * cos(theta3) * cos(theta4) + cos(theta5) * sin(theta3)) * cos(theta2) - sin(theta2) * sin(theta4) * sin(theta5)) * sin(theta1) - cos(theta1) * (-sin(theta3) * sin(theta5) * cos(theta4) + cos(theta5) * cos(theta3)));
  R22 = ((((-cos(theta5) * cos(theta6) * cos(theta4) + sin(theta4) * sin(theta6)) * cos(theta3) + cos(theta6) * sin(theta3) * sin(theta5)) * cos(theta2) + sin(theta2) * (cos(theta5) * cos(theta6) * sin(theta4) + sin(theta6) * cos(theta4))) * sin(theta7) - ((sin(theta5) * cos(theta3) * cos(theta4) + cos(theta5) * sin(theta3)) * cos(theta2) - sin(theta2) * sin(theta4) * sin(theta5)) * cos(theta7)) * sin(theta1) + cos(theta1) * ((-cos(theta3) * cos(theta6) * sin(theta5) - sin(theta3) * (cos(theta5) * cos(theta6) * cos(theta4) - sin(theta4) * sin(theta6))) * sin(theta7) + cos(theta7) * (-sin(theta3) * sin(theta5) * cos(theta4) + cos(theta5) * cos(theta3)));
  R23 = (((-cos(theta3) * cos(theta4) * cos(theta5) + sin(theta5) * sin(theta3)) * cos(theta2) + cos(theta5) * sin(theta2) * sin(theta4)) * sin(theta6) - cos(theta6) * (sin(theta4) * cos(theta2) * cos(theta3) + cos(theta4) * sin(theta2))) * sin(theta1) - ((cos(theta5) * sin(theta3) * cos(theta4) + sin(theta5) * cos(theta3)) * sin(theta6) + cos(theta6) * sin(theta3) * sin(theta4)) * cos(theta1);
  R31 = (((cos(theta5) * cos(theta6) * cos(theta4) - sin(theta4) * sin(theta6)) * cos(theta3) - cos(theta6) * sin(theta3) * sin(theta5)) * sin(theta2) + cos(theta2) * (cos(theta5) * cos(theta6) * sin(theta4) + sin(theta6) * cos(theta4))) * cos(theta7) - ((sin(theta5) * cos(theta3) * cos(theta4) + cos(theta5) * sin(theta3)) * sin(theta2) + sin(theta5) * sin(theta4) * cos(theta2)) * sin(theta7);
  R32 = (((-cos(theta5) * cos(theta6) * cos(theta4) + sin(theta4) * sin(theta6)) * cos(theta3) + cos(theta6) * sin(theta3) * sin(theta5)) * sin(theta7) - cos(theta7) * (sin(theta5) * cos(theta3) * cos(theta4) + cos(theta5) * sin(theta3))) * sin(theta2) - ((cos(theta5) * cos(theta6) * sin(theta4) + sin(theta6) * cos(theta4)) * sin(theta7) + cos(theta7) * sin(theta4) * sin(theta5)) * cos(theta2);
  R33 = ((-cos(theta3) * cos(theta4) * cos(theta5) + sin(theta5) * sin(theta3)) * sin(theta6) - cos(theta6) * sin(theta4) * cos(theta3)) * sin(theta2) + cos(theta2) * (-sin(theta4) * cos(theta5) * sin(theta6) + cos(theta6) * cos(theta4));
  XE = ((cos(theta5) * sin(theta2) * sin(theta6) * d8 - cos(theta2) * cos(theta3) * (cos(theta6) * d8 + d5)) * sin(theta4) - cos(theta2) * d8 * (cos(theta3) * cos(theta4) * cos(theta5) - sin(theta5) * sin(theta3)) * sin(theta6) - ((cos(theta6) * d8 + d5) * cos(theta4) + d3) * sin(theta2)) * cos(theta1) + sin(theta1) * (sin(theta3) * (cos(theta6) * d8 + d5) * sin(theta4) + sin(theta6) * d8 * (cos(theta5) * sin(theta3) * cos(theta4) + sin(theta5) * cos(theta3)));
  YE = ((cos(theta5) * sin(theta2) * sin(theta6) * d8 - cos(theta2) * cos(theta3) * (cos(theta6) * d8 + d5)) * sin(theta4) - cos(theta2) * d8 * (cos(theta3) * cos(theta4) * cos(theta5) - sin(theta5) * sin(theta3)) * sin(theta6) - ((cos(theta6) * d8 + d5) * cos(theta4) + d3) * sin(theta2)) * sin(theta1) - (sin(theta3) * (cos(theta6) * d8 + d5) * sin(theta4) + sin(theta6) * d8 * (cos(theta5) * sin(theta3) * cos(theta4) + sin(theta5) * cos(theta3))) * cos(theta1);
  ZE = ((cos(theta6) * d8 + d5) * cos(theta4) - cos(theta5) * sin(theta4) * sin(theta6) * d8 + d3) * cos(theta2) - ((cos(theta4) * cos(theta5) * sin(theta6) * d8 + sin(theta4) * (cos(theta6) * d8 + d5)) * cos(theta3) - sin(theta3) * sin(theta5) * sin(theta6) * d8) * sin(theta2);
/* Swivel angle constraints */
  lsw = sqrt((d8 ^ 2 + (-2 * R13 * XE - 2 * R23 * YE - 2 * R33 * ZE) * d8 + XE ^ 2 + YE ^ 2 + ZE ^ 2));
  XW = -R13 * d8 + XE;
  YW = -R23 * d8 + YE;
  ZW = -R33 * d8 + ZE;
  nX = XW/lsw;
  nY = YW/lsw;
  nZ = ZW/lsw;
  nX^2+nY^2+nZ^2=1;
  lsc = ((d8 ^ 2 + (-2 * R13 * XE - 2 * R23 * YE - 2 * R33 * ZE) * d8 + XE ^ 2 + YE ^ 2 + ZE ^ 2) + d3^2 - d5^2) / (2*lsw);
  XC = nX*lsc;
  YC = nY*lsc;
  ZC = nZ*lsc;
  rc = sqrt(d3^2-lsc^2);
  uX = -nZ * nX * (1 + nZ ^ 4 + (nX ^ 2 + nY ^ 2 - 2) * nZ ^ 2) ^ (-0.1e1 / 0.2e1);
  uY = -nZ * nY * (1 + nZ ^ 4 + (nX ^ 2 + nY ^ 2 - 2) * nZ ^ 2) ^ (-0.1e1 / 0.2e1);
  uZ = (-nZ ^ 2 + 1) * (1 + nZ ^ 4 + (nX ^ 2 + nY ^ 2 - 2) * nZ ^ 2) ^ (-0.1e1 / 0.2e1);
  vX = nY * uZ - nZ * uY;
  vY = -nX * uZ + nZ * uX;
  vZ = nX * uY - nY * uX;
  XEL = XC + rc*(cos(psi)*uX+sin(psi)*vX);
  YEL = YC + rc*(cos(psi)*uY+sin(psi)*vY);
  ZEL = ZC + rc*(cos(psi)*uZ+sin(psi)*vZ);
  XEL = -cos(theta1) * sin(theta2) * d3;
  YEL = -sin(theta1) * sin(theta2) * d3;
  ZEL = cos(theta2) * d3;
/* Spherical polygon constraints (Liu and Prakash 2003)*/
  q1 = quaternion(thetaswing[0],phiswing[0]);
  q2 = quaternion(thetaswing[1],phiswing[1]);
  q3 = quaternion(thetaswing[2],phiswing[2]);
  q4 = quaternion(thetaswing[3],phiswing[3]);
  q5 = quaternion(thetaswing[4],phiswing[4]);
  q6 = quaternion(thetaswing[5],phiswing[5]);
  q7 = quaternion(thetaswing[6],phiswing[6]);
  q8 = quaternion(thetaswing[7],phiswing[7]);
  Kqs = -YEL/d3;
  //dotq1q2 = abs(q1[0]*q2[0]-q1[1]*q2[1]-q1[2]*q2[2]-q1[3]*q2[3]);
  //thetaq1q2 = acos(dotq1q2);
  //scale0q1q2 = sin((1.0 - t1) * acos(dotq1q2)) / sin(acos(dotq1q2));
  //scale1q1q2 = sin((t1 * acos(dotq1q2))) / sin(acos(dotq1q2));
  //q1q2t = scale0q1q2 * q1 + scale1q1q2 * q2;
  
  dotq1q2 = (q1[0]*q2[0]-q1[1]*q2[1]-q1[2]*q2[2]-q1[3]*q2[3]);
  q1q2t = sin((1.0 - t1) * acos(dotq1q2)) / sin(acos(dotq1q2)) * q1 + sin((t1 * acos(dotq1q2))) / sin(acos(dotq1q2)) * q2;
  //q1q2tunit = 1/sqrt(q1q2t[0]^2 + q1q2t[1]^2 + q1q2t[2]^2 + q1q2t[3]^2) * q1q2t;
  Kqs > cos(2*atan2(sqrt(q1q2t[1]^2 + q1q2t[2]^2 + q1q2t[3]^2), q1q2t[0]));
  
  //dotq2q3 = (q2[0]*q3[0]-q2[1]*q3[1]-q2[2]*q3[2]-q2[3]*q3[3]);
  //q2q3t = sin((1.0 - t2) * acos(dotq2q3)) / sin(acos(dotq2q3)) * q2 + sin((t2 * acos(dotq2q3))) / sin(acos(dotq2q3)) * q3;
  //q2q3tunit = 1/sqrt(q2q3t[0]^2 + q2q3t[1]^2 + q2q3t[2]^2 + q2q3t[3]^2) * q2q3t;
  //Kqs > cos(2*atan2(sqrt(q2q3tunit[1]^2 + q2q3tunit[2]^2 + q2q3tunit[3]^2), q2q3tunit[0]));
  
  //dotq3q4 = (q3[0]*q4[0]-q3[1]*q4[1]-q3[2]*q4[2]-q3[3]*q4[3]);
  //q3q4t = sin((1.0 - t3) * acos(dotq3q4)) / sin(acos(dotq3q4)) * q3 + sin((t3 * acos(dotq3q4))) / sin(acos(dotq3q4)) * q4;
  //q3q4t = 1/sqrt(q3q4t[0]^2 + q3q4t[1]^2 + q3q4t[2]^2 + q3q4t[3]^2) * q3q4t;
  //Kqs > cos(2*atan2(sqrt(q3q4t[1]^2 + q3q4t[2]^2 + q3q4t[3]^2), q3q4t[0]));
  
  
  
  
end
