/*
 * redundant_workspace.cpp
 *
 *  Created on: Jan 11, 2019
 *      Author: jpickard
 */

#include "ibex.h"
#include "vibes.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <utility>
#include "LinearAlgebra.h"
#include <ctime>
#include <stdlib.h>
#include "Parameters.h"
#include <sstream>

#if defined(_OPENMP)
#include <omp.h>
#endif

using namespace kcadl;

//int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, const char* symbol);
int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol);
void copy_vars(const ibex::Array<const ibex::ExprSymbol> &variable_list1, ibex::IntervalVector &box1,
		const ibex::Array<const ibex::ExprSymbol> &variable_list2, ibex::IntervalVector &box2);
std::pair<ibex::IntervalVector,ibex::IntervalVector> bisect_at(ibex::IntervalVector &Vector, int index);
int max_diam_index(ibex::IntervalVector &Vector);

int verify_shoulder_axial_rotation(ibex::System &axial_system, ibex::CtcCompo &axial_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, ibex::Interval &psi,
		int verbose);
int verify_swing(ibex::IntervalVector &thetaswing, ibex::IntervalVector &phiswing,
		ibex::System &swing_system, ibex::CtcCompo &swing_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box,
		int verbose);
int kinematics_existence(ibex::System &system_kinematics, ibex::IntervalVector &kinematics_box, int verbose);

static void show_usage(std::string name)
{
    std::cerr << "Usage: "
              << "Options:\n"
              << "\t-h,--help       \t Show this help message\n"
              << "\t-t,--trajectory \t Specify the start and finish trajectory parameter values (t in [0,1])\n"
			  << "\t-sl,--slices    \t Specify the number of trajectory slices to compute\n"
			  << "\t-ri             \t Specify the resolution of inner loop\n"
			  << "\t-ro             \t Specify the resolution of outer loop angles and trajectory\n"
			  << "\t-i              \t Specify the input file for additional refinement\n"
              << std::endl;
}

struct Agruments{
	double tmin = 0;
	double tmax = 1;
	bool use_sliced_trajectory = false;
	int trajectory_slices = 0;
	double resolution_inner = 0.1;
	double resolution_outer_angles = 0.01;
	double resolution_outer_trajectory = 0.01;
	bool use_input_file = false;
	std::string input_file_name = "";
};

Agruments read_arguments(int argc, char **argv){
	Agruments agruments;

	int argc_index=1;
	while (argc_index<=argc)
	{
		try{
			std::string arg = argv[argc_index];
			if ((arg == "-h") || (arg == "--help")) {
				show_usage(argv[0]);
				std::exit(EXIT_SUCCESS);
			} else if (arg == "-t" || arg == "--trajectory"){
				try{
					if (argc >= argc_index + 3){
						agruments.tmin = atof(argv[argc_index + 1]);
						agruments.tmax = atof(argv[argc_index + 2]);
						argc_index += 2;

						if (agruments.tmin < 0 || agruments.tmin > agruments.tmax || agruments.tmin > 1 || agruments.tmax < 0 || agruments.tmax > 1){
							std::cerr << "Error in -t parameters. Values must be in the range [0,1]. E.g. -t 0 0.5" << std::endl;
							std::exit(EXIT_FAILURE);
						}
					} else {
						std::cerr << "Error in -t parameters. Values must be in the range [0,1]. E.g. -t 0 0.5" << std::endl;
						std::exit(EXIT_FAILURE);
					}
				} catch(...){
					std::cerr << "Arguments are incorrect. See help: --help" << std::endl;
					std::exit(EXIT_FAILURE);
				}
			} else if (arg == "-sl" || arg == "--slices"){
				try{
					if (argc >= argc_index + 2){
						agruments.trajectory_slices = atoi(argv[argc_index + 1]);
						argc_index += 1;
						agruments.use_sliced_trajectory = true;
					} else {
						std::cerr << "-sl arguments are incorrect. See help: --help" << std::endl;
						std::exit(EXIT_FAILURE);
					}
				} catch(...){
					std::cerr << "-sl arguments are incorrect. See help: --help" << std::endl;
					std::exit(EXIT_FAILURE);
				}
			} else if (arg == "-ri"){
				try{
					if (argc >= argc_index + 2){
						agruments.resolution_inner = atof(argv[argc_index + 1]);
						argc_index += 1;
					} else {
						std::cerr << "-ri arguments are incorrect. See help: --help" << std::endl;
						std::exit(EXIT_FAILURE);
					}
				} catch(...){
					std::cerr << "-ri arguments are incorrect. See help: --help" << std::endl;
					std::exit(EXIT_FAILURE);
				}
			} else if (arg == "-ro"){
				try{
					if (argc >= argc_index + 3){
						agruments.resolution_outer_angles = atof(argv[argc_index + 1]);
						agruments.resolution_outer_trajectory = atof(argv[argc_index + 2]);
						argc_index += 2;
					} else {
						std::cerr << "-ro arguments are incorrect. See help: --help" << std::endl;
						std::exit(EXIT_FAILURE);
					}
				} catch(...){
					std::cerr << "-ro arguments are incorrect. See help: --help" << std::endl;
					std::exit(EXIT_FAILURE);
				}
			} else if (arg == "-i"){
				try{
					if (argc >= argc_index + 2){
						agruments.input_file_name = argv[argc_index + 1];
						agruments.use_input_file = true;
						argc_index += 1;
					} else {
						std::cerr << "-i arguments are incorrect. See help: --help" << std::endl;
						std::exit(EXIT_FAILURE);
					}
				} catch(...){
					std::cerr << "-i arguments are incorrect. See help: --help" << std::endl;
					std::exit(EXIT_FAILURE);
				}
			} else {
				std::cerr << "Arguments are incorrect. See help: --help" << std::endl;
				std::exit(EXIT_FAILURE);
			}
		} catch(...){
			std::cerr << "" << std::endl;
		}
		argc_index++;
	}

	return agruments;
}

int main(int argc, char **argv) {

	// Check input options
	Agruments agruments = read_arguments(argc, argv);
	std::cout << "Using fast solver" << std::endl;
	std::cout << "Using t=[" << agruments.tmin << ',' << agruments.tmax << ']' << std::endl;
	std::cout << "Using ro=" << agruments.resolution_outer_angles << ',' << agruments.resolution_outer_trajectory << std::endl;
	std::cout << "Using ri=" << agruments.resolution_inner << std::endl;

	// Verbose flag (-1: none, 0: warnings only, 1: general messages, 2: more information)
	int verbose = 0;

	// Setup OpenMP
	int num_threads = 1;
#if defined(_OPENMP)
	omp_set_num_threads(8);
	num_threads = omp_get_num_threads();
#endif

	// Clear directory for new list
	int dir_clean = system("exec rm -r manifold/list/*");

	// File for exporting solution results
	int mk_dir = system("mkdir manifold");
	std::string fileExport("manifold/workspace.txt");
	// Clear fileExport
	std::ofstream ofs;
	ofs.open(fileExport, std::ofstream::out | std::ofstream::trunc);
	ofs.close();

	/* ################################################
	LOAD SYSTEMS FROM MINIBEX FILES
	################################################ */
	enum{
		kinematics=0,
		shoulder_axial,
		shoulder_swing,
		hand_swing
	};
	std::vector<const char*> system_filenames;
	system_filenames.push_back("examples/Human-Arm/minibex/kinematics_ZYX_trajectory2.mbx");
//	system_filenames.push_back("examples/Human-Arm/minibex/shoulder_axial.mbx");
//	system_filenames.push_back("examples/Human-Arm/minibex/shoulder_swing.mbx");
//	system_filenames.push_back("examples/Human-Arm/minibex/hand_swing.mbx");

	// Load system to list
	std::vector<ibex::System> system_list;
	for (int system_i=0; system_i<system_filenames.size(); system_i++){
		// Filename
		const char * filename = system_filenames[system_i];

		// Create system
		try{
			ibex::System system(filename);
			system_list.push_back(system);
		} catch (...){
			std::cerr << "Error in minibex file: " << filename << std::endl;
			return 0;
		}
	}

	// Shoulder quaternion constraints (To approximate Wang et al. 1998.)
	int num_shoulder_points = 18;
	double _shoulderphiswing[num_shoulder_points] = {-M_PI, -2.047099962, -1.570796327, -.8868510796, -.7439121072, -.6839452473, -.3872228929, 0., .2088928627, .6224288550, .6938260469, .8868510796, 1.570796327, 2.254741574, 2.356194490, 2.563319152, 2.780739254, M_PI};
	double _shoulderthetaswing[num_shoulder_points] = {.9995080490, .8345071701, .7853981634, .9995080490, 1.742730040, 1.900344531, 2.069514319, 2.112549334, 2.091560103, 2.069514319, 1.742730040, .9995080490, .7853981634, .9995080490, 1.107148718, 1.398862614, 1.170944762, .9995080490};
	ibex::IntervalVector shoulderthetaswing = ibex::IntervalVector(ibex::Vector(num_shoulder_points, _shoulderthetaswing));
	ibex::IntervalVector shoulderphiswing = ibex::IntervalVector(ibex::Vector(num_shoulder_points, _shoulderphiswing));

	// Wrist quaternion constraints (To approximate Gehrmann at al. 2008.)
	int num_wrist_points = 9;
	double _wristphiswing[num_wrist_points] = {-M_PI, -2.356194490192345, -1.570796326794897, -0.785398163397448, 0 , 0.785398163397448, 1.570796326794897, 2.356194490192345, M_PI};
	double _wristthetaswing[num_wrist_points] = {0.698131700797732, 0.698131700797732, 0.698131700797732, 0.698131700797732, 0.698131700797732, 0.523598775598299, 0.785398163397448, 1.396263401595464, 0.698131700797732};
	ibex::IntervalVector wristthetaswing = ibex::IntervalVector(ibex::Vector(num_wrist_points, _wristthetaswing));
	ibex::IntervalVector wristphiswing = ibex::IntervalVector(ibex::Vector(num_wrist_points, _wristphiswing));

	/* ################################################
	EVALUATE SWIVEL-TWIST WORKSPACE
	################################################ */
	// Visualize results - plot kinematic chain
	vibes::beginDrawing();
	vibes::newFigure("Allowable Psi-Phi (Swivel-Twist)");
	vibes::axisLimits(-M_PI/2-1,3*M_PI/2+1,-M_PI-1,M_PI+1);
	vibes::setFigureProperty("width",600);
	vibes::setFigureProperty("height",600);

	// Redundant angles
	ibex::IntervalVector outerbox(3);
	outerbox[0] = ibex::Interval(-M_PI/2,3*M_PI/2); // swivel angle
	outerbox[1] = ibex::Interval(-M_PI,M_PI); // twist angle
	outerbox[2] = ibex::Interval(agruments.tmin,agruments.tmax); // trajectory parameter
//	std::deque<ibex::IntervalVector> outerbox_list;
//	outerbox_list.push_back(outerbox);

	//  Redundant angles bisector
	ibex::Vector precisions(outerbox.size());
	precisions[0] = agruments.resolution_outer_angles;
	precisions[1] = agruments.resolution_outer_angles;
	precisions[2] = agruments.resolution_outer_trajectory;
	ibex::LargestFirst outerbox_bisector(precisions, 0.5);

	// Variables not to be bisected.
	std::vector<std::string> do_not_bisect_list = 	{"d3",
													"d5",
													"d8",
													"phi",
													"psi",
													"t",
													"tX",
													"tY",
													"tZ"};

	// Solver
	std::deque<Parameters> parameters_list;
	bool repeat = true;
#pragma omp parallel shared(parameters_list, repeat)
{
	int thread_id = 0;
#if defined(_OPENMP)
		thread_id = omp_get_thread_num();
		if (verbose > 0) std::cout << "thread: " << thread_id << std::endl;
#endif

	// SYSTEM
	// Copy systems for each thread
	ibex::System system_kinematics(system_list[kinematics], ibex::System::COPY);
//	ibex::System system_shoulder_axial(system_list[shoulder_axial], ibex::System::COPY);
//	ibex::System system_shoulder_swing(system_list[shoulder_swing], ibex::System::COPY);
//	ibex::System system_hand_swing(system_list[hand_swing], ibex::System::COPY);

	// CONTRACTORS
	// KINEMATICS //
	// Build contractors (rebuilding in loop improves contraction performance)
	ibex::CtcHC4 hc4_kinematics(system_kinematics,0.01);
	ibex::CtcHC4 hc4acid_kinematics(system_kinematics,0.1,true);
	ibex::CtcAcid acid_kinematics(system_kinematics, hc4acid_kinematics);
	ibex::CtcCompo kinematics_contractor(hc4_kinematics,acid_kinematics);

//	// SHOULDER AXIAL ROTATION //
//	// Build contractors
//	ibex::CtcHC4 hc4_shoulder_axial(system_shoulder_axial,0.01);
//	ibex::CtcHC4 hc4acid_shoulder_axial(system_shoulder_axial,0.1,true);
//	ibex::CtcAcid acid_shoulder_axial(system_shoulder_axial, hc4acid_shoulder_axial);
//	ibex::CtcCompo shoulder_axial_contractor(hc4_shoulder_axial,acid_shoulder_axial);
//
//	// SHOULDER SWING //
//	// Build contractors
//	ibex::CtcHC4 hc4_shoulder_swing(system_shoulder_swing,0.01);
//	ibex::CtcHC4 hc4acid_shoulder_swing(system_shoulder_swing,0.1,true);
//	ibex::CtcAcid acid_shoulder_swing(system_shoulder_swing, hc4acid_shoulder_swing);
//	ibex::CtcCompo shoulder_swing_contractor(hc4_shoulder_swing,acid_shoulder_swing);
//
//	// HAND SWING //
//	// Build contractors
//	ibex::CtcHC4 hc4_hand_swing(system_hand_swing,0.01);
//	ibex::CtcHC4 hc4acid_hand_swing(system_hand_swing,0.1,true);
//	ibex::CtcAcid acid_hand_swing(system_hand_swing, hc4acid_hand_swing);
//	ibex::CtcCompo hand_swing_contractor(hc4_hand_swing,acid_hand_swing);

	// VARIABLE LOOKUP TABLE //
	int system_kinematics_psi = find_var_index(system_kinematics.f_ctrs.args(), "psi");
	int system_kinematics_phi = find_var_index(system_kinematics.f_ctrs.args(), "phi");
	int system_kinematics_t = find_var_index(system_kinematics.f_ctrs.args(), "t");
	int system_kinematics_INT = find_var_index(system_kinematics.f_ctrs.args(), "INT");
	int system_kinematics_EXT = find_var_index(system_kinematics.f_ctrs.args(), "EXT");
	int system_kinematics_rx4 = find_var_index(system_kinematics.f_ctrs.args(), "rx4");
	int system_kinematics_ry4 = find_var_index(system_kinematics.f_ctrs.args(), "ry4");
	int system_kinematics_rz4 = find_var_index(system_kinematics.f_ctrs.args(), "rz4");
	int system_kinematics_Rwx5 = find_var_index(system_kinematics.f_ctrs.args(), "Rwx5");
	int system_kinematics_Rwy5 = find_var_index(system_kinematics.f_ctrs.args(), "Rwy5");
	int system_kinematics_Rwz5 = find_var_index(system_kinematics.f_ctrs.args(), "Rwz5");
	int system_kinematics_tX = find_var_index(system_kinematics.f_ctrs.args(), "tX");
	int system_kinematics_tY = find_var_index(system_kinematics.f_ctrs.args(), "tY");
	int system_kinematics_tZ = find_var_index(system_kinematics.f_ctrs.args(), "tZ");
	int system_kinematics_rx7 = find_var_index(system_kinematics.f_ctrs.args(), "rx7");
	int system_kinematics_ry7 = find_var_index(system_kinematics.f_ctrs.args(), "ry7");
	int system_kinematics_rz7 = find_var_index(system_kinematics.f_ctrs.args(), "rz7");
	int system_kinematics_d3 = find_var_index(system_kinematics.f_ctrs.args(), "d3");
	int system_kinematics_d8 = find_var_index(system_kinematics.f_ctrs.args(), "d8");


	std::vector<int> do_not_bisect_list_indices;
	for (int var_i=0; var_i<do_not_bisect_list.size(); var_i++)
		do_not_bisect_list_indices.push_back(find_var_index(system_kinematics.f_ctrs.args(), do_not_bisect_list[var_i]));

	// Initialise parameter search space (init new or init from input file)
	if (thread_id==0){
		if (agruments.use_input_file){
			std::ifstream source; // build a read-Stream
			source.open(agruments.input_file_name, std::ifstream::in); // open data
			std::ofstream ofs_; // output file
			ofs_.open(fileExport, std::ofstream::out | std::ofstream::app);
			for(std::string line; std::getline(source, line); )   //read stream line by line
			{
				std::istringstream in(line);      //make a stream for the line itself
				double swivel_min, swivel_max, twist_min, twist_max, t_min, t_max;
				int classification;
				in >> swivel_min
				   >> swivel_max
				   >> twist_min
				   >> twist_max
				   >> t_min
				   >> t_max
				   >> classification;      //now read the whitespace-separated data

				// Write inside and outside data to output file
				if (classification == 1 || classification == -1){
					ofs_ << swivel_min << '\t' <<
							swivel_max << '\t' <<
							twist_min << '\t' <<
							twist_max << '\t' <<
							t_min << '\t' <<
							t_max << '\t' <<
							classification << std::endl;
					if (classification == 1)
						vibes::drawBox(swivel_min, swivel_max, twist_min, twist_max, "b[b]");
					else if (classification == -1)
						vibes::drawBox(swivel_min, swivel_max, twist_min, twist_max, "r[r]");
				} else
				{ // Append boundary data to parameters_list
					Parameters init_parameters;
					ibex::IntervalVector outerbox_(3);
					outerbox_[0] = ibex::Interval(swivel_min, swivel_max); // swivel angle
					outerbox_[1] = ibex::Interval(twist_min, twist_max); // twist angle
					outerbox_[2] = ibex::Interval(t_min, t_max); // trajectory parameter
					// Bisect outerbox_ then add to list
					std::pair<ibex::IntervalVector,ibex::IntervalVector> bisected = outerbox_bisector.bisect(outerbox_);
					init_parameters.outer = bisected.first;
					init_parameters.inner = system_kinematics.box;
					parameters_list.push_back(init_parameters);
					init_parameters.outer = bisected.second;
					parameters_list.push_back(init_parameters);
				}
			}
			ofs_.close();

		} else{
			// Check if trajectory should be sliced
			if (agruments.use_sliced_trajectory){
				double slice_spacing = (agruments.tmax - agruments.tmin) / (agruments.trajectory_slices - 1);
				for (int slice=0; slice < agruments.trajectory_slices; slice++){
					Parameters init_parameters;
					init_parameters.outer = outerbox;
					init_parameters.outer[2] = agruments.tmin + slice_spacing * static_cast<double>(slice);
					init_parameters.inner = system_kinematics.box;
					parameters_list.push_back(init_parameters);
				}

			} else {
				Parameters init_parameters;
				init_parameters.outer = outerbox;
				init_parameters.inner = system_kinematics.box;
				parameters_list.push_back(init_parameters);
			}
		}
	}

	while(repeat){
		if(parameters_list.size()>0 && thread_id < parameters_list.size()){
			// Pop parameters from list
			ibex::IntervalVector outerbox_(1);
			ibex::IntervalVector kinematics_box(1);
	#pragma omp critical /* Threads update by turns */
			{
				Parameters parameters = parameters_list.front();
				outerbox_ = parameters.outer;
				kinematics_box = parameters.inner;
				if (verbose > 0) std::cout << "thread: " << thread_id << " outerbox_list.size(): " << parameters_list.size() << std::endl;
				if (verbose > 0) std::cout << "outerbox_ " << outerbox_ << std::endl;
				if (verbose > 0) std::cout << "kinematics_box " << kinematics_box << std::endl;
				parameters_list.pop_front();
			}

			// KINEMATICS//
//			ibex::IntervalVector kinematics_box = system_kinematics.box;
			kinematics_box[system_kinematics_psi] = outerbox_[0];
			kinematics_box[system_kinematics_phi] = outerbox_[1];
			kinematics_box[system_kinematics_t] = outerbox_[2];

			// List to store kinematics variables
			std::vector<ibex::IntervalVector> kinematic_list;
			kinematic_list.clear();
			kinematic_list.push_back(kinematics_box);
			std::vector<ibex::IntervalVector> remaining_kinematic_list;
			ibex::IntervalVector outerbox_simp(3);
			// Vector used for bisection (some elements are set to zero to avoid bisection)
			ibex::IntervalVector kinematics_box_bisection(kinematics_box.size());
			int boundary_size = 0;
			int solution_size = 0;
			int num_bisections = 0;
			int num_nosolutions = 0;
			int kinematics_verified = 0;
			int shoulder_axial_rotation_verified = 0;
			int shoulder_swing_verified = 0;
			int hand_swing_verified = 0;
			bool verify_shoulder_axial = true;
			bool verify_shoulder_swing = true;
			bool verify_hand_swing = true;
			int classification = 0;
			int bisect_index;

			// VERIFICATION LOOP //
			while(kinematic_list.size()>0 && num_bisections<5 && boundary_size<1){// && solution_size<1){
				/* ################################################
				CHECK VERIFICATION OF KINEMATICS
				################################################ */
				kinematics_verified = 0;
				// Contract
				ibex::IntervalVector precontract = kinematic_list.back();
				kinematics_contractor.contract(kinematic_list.back());
				double maxcontractdiff = (precontract.diam() - kinematic_list.back().diam()).max();
				double maxcontractratio = maxcontractdiff / precontract.diam().max();
				if (std::isnan(maxcontractratio))
					maxcontractratio = 1;

				// Make sure not empty (empty means no kinematic solution)
				if (kinematic_list.back().is_empty()){
					if (verbose > 0) std::cout << "kinematics_box empty" << std::endl;
					kinematics_verified = -2; // treated differently from following constraints
				} else {
					if (maxcontractratio > 0.01){// && maxcontractdiff > 0.05){
						// Check if phi and psi were modified by simplification (TODO: prevent this in the contractions)
						outerbox_simp[0] = kinematic_list.back()[system_kinematics_psi];
						outerbox_simp[1] = kinematic_list.back()[system_kinematics_phi];
						outerbox_simp[2] = kinematic_list.back()[system_kinematics_t];
						if (outerbox_simp==outerbox_)
							kinematics_verified = kinematics_existence(system_kinematics, kinematic_list.back(), verbose);
						else{
							if (verbose > 0) std::cout << "kinematics_box simplified angles" << std::endl;
							kinematics_verified = 0;
						}
					} else{
						std::cout << "kinematics_box contraction not effective enough -- bisect" << std::endl;
						kinematics_verified = 0;
					}
				}

				/* ################################################
				CHECK VERIFICATION OF CONSTRAINTS
				################################################ */
				shoulder_axial_rotation_verified = 0;
				shoulder_swing_verified = 0;
				hand_swing_verified = 0;

				// SHOULDER AXIAL ROTATION //
				if (verify_shoulder_axial && kinematics_verified >= 0){
					if (verbose > 0) std::cout << "SHOULDER AXIAL ROTATION" << std::endl;
					ibex::Interval psi = kinematic_list.back()[system_kinematics_psi];
					ibex::Interval INT = kinematic_list.back()[system_kinematics_INT];
					ibex::Interval EXT = kinematic_list.back()[system_kinematics_EXT];
					ibex::Interval rx4 = kinematic_list.back()[system_kinematics_rx4];
					ibex::Interval ry4 = kinematic_list.back()[system_kinematics_ry4];
					ibex::Interval rz4 = kinematic_list.back()[system_kinematics_rz4];
					ibex::Interval alpha = atan2(rx4, -ry4);
					ibex::Interval beta = atan2(rz4, sqrt(sqr(rx4)+sqr(ry4)));
					if (alpha.ub() < -1.7453 || alpha.lb() > 2.2689 || beta.ub() < -1.3963 || beta.lb() > 1.3963){
						// alpha, beta limits dissatisfied
						if (verbose > 0) std::cout << "shoulder_axial not valid: alpha/beta range" << std::endl;
						shoulder_axial_rotation_verified = -1;
					} else {
						ibex::Interval A0 = alpha*cos(beta);
						ibex::Interval A1 = sqr(alpha)*sqr(cos(beta));
						ibex::Interval A2 = pow(alpha,3)*pow(cos(beta),3);
						ibex::Interval A3 = pow(alpha,4)*pow(cos(beta),4);
						EXT = M_PI + M_PI/180 * (0.724559*pow(beta,4)+(11.73726*A0-1.535318)*pow(beta,3)+(-5.683932*A1+23.970675*A0-7.366772)*sqr(beta)+(-4.558201*A2-2.565833*A1+29.532220*A0-15.71531)*beta+.331*A3-2.987*A2+3.428*A1+9.299*A0-2.459);
						INT = M_PI + M_PI/180 * (3.417002*pow(beta,3)+(20.882835*A0-2.676168)*sqr(beta)+(-.143589*A1+29.671092*A0-41.687220)*beta-2.081*A2+4.092*A1+18.652*A0-139.27);

						if (verbose > 1) std::cout << "axial_rotation " << INT << '\t' << psi << '\t' << EXT << std::endl;
						if (psi.ub() < EXT.lb() && psi.lb() > INT.ub() &&
								alpha.lb() >= -1.7453 && alpha.ub() <= 2.2689 && beta.lb() >= -1.3963 && beta.ub() <= 1.3963){
							// alpha, beta limits and INT, EXT limits satisfied
							if (verbose > 0) std::cout << "shoulder_axial valid" << std::endl;
							shoulder_axial_rotation_verified = 1;
						} else if (psi.lb() > EXT.ub() || psi.ub() < INT.lb()){
							if (verbose > 0) std::cout << "shoulder_axial not valid" << std::endl;
							shoulder_axial_rotation_verified = -1;
						} else{
							// alpha, beta limits or INT, EXT limits partially satisfied
							if (verbose > 0) std::cout << "shoulder_axial partially valid" << std::endl;
							shoulder_axial_rotation_verified = 0;
						}
					}
//					shoulder_axial_rotation_verified = verify_shoulder_axial_rotation(system_shoulder_axial,
//						shoulder_axial_contractor, system_kinematics, kinematic_list.back(), outerbox_[0], verbose);
				} else {
					// Default value
					shoulder_axial_rotation_verified = 1;
				}

				// SHOULDER SWING //
				if (verify_shoulder_swing && kinematics_verified >= 0 && shoulder_axial_rotation_verified >= 0) {
					// SHOULDER SWING //
					if (verbose > 0) std::cout << "SHOULDER SWING" << std::endl;
					ibex::Interval ry4 = kinematic_list.back()[system_kinematics_ry4];
					ibex::Interval d3 = kinematic_list.back()[system_kinematics_d3];
					if ((-ry4/d3).lb() > cos(1)) shoulder_swing_verified = 1;
					else if ((-ry4/d3).ub()< cos(1)) shoulder_swing_verified = -1;
					else shoulder_swing_verified = 0;
//					shoulder_swing_verified = verify_swing(shoulderthetaswing, shoulderphiswing,
//							system_shoulder_swing, shoulder_swing_contractor,
//							system_kinematics, kinematic_list.back(), verbose);
				} else {
					// Default value
					shoulder_swing_verified = 1;
				}

				// HAND SWING //
				if (verify_hand_swing && kinematics_verified >= 0 && shoulder_axial_rotation_verified >= 0 && shoulder_swing_verified >= 0) {
					// HAND SWING //
					if (verbose > 0) std::cout << "HAND SWING" << std::endl;
					ibex::Interval Rwx5 = kinematic_list.back()[system_kinematics_Rwx5];
					ibex::Interval Rwy5 = kinematic_list.back()[system_kinematics_Rwy5];
					ibex::Interval Rwz5 = kinematic_list.back()[system_kinematics_Rwz5];
					ibex::Interval tX = kinematic_list.back()[system_kinematics_tX];
					ibex::Interval tY = kinematic_list.back()[system_kinematics_tY];
					ibex::Interval tZ = kinematic_list.back()[system_kinematics_tZ];
					ibex::Interval rx7 = kinematic_list.back()[system_kinematics_rx7];
					ibex::Interval ry7 = kinematic_list.back()[system_kinematics_ry7];
					ibex::Interval rz7 = kinematic_list.back()[system_kinematics_rz7];
					ibex::Interval d8 = kinematic_list.back()[system_kinematics_d8];
					if (((Rwx5*(tX-rx7)+Rwy5*(tY-ry7)+Rwz5*(tZ-rz7))/d8).lb() > cos(1)) hand_swing_verified = 1;
					else if (((Rwx5*(tX-rx7)+Rwy5*(tY-ry7)+Rwz5*(tZ-rz7))/d8).ub() < cos(1)) hand_swing_verified = -1;
					else hand_swing_verified = 0;
//					hand_swing_verified = verify_swing(wristthetaswing, wristphiswing,
//							system_hand_swing, hand_swing_contractor,
//							system_kinematics, kinematic_list.back(), verbose);
				} else {
					// Default value
					hand_swing_verified = 1;
				}

				// Determine classification
				if (kinematics_verified == -2)
					classification = -2;
				else if (kinematics_verified == 1 &&
						shoulder_axial_rotation_verified == 1 &&
						shoulder_swing_verified ==1 &&
						hand_swing_verified == 1)
					classification = 1;
				else if (kinematics_verified == -1 ||
						shoulder_axial_rotation_verified == -1 ||
						shoulder_swing_verified == -1 ||
						hand_swing_verified == -1)
					classification = -1;
				else
					classification = 0;

				// BISECTION
				if (classification == -2){
					// There does not exist a solution for the kinematics (does not count towards final classification)
					if (verbose > 0) std::cout << "NO SOLUTION - kinematics empty" << std::endl;
					kinematic_list.pop_back();
				}else if (classification == -1){
					// There does not exist a solution for all of the constraints (does not count towards final classification)
					if (verbose > 0) std::cout << "NO SOLUTION - kinematics" << std::endl;
					num_nosolutions++;
					kinematic_list.pop_back();
				}else if (classification == 1){
					if (verbose > 0) std::cout << "SOLUTION - kinematics" << std::endl;
					solution_size++;
					remaining_kinematic_list.push_back(kinematic_list.back());
					kinematic_list.pop_back();
				}else if (classification == 0){
					// bisect kinematics_box along rotation matrix elements
					kinematics_box_bisection = kinematic_list.back();
					// Variables not to be bisected.
					for (int var_i=0; var_i<do_not_bisect_list_indices.size(); var_i++)
						kinematics_box_bisection[do_not_bisect_list_indices[var_i]] = 0;

					if (kinematics_box_bisection.max_diam() > agruments.resolution_inner){
						bisect_index = max_diam_index(kinematics_box_bisection);
						std::pair<ibex::IntervalVector,ibex::IntervalVector> bisected = bisect_at(kinematic_list.back(), bisect_index);
						kinematic_list.pop_back();
						// append bisected to list
						kinematic_list.push_back(bisected.first);
						kinematic_list.push_back(bisected.second);
						num_bisections++;
						if (verbose > 1) std::cout << "BISECT - kinematics: " << system_kinematics.f_ctrs.args()[bisect_index] << '\t' << kinematics_box_bisection[bisect_index] << std::endl;
					} else {
						if (verbose > 0) std::cout << "BOUNDARY - kinematics" << std::endl;
						boundary_size++;
						remaining_kinematic_list.push_back(kinematic_list.back());
						kinematic_list.pop_back();
					}
				}
			}

			// Add any remaining kinematic boxes to boundary list
			if (verbose >= 0) std::cout << "Number of solutions: " << solution_size << std::endl;
			if (verbose >= 0) std::cout << "Number of boundaries: " << boundary_size << std::endl;
			if (verbose >= 0) std::cout << "Number of unclassified: " << kinematic_list.size() << std::endl;
			if (verbose >= 0) std::cout << "Number of no solution: " << num_nosolutions << std::endl;
			if (verbose >= 0) std::cout << "outerbox_ " << outerbox_ << std::endl;

			/* ################################################
			DETERMINE CLASSIFICATION
			################################################ */
			// Determine box classification
			if (solution_size >= 1 && boundary_size == 0 && kinematic_list.size() == 0 && num_nosolutions == 0) // TODO: SAFE - getting solution_size>=1 only may require ensuring contractors do not contract constant variables
				classification = 1;
			else if(solution_size == 0 && boundary_size == 0 && kinematic_list.size() == 0 && num_nosolutions >= 0)
				classification = -1; // OUTSIDE
			else
				classification = 0; // BOUNDARY

			/* ################################################
			APPLY BISECTION
			################################################ */
			#pragma omp critical /* Threads update by turns */
			{
				vibes::drawBox(outerbox_[0].lb(), outerbox_[0].ub(), outerbox_[1].lb(), outerbox_[1].ub(), "k");
			}
			if (classification == 1){
				std::ofstream ofs_;
				ofs_.open(fileExport, std::ofstream::out | std::ofstream::app);
				ofs_ << outerbox_[0].lb() << '\t' <<
						outerbox_[0].ub() << '\t' <<
						outerbox_[1].lb() << '\t' <<
						outerbox_[1].ub() << '\t' <<
						outerbox_[2].lb() << '\t' <<
						outerbox_[2].ub() << '\t' <<
						classification << std::endl;
				ofs_.close();
				if (verbose >= 0) std::cout << "SOLUTION - outerbox" << std::endl;
	#pragma omp critical /* Threads update by turns */
				{
					vibes::drawBox(outerbox_[0].lb(), outerbox_[0].ub(), outerbox_[1].lb(), outerbox_[1].ub(), "b[b]");
				}
			}else if (classification == -1){
				std::ofstream ofs_;
				ofs_.open(fileExport, std::ofstream::out | std::ofstream::app);
				ofs_ << outerbox_[0].lb() << '\t' <<
						outerbox_[0].ub() << '\t' <<
						outerbox_[1].lb() << '\t' <<
						outerbox_[1].ub() << '\t' <<
						outerbox_[2].lb() << '\t' <<
						outerbox_[2].ub() << '\t' <<
						classification << std::endl;
				ofs_.close();
				if (verbose >= 0) std::cout << "NO SOLUTION - outerbox" << std::endl;
	#pragma omp critical /* Threads update by turns */
				{
					vibes::drawBox(outerbox_[0].lb(), outerbox_[0].ub(), outerbox_[1].lb(), outerbox_[1].ub(), "r[r]");
				}
			}else if (classification == 0){
				// bisect outerbox_
				try {
					// apply test bisection to check precision
					outerbox_bisector.bisect(outerbox_);
					// multiply trajectory and perform real bisection
					int trajectory_multiplier = 10;
					outerbox_[2] = outerbox_[2] * trajectory_multiplier; // scale trajectory to force its bisection first
					std::pair<ibex::IntervalVector,ibex::IntervalVector> bisected = outerbox_bisector.bisect(outerbox_);
					bisected.first[2] = bisected.first[2] / trajectory_multiplier;
					bisected.second[2] = bisected.second[2] / trajectory_multiplier;
	#pragma omp critical /* Threads update by turns */
					{
						// append bisected to list
//						outerbox_list.push_back(bisected.first);
//						outerbox_list.push_back(bisected.second);

						// Union all non-inside/outside kinematics boxes
						// Copy all remaining elements in kinematic_list to remaining_kinematic_list
						while (kinematic_list.size()>0){
							remaining_kinematic_list.push_back(kinematic_list.back());
							kinematic_list.pop_back();
						}

						// Take union of all non-outside kinematics boxes (remaining_kinematic_list)
						ibex::IntervalVector kinematic_box_union = remaining_kinematic_list.back();
						remaining_kinematic_list.pop_back();
						while (remaining_kinematic_list.size()>0){
							kinematic_box_union |= remaining_kinematic_list.back();
							remaining_kinematic_list.pop_back();
						}

						// append bisected to list
						Parameters parameters1;
						parameters1.outer = bisected.first;
						parameters1.inner = kinematic_box_union;
						parameters_list.push_back(parameters1);
						Parameters parameters2;
						parameters2.outer = bisected.second;
						parameters2.inner = kinematic_box_union;
						parameters_list.push_back(parameters2);
					}
					if (verbose >= 0) std::cout << "BISECT - outerbox" << std::endl;
				} catch (...) {
					std::ofstream ofs_;
					ofs_.open(fileExport, std::ofstream::out | std::ofstream::app);
					ofs_ << outerbox_[0].lb() << '\t' <<
							outerbox_[0].ub() << '\t' <<
							outerbox_[1].lb() << '\t' <<
							outerbox_[1].ub() << '\t' <<
							outerbox_[2].lb() << '\t' <<
							outerbox_[2].ub() << '\t' <<
							classification << std::endl;
					ofs_.close();
					if (verbose >= 0) std::cout << "BOUNDARY - outerbox" << std::endl;
	#pragma omp critical /* Threads update by turns */
					{
						vibes::drawBox(outerbox_[0].lb(), outerbox_[0].ub(), outerbox_[1].lb(), outerbox_[1].ub(), "y[y]");
					}
				}
			}
		} else {
			// Check if outerbox_list.size()==0 for all threads
			if (thread_id==0)
				repeat = false;
			else
				sleep(1);
		}
	}
}
	return 0;
}


int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol){
	const char *cstr = symbol.c_str();
	for (int var_i=0; var_i<variable_list.size(); var_i++){
//		std::cout << variable_list[var_i].name << '\t' << symbol << std::endl;
		if (strcmp(variable_list[var_i].name, cstr) == 0){
//			std::cout << "Found index: " << var_i << std::endl;
			return var_i;
		}
	}
	return -1;
}

/*
 * Update box 2 with the values from box1 by matching the variables.
 */
void copy_vars(const ibex::Array<const ibex::ExprSymbol> &variable_list1, ibex::IntervalVector &box1,
		const ibex::Array<const ibex::ExprSymbol> &variable_list2, ibex::IntervalVector &box2){

	for (int var_i=0; var_i<variable_list1.size(); var_i++){
		for (int var_j=0; var_j<variable_list2.size(); var_j++){
//			std::cout << variable_list1[var_i].name << '\t' << variable_list2[var_j].name << std::endl;
			if (strcmp(variable_list1[var_i].name, variable_list2[var_j].name) == 0){
//				std::cout << "Match found.. updating box2" << std::endl;
				box2[var_j] = box1[var_i];
			}
		}
	}
}

/*
 * Bisect interval vector at index.
 */
std::pair<ibex::IntervalVector,ibex::IntervalVector> bisect_at(ibex::IntervalVector &Vector, int index){

	ibex::IntervalVector Vector1 = Vector;
	Vector1[index] = ibex::Interval(Vector[index].lb(),Vector[index].mid());
	ibex::IntervalVector Vector2 = Vector;
	Vector2[index] = ibex::Interval(Vector[index].mid(),Vector[index].ub());
	std::pair<ibex::IntervalVector,ibex::IntervalVector> pair = std::make_pair(Vector1,Vector2);
	return pair;
}

int max_diam_index(ibex::IntervalVector &Vector){
	ibex::Vector Diams = Vector.diam();
	int max_index = 0;
	double max_diam = Diams[max_index];
	for (int i=1; i<Diams.size(); i++){
		if (Diams[i] > Diams[max_index]){
			max_diam = Diams[i];
			max_index = i;
		}
	}
	return max_index;
}

int verify_shoulder_axial_rotation(ibex::System &axial_system, ibex::CtcCompo &axial_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, ibex::Interval &psi, int verbose){

	ibex::IntervalVector shoulder_axial_box = axial_system.box;
	copy_vars(kinematics_system.f_ctrs.args(), kinematics_box,
			axial_system.f_ctrs.args(), shoulder_axial_box);

	// Evaluate alpha and beta without constraints on limits for comparison with system box after contraction
	ibex::Interval rx4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "rx4")];
	ibex::Interval ry4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "ry4")];
	ibex::Interval rz4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "rz4")];
	ibex::Interval beta = atan2(rz4, sqrt(sqr(rx4)+sqr(ry4)));
	ibex::Interval alpha = atan2(rx4, -ry4);

	// Contract
	axial_contractor.contract(shoulder_axial_box);

	// Check if alpha and beta are not empty
	ibex::Interval alpha_ = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "alpha")];
	ibex::Interval beta_ = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "beta")];

	if (alpha_.is_empty() || beta_.is_empty()){
		if (verbose > 0) std::cout << "shoulder_axial not valid: alpha/beta empty" << std::endl;
		return -1;
	} else if (alpha.ub() < -1.7453 || alpha.lb() > 2.2689 || beta.ub() < -1.3963 || beta.lb() > 1.3963){
		// alpha, beta limits dissatisfied
		if (verbose > 0) std::cout << "shoulder_axial not valid: alpha/beta range" << std::endl;
		return -1;
	} else {
		// Check constraint satisfaction: psi < EXT; psi > INT;
		ibex::Interval EXT = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "EXT")];
		ibex::Interval INT = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "INT")];
		if (verbose > 0) std::cout << "axial_rotation " << INT << '\t' << psi << '\t' << EXT << std::endl;
		if (psi.ub() < EXT.lb() && psi.lb() > INT.ub() &&
				alpha.lb() >= -1.7453 && alpha.ub() <= 2.2689 && beta.lb() >= -1.3963 && beta.ub() <= 1.3963){
			// alpha, beta limits and INT, EXT limits satisfied
			if (verbose > 0) std::cout << "shoulder_axial valid" << std::endl;
			return 1;
		} else if (psi.lb() > EXT.ub() || psi.ub() < INT.lb()){
			if (verbose > 0) std::cout << "shoulder_axial not valid" << std::endl;
			return -1;
		} else{
			// alpha, beta limits or INT, EXT limits partially satisfied
			if (verbose > 0) std::cout << "shoulder_axial partially valid" << std::endl;
			return 0;
		}
	}
}

int verify_swing(ibex::IntervalVector &thetaswing, ibex::IntervalVector &phiswing,
		ibex::System &swing_system, ibex::CtcCompo &swing_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, int verbose){
	int num_inside_classifications = 0;
	int num_outside_classifications = 0;
	int shoulder_swing_pair_verified = 0;
	bool has_boundary = false;
	ibex::IntervalVector swing_box_orig = swing_system.box;
	copy_vars(kinematics_system.f_ctrs.args(), kinematics_box, swing_system.f_ctrs.args(), swing_box_orig);
	for (int pair=0; pair<thetaswing.size()-1; pair++){

		// Loop over t in [0,1] until swing constraint is satisfied for all t or all t are unsatisfied
		std::vector<ibex::Interval> t_list;
		t_list.push_back(ibex::Interval(0,1));
		int num_pair_inside_classifications = 0;
		int num_pair_outside_classifications = 0;
		ibex::IntervalVector swing_box = swing_box_orig;
		ibex::Interval t,t_orig,K,G;
		while(t_list.size()>0 && !has_boundary){
			// Pop t
			t = t_list.back();
			t_list.pop_back();

			// Copy t to check solution existence after filtering
			t_orig = t;

			// Update box
			swing_box = swing_box_orig;

			// Update theta and gamma values for quaternion pair
			swing_box[find_var_index(swing_system.f_ctrs.args(), "theta1")] = thetaswing[pair];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "gamma1")] = phiswing[pair];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "theta2")] = thetaswing[pair+1];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "gamma2")] = phiswing[pair+1];

			// Set t value
			swing_box[find_var_index(swing_system.f_ctrs.args(), "t")] = t;

			// Contract
			swing_contractor.contract(swing_box);

			// Get simplified t value
			t = swing_box[find_var_index(swing_system.f_ctrs.args(), "t")];
			if (verbose > 1) std::cout << "t_orig " << t_orig << " t " << t << '\t' << std::endl;

			// Check constraint G <= K <= 1
			K = swing_box[find_var_index(swing_system.f_ctrs.args(), "K")];
			G = swing_box[find_var_index(swing_system.f_ctrs.args(), "G")];

			if (verbose > 1) std::cout << "swing: " << pair << '\t' << G << '\t' << K << '\t' << 1 << std::endl;

			// Handle empty cases
			if (!K.is_empty() && !G.is_empty()){
				// Determine classification
				if (K.ub() <= 1 && K.lb() >= G.ub()){
					if (verbose > 1) std::cout << "swing pair valid" << std::endl;
					num_pair_inside_classifications++;
				} else if (K.lb() > 1 || K.ub() < G.lb()){
					num_pair_outside_classifications++;
					if (verbose > 1) std::cout << "swing pair not valid: " << std::endl;
				} else{
					if (t.diam()>0.0001){
						t_list.push_back(ibex::Interval(t.lb(),t.mid()));
						t_list.push_back(ibex::Interval(t.mid(),t.ub()));
						if (verbose > 1) std::cout << "BISECTION" << std::endl;
					} else{
						if (verbose > 1) std::cout << "BOUNDARY" << std::endl;
						has_boundary = true;
					}
				}
			}
		}

		if (verbose > 1) std::cout << "num_pair_inside_classifications "  << num_pair_inside_classifications << std::endl;
		if (verbose > 1) std::cout << "num_pair_outside_classifications "  << num_pair_outside_classifications << std::endl;

		if (num_pair_inside_classifications>0 && num_pair_outside_classifications==0 && !has_boundary){
			// swing pair constraint is valid for all t.
			num_inside_classifications++;
			if (verbose > 1) std::cout << "swing pair valid "  << std::endl;
		}
		else if (num_pair_outside_classifications>0  && num_pair_inside_classifications==0 && !has_boundary){
			// swing pair constraint is invalid for some t.
			num_outside_classifications++;
			if (verbose > 1) std::cout << "swing pair not valid " << std::endl;
		} else if (has_boundary || (num_pair_inside_classifications>0 && num_pair_outside_classifications>0)) {
			// if both inside and outside classifications are found then there must exist a boundary (there seems to be a rare
			// case where the bisection does not detect the boundary but does find inside and outside classifications)
			has_boundary = true;
			if (verbose > 1) std::cout << "swing pair partially valid "  << std::endl;
			break;
		} else {
			if (verbose > 1) std::cout << "swing pair empty" << std::endl;
		}

	}

	if (verbose > 0) std::cout << "num_inside_classifications "  << num_inside_classifications << std::endl;
	if (verbose > 0) std::cout << "num_outside_classifications "  << num_outside_classifications << std::endl;
	// Determine swing classification
	if (num_outside_classifications>0 && !has_boundary){
		if (verbose > 0) std::cout << "swing not valid "  << std::endl;
		return -1; // outside
	} else if (num_inside_classifications>0 && !has_boundary){
		if (verbose > 0) std::cout << "swing valid "  << std::endl;
		return 1; // inside
	} else{
		if (verbose > 0) std::cout << "swing partially valid "  << std::endl;
		return 0; // boundary
	}
}

int kinematics_existence(ibex::System &system_kinematics, ibex::IntervalVector &kinematics_box, int verbose){
	// Existence function
	ibex::Interval d3=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "d3")];
	ibex::Interval d5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "d5")];
	ibex::Interval rx5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"rx5")];
	ibex::Interval ry5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"ry5")];
	ibex::Interval rz5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"rz5")];

	ibex::Interval wrist_dist = sqrt(sqr(rx5) + sqr(ry5) + sqr(rz5));
	if (verbose > 1) std::cout << "wrist_dist: " << wrist_dist << std::endl;
	if (verbose > 1) std::cout << "d3 + d5: " << d3 + d5 << std::endl;
	if (wrist_dist.ub() < (d3 + d5).lb()){
		return 1;
	} else if (wrist_dist.lb() > (d3 + d5).ub()){
		return -1;
	} else {
		return 0;
	}

}











