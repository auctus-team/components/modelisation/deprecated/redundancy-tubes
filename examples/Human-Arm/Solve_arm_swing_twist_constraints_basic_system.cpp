/*
 * Solve_arm_swing_twist_constraints.cpp
 *
 *  Created on: Dec 11, 2018
 *      Author: jpickard
 */

#include "ibex.h"
#include "vibes.h"
#include <iostream>
#include <fstream>

ibex::IntervalVector slerp(ibex::IntervalVector &q1, ibex::IntervalVector &q2, ibex::Interval t);
ibex::Interval polygon_swing_xz_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi);
ibex::Interval norm(ibex::IntervalVector &v);
ibex::Interval Rot_Ext_Max(ibex::Interval alpha, ibex::Interval beta);
ibex::Interval Rot_Int_Max(ibex::Interval alpha, ibex::Interval beta);

int main() {

	bool check_swing_limits = false;
	bool check_axial_limits = false;
	ibex::Interval d3 = 28.74;

	// Clear directory for new list
	int dir_clean = system("exec rm -r manifold/list/*");

	// ################################################
	// ALPHA AND BETA RANGES FROM Wang et al. 1998.
	// ################################################
	std::string file = "manifold/alpha_beta_range.txt";
	std::ofstream alpha_beta_range;
	alpha_beta_range.open(file);
	for (int alpha=-100; alpha<=100; alpha+=4){
		for (int beta=-50; beta<=50; beta+=4){
			ibex::Interval Phi_Ext = Rot_Ext_Max(alpha*M_PI/180, beta*M_PI/180);
			ibex::Interval Phi_Int = Rot_Int_Max(alpha*M_PI/180, beta*M_PI/180);
			alpha_beta_range << alpha << ',' << beta << ',' << Phi_Ext.mid() << ',' << Phi_Int.mid() << ";\n";
		}
	}
	alpha_beta_range.close();

	// ################################################
	// CREATE SOLVER
	// ################################################
	ibex::System system("examples/Human-Arm/minibex/Arm_kinematics_basic_system_axial_constraints.mbx");
	std::cout << system << std::endl;
	// Contractors
	ibex::CtcHC4 hc4_1(system,0.01);
	ibex::CtcHC4 hc4acid_1(system,0.1,true);
	ibex::CtcAcid acid_1(system, hc4acid_1);
	ibex::CtcCompo compo_kinematics(hc4_1,acid_1);

//	ibex::System system_swing1("minibex/Arm_swing_constraint1.mbx");
//	// Contractors
//	ibex::CtcHC4 hc4_2(system_swing1,0.01);
//	ibex::CtcHC4 hc4acid_2(system_swing1,0.1,true);
//	ibex::CtcAcid acid_2(system_swing1, hc4acid_2);
//	ibex::CtcCompo compo_swing1(hc4_2,acid_2);

//	ibex::System system_swing2("minibex/Arm_swing_constraint2.mbx");
//	// Contractors
//	ibex::CtcHC4 hc4_3(system_swing2,0.01);
//	ibex::CtcHC4 hc4acid_3(system_swing2,0.1,true);
//	ibex::CtcAcid acid_3(system_swing2, hc4acid_3);
//	ibex::CtcCompo compo_swing2(hc4_3,acid_3);
//
//	// All Contractors
//	ibex::CtcUnion union_swing(compo_swing1, compo_swing2); // HOW TO DO THIS CORRECTLY??
//	ibex::CtcCompo compo_all(compo_swing1,compo_kinematics);

	/* Create a smear-function bisection heuristic. */
	ibex::SmearSumRelative bisector(system, 1e-02);

	/* Create a "stack of boxes" (CellStack) (depth-first search). */
	ibex::CellStack buff;

	/* Vector precisions required on variables */
	ibex::Vector prec(6, 1e-02);

	/* Create a solver with the previous objects */
	ibex::Solver s(system, compo_kinematics, bisector, buff, prec, prec);

	// ################################################
	// SLERP ROUTINE TO GENERATE CONSTRAINT G
	// ################################################
	// Quaternion constraints
	int num_points = 8;
	double _thetaswing[num_points] = {1.04719755119660,	2.26892802759263,	0.872664625997165,	0.523598775598299,	2.09439510239320,	1.74532925199433,	2.09439510239320,	1.04719755119660};
	double _phiswing[num_points] = {-3.14159265358979,	-2.79252680319093,	-1.91986217719376,	-0.872664625997165,	0.0,	0.872664625997165,	1.91986217719376,	3.14159265358979};
	ibex::IntervalVector thetaswing = ibex::IntervalVector(ibex::Vector(8, _thetaswing));
	ibex::IntervalVector phiswing = ibex::IntervalVector(ibex::Vector(8, _phiswing));

	// Lists
	std::vector<ibex::Interval> phiswing_list;
	std::vector<ibex::Interval> thetaswing_list;
	std::vector<ibex::Interval> g_list;

	// Check quaternion pairs
	for (int index=0; index<num_points-1; index++){
		ibex::IntervalVector qi(4);
		qi[0] = (cos((0.5)*thetaswing[index]));
		qi[1] = (sin(phiswing[index])*sin((0.5)*thetaswing[index]));
		qi[2] = (0);
		qi[3] = (cos(phiswing[index])*sin((0.5)*thetaswing[index]));

		ibex::IntervalVector qj(4);
		qj[0] = (cos((0.5)*sign(thetaswing[index+1])*thetaswing[index+1]));
		qj[1] = (sin(phiswing[index+1])*sin((0.5)*thetaswing[index+1]));
		qj[2] = (0);
		qj[3] = (cos(phiswing[index+1])*sin((0.5)*thetaswing[index+1]));

		ibex::Interval t = 0;
		ibex::Interval dt = 1.0/75.0;
		while(t.ub()<=1.0){
			// Slerp to find intermediate quaternion
			ibex::IntervalVector qrel = slerp(qi, qj, t);

			// Convert quaternion to axis and angle
			ibex::IntervalVector u = qrel.subvector(1,3);
			ibex::Interval u_norm = norm(u);
			u = 1 / u_norm * qrel.subvector(1,3); // normalize
			ibex::Interval theta = 2 * atan2(u_norm, qrel[0]);
			ibex::Interval phi = atan2(u[0], u[2]);

//			std::cout << "theta: " << theta << std::endl;
//			std::cout << "phi: " << phi  << "\t" << t << std::endl;

			// Save theta and phi to list
			phiswing_list.push_back(phi);
			thetaswing_list.push_back(theta);
			g_list.push_back(cos(theta));

			// Increment t
			t += dt;
		}
	}

	// ################################################
	// EVALUATE SOLVER OVER PSI-PHI GRID
	// ################################################
	// Visualize results - plot kinematic chain
	vibes::beginDrawing();
	vibes::newFigure("Allowable Psi-Phi (Swivel-Twist)");
	vibes::axisLimits(-1,M_PI*2+1,-M_PI-1,M_PI+1);
	vibes::setFigureProperty("width",600);
	vibes::setFigureProperty("height",600);

	int spacing = 5;
	int solutionNum = 1;
	std::vector<ibex::IntervalVector> psiphiSolutions;
	for (int degpsi=0; degpsi<359; degpsi+=spacing){
		ibex::Interval radpsi;
		radpsi = static_cast<double>(degpsi)*M_PI/180;

		for (int degphi=-180; degphi<179; degphi+=spacing){
				ibex::Interval radphi;
				radphi = static_cast<double>(degphi)*M_PI/180;

				// Set psi and phi in system_box
				ibex::IntervalVector psiphi(2);
				psiphi[0] = radpsi;
				psiphi[1] = radphi;
				ibex::IntervalVector system_box = system.box;
				system_box[96] = radpsi;
				system_box[97] = radphi;

				// Check if system_box has a solution
				s.solve(system_box);
				std::cout << psiphi << std::endl;
				std::cout << s.get_manifold().size() << std::endl;
				if (s.get_manifold().size()>0){

					std::vector<ibex::QualifiedBox> solverSolutions;
					solverSolutions.insert(solverSolutions.end(), s.get_manifold().inner.begin(), s.get_manifold().inner.end());
					solverSolutions.insert(solverSolutions.end(), s.get_manifold().boundary.begin(), s.get_manifold().boundary.end());
					solverSolutions.insert(solverSolutions.end(), s.get_manifold().unknown.begin(), s.get_manifold().unknown.end());

					bool foundsolution = false;
					for (int isol=0; isol<1; isol++){
						// Two conditions to check
						bool foundswingsolution = true;
						bool foundaxialsolution = true;
						ibex::Interval d3 = 28.74;

						// Continue until solution found
						if (!foundsolution){
							ibex::IntervalVector sol_box = solverSolutions[isol].existence();

							ibex::IntervalVector PEL(3);
							PEL[0] = sol_box[74];
							PEL[1] = sol_box[82];
							PEL[2] = sol_box[90];

//							std::cout << "k " << sol_box[sol_box.size()-15] << std::endl;
//							std::cout << "gu1 " << sol_box[sol_box.size()-1] << std::endl;

							if (check_axial_limits){
								// Check axial constraints
								// Compute alpha and beta
								ibex::Interval XEL = 1/d3 * PEL[0];
								ibex::Interval YEL = 1/d3 * PEL[1];
								ibex::Interval ZEL = 1/d3 * PEL[2];
								ibex::Interval beta = atan2(-ZEL, sqrt(sqr(XEL)+sqr(YEL)));
								ibex::Interval alpha = atan2(XEL/cos(beta), -YEL/cos(beta));

								ibex::Interval Phi_Ext = Rot_Ext_Max(alpha, beta)*M_PI/180.0+M_PI; // convert to rad and add Pi to adjust for Wang et al. test orientation
								ibex::Interval Phi_Int = Rot_Int_Max(alpha, beta)*M_PI/180.0+M_PI; // convert to rad and add Pi to adjust for Wang et al. test orientation

								ibex::Interval psi = sol_box[96];

								// Create allowable psi interval
								ibex::Interval psi_allowable = Phi_Int | Phi_Ext;

								std::cout << Phi_Ext << '\t' << Phi_Int << '\t' << psi << std::endl;
								std::cout << alpha << '\t' << beta << std::endl;
								std::cout << psi << " in \t" << psi_allowable << std::endl;

								if (!psi.is_subset(psi_allowable)){
									foundaxialsolution = false;
									std::cout << "NOT feasible - axial" << std::endl;
								}
							}

							if (check_swing_limits && foundaxialsolution) {
								// Check swing constraints
								ibex::IntervalVector PEL_unit = 1/d3 * PEL;
								ibex::IntervalVector a_vect(4,0);
								a_vect[2] = -1; // y axis
								ibex::IntervalVector a_vect_prime(4,0);
								a_vect_prime[1] = PEL_unit[0];
								a_vect_prime[2] = PEL_unit[1];
								a_vect_prime[3] = PEL_unit[2];
								// Compute K
								ibex::Interval K = a_vect_prime * a_vect;
								ibex::IntervalVector U = ibex::cross(a_vect.subvector(1,3),a_vect_prime.subvector(1,3));
								ibex::Interval Anglephi = atan2(U[0],U[2]);
	//							std::cout << "PEL_unit " << PEL_unit << std::endl;
	//							std::cout << "a_vect_prime " << a_vect_prime << std::endl;
								std::cout << "Anglephi " << Anglephi << std::endl;

								// Use Slerp to find thetaswing
								ibex::Interval gu = polygon_swing_xz_limit(Anglephi, thetaswing_list, phiswing_list);
								std::cout << gu << " <= " << K << " <= " << 1 << std::endl;

								// Feasible solution found!
								if (K.ub() < gu.lb()){
									std::cout << "NOT feasible - swing" << std::endl;
									foundaxialsolution = false;
								}
							}

							// Check if all conditions are satisfied
							if (foundaxialsolution && foundswingsolution){
								// Feasible solution found!
								std::cout << "SOLUTION" << std::endl;
								foundsolution = true;
								psiphiSolutions.push_back(psiphi);
								vibes::drawEllipse (psiphi[0].mid(), psiphi[1].mid(), 0.01, 0.01, 0.0, "b");
							}
						}
					}
					// No feasible solution found
					if (!foundsolution){
						std::cout << "NO SOLUTION" << std::endl;
						vibes::drawEllipse (psiphi[0].mid(), psiphi[1].mid(), 0.01, 0.01, 0.0, "r");
					}
					// Save feasible solution
					else {
						// Save solution
						std:string file = "manifold/list/manifold";
						file += std::to_string(solutionNum);
						file += ".txt";
						std::cout << file << std::endl;
						const char *cstr = file.c_str();
						s.get_manifold().write_txt(cstr);
						solutionNum++;
					}
				}
		}
	}
	// Save psiphiSolutions to txt file
	std::ofstream myfile;
	myfile.open ("manifold/psi-phi-workspace.txt");
	for (int isol=0; isol<psiphiSolutions.size(); isol++){
		ibex::IntervalVector psiphi = psiphiSolutions[isol];
		myfile << psiphi[0].mid() << '\t' << psiphi[1].mid() << '\n';
	}
	myfile.close();


	return 0;
}

ibex::IntervalVector slerp(ibex::IntervalVector &q1, ibex::IntervalVector &q2, ibex::Interval t){
	// ################################################
	// SLERP ROUTINE TO GENERATE CONSTRAINT G
	// ################################################
	ibex::Interval dotqq = (q1[0]*q2[0]+q1[1]*q2[1]+q1[2]*q2[2]+q1[3]*q2[3]);
//	ibex::Interval thetaqq = acos(dotqq);
	ibex::Interval scale0 = sin((1.0 - t) * acos(dotqq)) / sin(acos(dotqq));
	ibex::Interval scale1 = sign(dotqq)*sin((t * acos(dotqq))) / sin(acos(dotqq));
//	std::cout << "scale0 " << scale0 << std::endl;
//	std::cout << "scale1 " << scale1 << std::endl;
	ibex::IntervalVector q1q2t = scale0 * q1 + scale1 * q2;
	ibex::IntervalVector q1q2t_unit = 1/sqrt(sqr(q1q2t[0])+sqr(q1q2t[1])+sqr(q1q2t[2])+sqr(q1q2t[3])) * q1q2t;
	return q1q2t_unit;
}

ibex::Interval norm(ibex::IntervalVector &v){
	ibex::Interval norm_val = 0;
	for (int i=0; i<v.size(); i++)
		norm_val += sqr(v[i]);
	norm_val = sqrt(norm_val);
	return norm_val;
}

ibex::IntervalVector quatexp(ibex::IntervalVector &v){
	ibex::Interval v_norm = norm(v);
	ibex::IntervalVector quat(4);
	quat[0] = cos(v_norm);
	ibex::IntervalVector temp = sin(v_norm)/v_norm * v;
	quat[1] = temp[0];
	quat[2] = temp[1];
	quat[3] = temp[2];
	return quat;
}

ibex::Interval polygon_swing_xz_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi){

	// Find the indices of vphi which bound the value of phi
	int vphi_index1 = 0;
	int vphi_index2 = 0;
	for (int i=0; i<vphi.size(); i++){
		if (phi.lb() >= vphi[i].ub()){
			vphi_index1 = i;
			vphi_index2 = i+1;
		}
	}
	// Check if vphi_index2 needs to be corrected (overflows)
	if (vphi_index2>=vphi.size())
		vphi_index2 = 0;
//	std::cout << vphi.size() << std::endl;
//	std::cout << "vphi_index1 " << vphi_index1 << std::endl;
//	std::cout << "vphi_index2 " << vphi_index2 << std::endl;

	ibex::Interval phi1 = vphi[vphi_index1];
	ibex::Interval phi2 = vphi[vphi_index2];
	ibex::Interval theta1 = vtheta[vphi_index1];
	ibex::Interval theta2 = vtheta[vphi_index2];
//    std::cout << "phi1 " << phi1 << std::endl;
//    std::cout << "phi2 " << phi2 << std::endl;
//    std::cout << "theta1 " << theta1 << std::endl;
//    std::cout << "theta2 " << theta2 << std::endl;

	// Slerp
	ibex::Interval t = (phi - phi1) / (phi2 - phi1);
	ibex::IntervalVector u1(3);
	u1[0] = sin(phi1);
	u1[1] = 0;
	u1[2] = cos(phi1);
	ibex::IntervalVector v1 = theta1/2 * u1;
	ibex::IntervalVector q1 = quatexp(v1);

	ibex::IntervalVector u2(3);
	u2[0] = sin(phi2);
	u2[1] = 0;
	u2[2] = cos(phi2);
	ibex::IntervalVector v2 = theta2/2 * u2;
	ibex::IntervalVector q2 = quatexp(v2);

	ibex::IntervalVector qrel = slerp(q1,q2,t);

//	std::cout << "v1 " << v1 << std::endl;
//	std::cout << "v2 " << v2 << std::endl;
//	std::cout << "q1 " << q1 << std::endl;
//	std::cout << "q2 " << q2 << std::endl;
//	std::cout << "t " << t << std::endl;
//	std::cout << "qrel " << qrel << std::endl;

	// Swing limit (theta max)
	ibex::IntervalVector qrel_vect = qrel.subvector(1,3);
	ibex::Interval theta = 2 *atan2(norm(qrel_vect), qrel[0]);
	return cos(theta);
}

ibex::Interval Rot_Ext_Max(ibex::Interval alpha, ibex::Interval beta){
/*
 * Calculate the maximum external rotation of the humerus using formulation of Wang et al. 1998
 *
 * Input: alpha, beta : azimuth and elevation in radian
 * Output: Phi_Ext: maximum external rotation in radian
 */
	ibex::Interval x = alpha * cos(beta);
	ibex::Interval y = beta;

	double a_j[5] = {-2.459, 9.299, 3.428, -2.987, 0.331};
	double b_jm[5][5] = {{1, 0, 0, 0, 0},
			{1, -1.69, 0, 0, 0},
			{1, 8.615, -2.149, 0, 0},
			{1, 0.859, -8.025, 0.514, 0},
			{1, -13.771, -17.172, 35.46, 2.189}};

	ibex::Interval Phi_Ext = 0;
    for (int j=0; j<5; j++){
    	ibex::Interval P_j = 0;
    	for (int m=0; m<=j; m++){
    		P_j += b_jm[j][m] * pow(x,j-m) * pow(y,m);
    	}
    	Phi_Ext += a_j[j] * P_j;
    }

    return Phi_Ext;
}

ibex::Interval Rot_Int_Max(ibex::Interval alpha, ibex::Interval beta){
/*
 * Calculate the maximum internal rotation of the humerus using formulation of Wang et al. 1998
 *
 * Input: alpha, beta : azimuth and elevation in radian
 * Output: Phi_Ext: maximum internal rotation in radian
 */
	ibex::Interval x = alpha * cos(beta);
	ibex::Interval y = beta;

	double a_j[4] = {-139.27, 18.652, 4.092, -2.081};
	double b_jm[4][4] = {{1, 0, 0, 0},
			{1, -2.235,0,0},
			{1, 7.251, -0.654,0},
			{1, 0.069, -10.035, -1.642}};

	ibex::Interval Phi_Int = 0;
    for (int j=0; j<4; j++){
    	ibex::Interval Pj = 0;
    	for (int m=0; m<=j; m++){
    		Pj += b_jm[j][m] * pow(x,j-m) * pow(y,m);
    	}
    	Phi_Int += a_j[j] * Pj;
    }

    return Phi_Int;
}







