/*
 * Redundant_workspace_discrete.cpp
 *
 *  Created on: Jan 15, 2019
 *      Author: jpickard
 */


#include "ibex.h"
#include <iostream>
#include <fstream>

int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol);
ibex::Interval Rot_Ext_Max(ibex::Interval alpha, ibex::Interval beta);
ibex::Interval Rot_Int_Max(ibex::Interval alpha, ibex::Interval beta);
ibex::IntervalVector slerp(ibex::IntervalVector &q1, ibex::IntervalVector &q2, ibex::Interval t);
ibex::Interval polygon_swing_xz_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi);
ibex::Interval polygon_swing_xy_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi);
ibex::Interval norm(ibex::IntervalVector &v);

int main() {

	bool check_shoulder_swing_limits = true;
	bool check_hand_swing_limits = false;
	ibex::Interval d3 = 28.74;
	ibex::Interval d8 = 3.76;

	// Clear directory for new list
	int dir_clean = system("exec rm -r manifold/list/*");

	// ################################################
	// SLERP ROUTINE TO GENERATE CONSTRAINT G
	// ################################################
	// Shoulder quaternion constraints (To approximate Wang et al. 1998.)
	int num_shoulder_points = 18;
	double _shoulderphiswing[num_shoulder_points] = {-M_PI, -2.047099962, -1.570796327, -.8868510796, -.7439121072, -.6839452473, -.3872228929, 0., .2088928627, .6224288550, .6938260469, .8868510796, 1.570796327, 2.254741574, 2.356194490, 2.563319152, 2.780739254, M_PI};
	double _shoulderthetaswing[num_shoulder_points] = {.9995080490, .8345071701, .7853981634, .9995080490, 1.742730040, 1.900344531, 2.069514319, 2.112549334, 2.091560103, 2.069514319, 1.742730040, .9995080490, .7853981634, .9995080490, 1.107148718, 1.398862614, 1.170944762, .9995080490};
	ibex::IntervalVector shoulderthetaswing = ibex::IntervalVector(ibex::Vector(num_shoulder_points, _shoulderthetaswing));
	ibex::IntervalVector shoulderphiswing = ibex::IntervalVector(ibex::Vector(num_shoulder_points, _shoulderphiswing));

	// Wrist quaternion constraints (To approximate Gehrmann at al. 2008.)
	int num_wrist_points = 9;
	double _wristphiswing[num_wrist_points] = {-M_PI, -2.356194490192345, -1.570796326794897, -0.785398163397448, 0 , 0.785398163397448, 1.570796326794897, 2.356194490192345, M_PI};
	double _wristthetaswing[num_wrist_points] = {0.698131700797732, 0.698131700797732, 0.698131700797732, 0.698131700797732, 0.698131700797732, 0.523598775598299, 0.785398163397448, 1.396263401595464, 0.698131700797732};
	ibex::IntervalVector wristthetaswing = ibex::IntervalVector(ibex::Vector(num_wrist_points, _wristthetaswing));
	ibex::IntervalVector wristphiswing = ibex::IntervalVector(ibex::Vector(num_wrist_points, _wristphiswing));

	// Lists
	std::vector<ibex::Interval> shoulderphiswing_list;
	std::vector<ibex::Interval> shoulderthetaswing_list;
//	std::vector<ibex::Interval> g_shoulderlist;

	// Check quaternion pairs
	for (int index=0; index<num_shoulder_points-1; index++){
		ibex::IntervalVector qi(4);
		qi[0] = (cos((0.5)*shoulderthetaswing[index]));
		qi[1] = (sin(shoulderphiswing[index])*sin((0.5)*shoulderthetaswing[index]));
		qi[2] = (0);
		qi[3] = (cos(shoulderphiswing[index])*sin((0.5)*shoulderthetaswing[index]));

		ibex::IntervalVector qj(4);
		qj[0] = (cos((0.5)*sign(shoulderthetaswing[index+1])*shoulderthetaswing[index+1]));
		qj[1] = (sin(shoulderphiswing[index+1])*sin((0.5)*shoulderthetaswing[index+1]));
		qj[2] = (0);
		qj[3] = (cos(shoulderphiswing[index+1])*sin((0.5)*shoulderthetaswing[index+1]));

		ibex::Interval t = 0;
		ibex::Interval dt = 1.0/75.0;
		while(t.ub()<=1.0){
			// Slerp to find intermediate quaternion
			ibex::IntervalVector qrel = slerp(qi, qj, t);

			// Convert quaternion to axis and angle
			ibex::IntervalVector u = qrel.subvector(1,3);
			ibex::Interval u_norm = norm(u);
			u = 1 / u_norm * qrel.subvector(1,3); // normalize
			ibex::Interval theta = 2 * atan2(u_norm, qrel[0]);
			ibex::Interval phi = atan2(u[0], u[2]);

//			std::cout << "theta: " << theta << std::endl;
//			std::cout << "phi: " << phi  << "\t" << t << std::endl;

			// Save theta and phi to list
			shoulderphiswing_list.push_back(phi);
			shoulderthetaswing_list.push_back(theta);
//			g_shoulderlist.push_back(cos(theta));

			// Increment t
			t += dt;
		}
	}

	// Lists
	std::vector<ibex::Interval> wristphiswing_list;
	std::vector<ibex::Interval> wristthetaswing_list;
//	std::vector<ibex::Interval> g_wristlist;

	// Check quaternion pairs
	for (int index=0; index<num_wrist_points-1; index++){
		ibex::IntervalVector qi(4);
		qi[0] = (cos((0.5)*wristthetaswing[index]));
		qi[1] = (cos(_wristphiswing[index])*sin((0.5)*wristthetaswing[index]));
		qi[2] = (sin(_wristphiswing[index])*sin((0.5)*wristthetaswing[index]));
		qi[3] = (0);

		ibex::IntervalVector qj(4);
		qj[0] = (cos((0.5)*sign(wristthetaswing[index+1])*wristthetaswing[index+1]));
		qj[1] = (cos(_wristphiswing[index+1])*sin((0.5)*wristthetaswing[index+1]));
		qj[2] = (sin(_wristphiswing[index+1])*sin((0.5)*wristthetaswing[index+1]));
		qj[3] = (0);

		ibex::Interval t = 0;
		ibex::Interval dt = 1.0/75.0;
		while(t.ub()<=1.0){
			// Slerp to find intermediate quaternion
			ibex::IntervalVector qrel = qi;
			if (t==0)
				qrel = qi;
			else if (t==1)
				qrel = qj;
			else
				qrel = slerp(qi, qj, t);

			// Convert quaternion to axis and angle
			ibex::IntervalVector u = qrel.subvector(1,3);
			ibex::Interval u_norm = norm(u);
			u = 1 / u_norm * qrel.subvector(1,3); // normalize
			ibex::Interval theta = 2 * atan2(u_norm, qrel[0]);
			ibex::Interval phi = atan2(u[1], u[0]);

//			std::cout << "u: " << u << std::endl;
//			std::cout << "theta: " << theta << " phi: " << phi  << "\t" << t << std::endl;

			// Save theta and phi to list
			wristphiswing_list.push_back(phi);
			wristthetaswing_list.push_back(theta);
//			g_wristlist.push_back(cos(theta));

			// Increment t
			t += dt;
		}
	}

	// ################################################
	// CREATE SOLVER
	// ################################################
	ibex::System system("examples/Human-Arm/minibex/kinematics_ZYX_workspace.mbx");
	std::cout << system << std::endl;
	// Contractors
	ibex::CtcHC4 hc4_1(system,0.01);
	ibex::CtcHC4 hc4acid_1(system,0.1,true);
	ibex::CtcAcid acid_1(system, hc4acid_1);
	ibex::CtcCompo compo_kinematics(hc4_1,acid_1);

	/* Create a smear-function bisection heuristic. */
	ibex::SmearSumRelative bisector(system, 1e-05); // must be small so that constraints are not over-estimated.

	/* Create a "stack of boxes" (CellStack) (depth-first search). */
	ibex::CellStack buff;

	/* Vector precisions required on variables */
	ibex::Vector prec(6, 1e-05);

	/* Create a solver with the previous objects */
	ibex::Solver s(system, compo_kinematics, bisector, buff, prec, prec);

	// ################################################
	// EVALUATE SOLVER OVER GRID
	// ################################################
	int spacing = 5;
	int solutionNum = 1;
	int searchSize = 61;
	std::vector<ibex::IntervalVector> outerboxSolutions;

	for (int posX=-searchSize; posX<searchSize; posX+=spacing){
		for (int posY=-searchSize; posY<searchSize; posY+=spacing){
			for (int posZ=-searchSize; posZ<searchSize; posZ+=spacing){
				int degpsi=-90;
				bool iterate_psi = true;
				while (iterate_psi && degpsi<=269){
					ibex::Interval radpsi;
					radpsi = static_cast<double>(degpsi)*M_PI/180;

					// Set pose in system_box
					ibex::IntervalVector outerbox(3);
					outerbox[0] = ibex::Interval(posX);
					outerbox[1] = ibex::Interval(posY);
					outerbox[2] = ibex::Interval(posZ);
					ibex::IntervalVector system_box = system.box;
					system_box[find_var_index(system.f_ctrs.args(), "psi")] = radpsi;
					system_box[find_var_index(system.f_ctrs.args(), "rx8")] = ibex::Interval(posX);
					system_box[find_var_index(system.f_ctrs.args(), "ry8")] = ibex::Interval(posY);
					system_box[find_var_index(system.f_ctrs.args(), "rz8")] = ibex::Interval(posZ);

					ibex::IntervalMatrix R8(3,3);
					R8[0][0] = 0;	R8[0][1] = 0;	R8[0][2] = 1;
					R8[1][0] = 1;	R8[1][1] = 0;	R8[1][2] = 0;
					R8[2][0] = 0;	R8[2][1] = 1;	R8[2][2] = 0;

					// Update values in kinematics_box
					system_box[find_var_index(system.f_ctrs.args(), "Rux8")] = R8[0][0];
					system_box[find_var_index(system.f_ctrs.args(), "Ruy8")] = R8[1][0];
					system_box[find_var_index(system.f_ctrs.args(), "Ruz8")] = R8[2][0];
					system_box[find_var_index(system.f_ctrs.args(), "Rvx8")] = R8[0][1];
					system_box[find_var_index(system.f_ctrs.args(), "Rvy8")] = R8[1][1];
					system_box[find_var_index(system.f_ctrs.args(), "Rvz8")] = R8[2][1];
					system_box[find_var_index(system.f_ctrs.args(), "Rwx8")] = R8[0][2];
					system_box[find_var_index(system.f_ctrs.args(), "Rwy8")] = R8[1][2];
					system_box[find_var_index(system.f_ctrs.args(), "Rwz8")] = R8[2][2];

					// Check if system_box has a solution
//					std::cout << system_box << std::endl;
					s.solve(system_box);
					if (s.get_manifold().size()>0){
						std::cout << outerbox << std::endl;
						std::cout << s.get_manifold().size() << std::endl;
						std::cout << "radpsi: " << radpsi << std::endl;

						std::vector<ibex::QualifiedBox> solverSolutions;
						solverSolutions.insert(solverSolutions.end(), s.get_manifold().inner.begin(), s.get_manifold().inner.end());
						solverSolutions.insert(solverSolutions.end(), s.get_manifold().boundary.begin(), s.get_manifold().boundary.end());
						solverSolutions.insert(solverSolutions.end(), s.get_manifold().unknown.begin(), s.get_manifold().unknown.end());

						bool foundsolution = false;
						for (int isol=0; isol<1; isol++){
							// Conditions to check
							bool foundshoulderswingsolution = true;
							bool foundhandswingsolution = true;

							// Continue until solution found
							if (!foundsolution){
								ibex::IntervalVector sol_box = solverSolutions[isol].existence();
								ibex::IntervalVector PEL(3);
								PEL[0] = sol_box[find_var_index(system.f_ctrs.args(), "rx4")];
								PEL[1] = sol_box[find_var_index(system.f_ctrs.args(), "ry4")];
								PEL[2] = sol_box[find_var_index(system.f_ctrs.args(), "rz4")];
//								std::cout << "PEL " << PEL << std::endl;

//								std::cout << "alpha" << sol_box[find_var_index(system.f_ctrs.args(), "alpha")] << std::endl;
//								std::cout << "beta" << sol_box[find_var_index(system.f_ctrs.args(), "beta")] << std::endl;
//								std::cout << "INT" << sol_box[find_var_index(system.f_ctrs.args(), "INT")] << std::endl;
//								std::cout << "EXT" << sol_box[find_var_index(system.f_ctrs.args(), "EXT")] << std::endl;

								if (check_shoulder_swing_limits) {
									std::cout << "SHOULDER SWING" << std::endl;
									// Check shoulder swing constraints
									ibex::IntervalVector PEL_unit = 1/d3 * PEL;
									ibex::IntervalVector a_vect(4,0);
									a_vect[2] = -1; // y axis
									ibex::IntervalVector a_vect_prime(4,0);
									a_vect_prime[1] = PEL_unit[0];
									a_vect_prime[2] = PEL_unit[1];
									a_vect_prime[3] = PEL_unit[2];
									// Compute K
									ibex::Interval K = a_vect_prime * a_vect;
									ibex::IntervalVector U = ibex::cross(a_vect.subvector(1,3),a_vect_prime.subvector(1,3));
									ibex::Interval Anglephi = atan2(U[0],U[2]);
		//							std::cout << "PEL_unit " << PEL_unit << std::endl;
		//							std::cout << "a_vect_prime " << a_vect_prime << std::endl;
	//								std::cout << "Anglephi " << Anglephi << std::endl;

									// Use Slerp to find shoulderthetaswing
									ibex::Interval gu = polygon_swing_xz_limit(Anglephi, shoulderthetaswing_list, shoulderphiswing_list);
									std::cout << gu << " <= " << K << " <= " << 1 << std::endl;

									// Feasible solution found!
									if (K.ub() < gu.lb() || gu.is_empty()){
										std::cout << "NOT feasible - shoulder swing" << std::endl;
										foundshoulderswingsolution = false;
									}
								}

								if (check_hand_swing_limits) {
									std::cout << "HAND SWING" << std::endl;
									ibex::IntervalMatrix R50(3,3);
									R50[0][0] = sol_box[find_var_index(system.f_ctrs.args(), "Rux5")];
									R50[1][0] = sol_box[find_var_index(system.f_ctrs.args(), "Rvx5")];
									R50[2][0] = sol_box[find_var_index(system.f_ctrs.args(), "Rwx5")];
									R50[0][1] = sol_box[find_var_index(system.f_ctrs.args(), "Ruy5")];
									R50[1][1] = sol_box[find_var_index(system.f_ctrs.args(), "Rvy5")];
									R50[2][1] = sol_box[find_var_index(system.f_ctrs.args(), "Rwy5")];
									R50[0][2] = sol_box[find_var_index(system.f_ctrs.args(), "Ruz5")];
									R50[1][2] = sol_box[find_var_index(system.f_ctrs.args(), "Rvz5")];
									R50[2][2] = sol_box[find_var_index(system.f_ctrs.args(), "Rwz5")];

									ibex::IntervalVector r8(3);
									r8[0] = sol_box[find_var_index(system.f_ctrs.args(), "rx8")];
									r8[1] = sol_box[find_var_index(system.f_ctrs.args(), "ry8")];
									r8[2] = sol_box[find_var_index(system.f_ctrs.args(), "rz8")];
									ibex::IntervalVector r7(3);
									r7[0] = sol_box[find_var_index(system.f_ctrs.args(), "rx7")];
									r7[1] = sol_box[find_var_index(system.f_ctrs.args(), "ry7")];
									r7[2] = sol_box[find_var_index(system.f_ctrs.args(), "rz7")];

									// Check hand swing constraints
									ibex::IntervalVector PWH_unit = R50 * ( 1/d8 * (r8-r7));
									ibex::IntervalVector a_vect(4,0);
									a_vect[3] = 1; // z axis
									ibex::IntervalVector a_vect_prime(4,0);
									a_vect_prime[1] = PWH_unit[0];
									a_vect_prime[2] = PWH_unit[1];
									a_vect_prime[3] = PWH_unit[2];
									// Compute K
									ibex::Interval K = a_vect_prime * a_vect;
									ibex::IntervalVector U = ibex::cross(a_vect.subvector(1,3),a_vect_prime.subvector(1,3));
									ibex::Interval Anglephi = atan2(U[1],U[0]);
//									std::cout << "PWH_unit " << PWH_unit << std::endl;
//									std::cout << "a_vect_prime " << a_vect_prime << std::endl;
//									std::cout << "a_vect " << a_vect << std::endl;
//									std::cout << "U " << U << std::endl;
//									std::cout << "Anglephi " << Anglephi << std::endl;

									// Use Slerp to find shoulderthetaswing
									ibex::Interval gu = polygon_swing_xy_limit(Anglephi, wristthetaswing_list, wristphiswing_list);
									std::cout << gu << " <= " << K << " <= " << 1 << std::endl;

									// Feasible solution found!
									if (K.ub() < gu.lb() || gu.is_empty()){
										std::cout << "NOT feasible - hand swing" << std::endl;
										foundhandswingsolution = false;
									}
								}
								// Check if all conditions are satisfied
								if (foundshoulderswingsolution && foundhandswingsolution){
									// Feasible solution found!
									iterate_psi = false;
									std::cout << "SOLUTION" << std::endl;
									foundsolution = true;
									outerboxSolutions.push_back(outerbox);
								}
							}
						}

						// No feasible solution found
						if (!foundsolution){
							std::cout << "NO SOLUTION" << std::endl;
						}
						// Save feasible solution
						else {
							// Save solution
							std:string file = "manifold/list/manifold";
							file += std::to_string(solutionNum);
							file += ".txt";
							std::cout << file << std::endl;
							const char *cstr = file.c_str();
							s.get_manifold().write_txt(cstr);
							solutionNum++;
						}
					}
					degpsi+=spacing;
				}
			}
		}
	}
//	// Save outerboxSolutions to txt file
//	std::ofstream myfile;
//	myfile.open ("manifold/psi-phi-workspace.txt");
//	for (int isol=0; isol<outerboxSolutions.size(); isol++){
//		ibex::IntervalVector outerbox = outerboxSolutions[isol];
//		myfile << outerbox[0].mid() << '\t' << outerbox[1].mid() << '\n';
//	}
//	myfile.close();


	return 0;
}

int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol){
	const char *cstr = symbol.c_str();
	for (int var_i=0; var_i<variable_list.size(); var_i++){
//		std::cout << variable_list[var_i].name << '\t' << symbol << std::endl;
		if (strcmp(variable_list[var_i].name, cstr) == 0){
//			std::cout << "Found index: " << var_i << std::endl;
			return var_i;
		}
	}
	return -1;
}

ibex::Interval Rot_Ext_Max(ibex::Interval alpha, ibex::Interval beta){
/*
 * Calculate the maximum external rotation of the humerus using formulation of Wang et al. 1998
 *
 * Input: alpha, beta : azimuth and elevation in radian
 * Output: Phi_Ext: maximum external rotation in radian
 */
	ibex::Interval x = alpha * cos(beta);
	ibex::Interval y = beta;

	double a_j[5] = {-2.459, 9.299, 3.428, -2.987, 0.331};
	double b_jm[5][5] = {{1, 0, 0, 0, 0},
			{1, -1.69, 0, 0, 0},
			{1, 8.615, -2.149, 0, 0},
			{1, 0.859, -8.025, 0.514, 0},
			{1, -13.771, -17.172, 35.46, 2.189}};

	ibex::Interval Phi_Ext = 0;
    for (int j=0; j<5; j++){
    	ibex::Interval P_j = 0;
    	for (int m=0; m<=j; m++){
    		P_j += b_jm[j][m] * pow(x,j-m) * pow(y,m);
    	}
    	Phi_Ext += a_j[j] * P_j;
    }

    return Phi_Ext;
}

ibex::Interval Rot_Int_Max(ibex::Interval alpha, ibex::Interval beta){
/*
 * Calculate the maximum internal rotation of the humerus using formulation of Wang et al. 1998
 *
 * Input: alpha, beta : azimuth and elevation in radian
 * Output: Phi_Ext: maximum internal rotation in radian
 */
	ibex::Interval x = alpha * cos(beta);
	ibex::Interval y = beta;

	double a_j[4] = {-139.27, 18.652, 4.092, -2.081};
	double b_jm[4][4] = {{1, 0, 0, 0},
			{1, -2.235,0,0},
			{1, 7.251, -0.654,0},
			{1, 0.069, -10.035, -1.642}};

	ibex::Interval Phi_Int = 0;
    for (int j=0; j<4; j++){
    	ibex::Interval Pj = 0;
    	for (int m=0; m<=j; m++){
    		Pj += b_jm[j][m] * pow(x,j-m) * pow(y,m);
    	}
    	Phi_Int += a_j[j] * Pj;
    }

    return Phi_Int;
}

ibex::IntervalVector slerp(ibex::IntervalVector &q1, ibex::IntervalVector &q2, ibex::Interval t){
	// ################################################
	// SLERP ROUTINE TO GENERATE CONSTRAINT G
	// ################################################
	ibex::Interval dotqq = (q1[0]*q2[0]+q1[1]*q2[1]+q1[2]*q2[2]+q1[3]*q2[3]);
//	ibex::Interval thetaqq = acos(dotqq);
	ibex::Interval scale0 = sin((1.0 - t) * acos(dotqq)) / sin(acos(dotqq));
	ibex::Interval scale1 = sign(dotqq)*sin((t * acos(dotqq))) / sin(acos(dotqq));
//	std::cout << "scale0 " << scale0 << std::endl;
//	std::cout << "scale1 " << scale1 << std::endl;
	ibex::IntervalVector q1q2t = scale0 * q1 + scale1 * q2;
	ibex::IntervalVector q1q2t_unit = 1/sqrt(sqr(q1q2t[0])+sqr(q1q2t[1])+sqr(q1q2t[2])+sqr(q1q2t[3])) * q1q2t;
	return q1q2t_unit;
}

ibex::Interval norm(ibex::IntervalVector &v){
	ibex::Interval norm_val = 0;
	for (int i=0; i<v.size(); i++)
		norm_val += sqr(v[i]);
	norm_val = sqrt(norm_val);
	return norm_val;
}

ibex::IntervalVector quatexp(ibex::IntervalVector &v){
	ibex::Interval v_norm = norm(v);
	ibex::IntervalVector quat(4);
	quat[0] = cos(v_norm);
	ibex::IntervalVector temp = sin(v_norm)/v_norm * v;
	quat[1] = temp[0];
	quat[2] = temp[1];
	quat[3] = temp[2];
	return quat;
}

ibex::Interval polygon_swing_xz_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi){

	// Find the indices of vphi which bound the value of phi
	int vphi_index1 = 0;
	int vphi_index2 = 0;
	for (int i=0; i<vphi.size(); i++){
		if (phi.lb() >= vphi[i].ub()){
			vphi_index1 = i;
			vphi_index2 = i+1;
		}
	}
	// Check if vphi_index2 needs to be corrected (overflows)
	if (vphi_index2>=vphi.size())
		vphi_index2 = 0;
//	std::cout << vphi.size() << std::endl;
//	std::cout << "vphi_index1 " << vphi_index1 << std::endl;
//	std::cout << "vphi_index2 " << vphi_index2 << std::endl;

	ibex::Interval phi1 = vphi[vphi_index1];
	ibex::Interval phi2 = vphi[vphi_index2];
	ibex::Interval theta1 = vtheta[vphi_index1];
	ibex::Interval theta2 = vtheta[vphi_index2];
//    std::cout << "phi1 " << phi1 << std::endl;
//    std::cout << "phi2 " << phi2 << std::endl;
//    std::cout << "theta1 " << theta1 << std::endl;
//    std::cout << "theta2 " << theta2 << std::endl;

	// Slerp
	ibex::Interval t = (phi - phi1) / (phi2 - phi1);
	ibex::IntervalVector u1(3);
	u1[0] = sin(phi1);
	u1[1] = 0;
	u1[2] = cos(phi1);
	ibex::IntervalVector v1 = theta1/2 * u1;
	ibex::IntervalVector q1 = quatexp(v1);

	ibex::IntervalVector u2(3);
	u2[0] = sin(phi2);
	u2[1] = 0;
	u2[2] = cos(phi2);
	ibex::IntervalVector v2 = theta2/2 * u2;
	ibex::IntervalVector q2 = quatexp(v2);

	ibex::IntervalVector qrel = slerp(q1,q2,t);

//	std::cout << "v1 " << v1 << std::endl;
//	std::cout << "v2 " << v2 << std::endl;
//	std::cout << "q1 " << q1 << std::endl;
//	std::cout << "q2 " << q2 << std::endl;
//	std::cout << "t " << t << std::endl;
//	std::cout << "qrel " << qrel << std::endl;

	// Swing limit (theta max)
	ibex::IntervalVector qrel_vect = qrel.subvector(1,3);
	ibex::Interval theta = 2 *atan2(norm(qrel_vect), qrel[0]);
	return cos(theta);
}

ibex::Interval polygon_swing_xy_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi){

	// Find the indices of vphi which bound the value of phi
	int vphi_index1 = 0;
	int vphi_index2 = 0;
	for (int i=0; i<vphi.size(); i++){
		if (phi.lb() >= vphi[i].ub()){
			vphi_index1 = i;
			vphi_index2 = i+1;
		}
	}
	// Check if vphi_index2 needs to be corrected (overflows)
	if (vphi_index2>=vphi.size())
		vphi_index2 = 0;
//	std::cout << vphi.size() << std::endl;
//	std::cout << "vphi_index1 " << vphi_index1 << std::endl;
//	std::cout << "vphi_index2 " << vphi_index2 << std::endl;

	ibex::Interval phi1 = vphi[vphi_index1];
	ibex::Interval phi2 = vphi[vphi_index2];
	ibex::Interval theta1 = vtheta[vphi_index1];
	ibex::Interval theta2 = vtheta[vphi_index2];
//	std::cout << "phi " << phi << std::endl;
//    std::cout << "phi1 " << phi1 << std::endl;
//    std::cout << "phi2 " << phi2 << std::endl;
//    std::cout << "theta1 " << theta1 << std::endl;
//    std::cout << "theta2 " << theta2 << std::endl;

	// Slerp
	ibex::Interval t = (phi - phi1) / (phi2 - phi1);
	ibex::IntervalVector u1(3);
	u1[0] = cos(phi1);
	u1[1] = sin(phi1);
	u1[2] = 0;
	ibex::IntervalVector v1 = theta1/2 * u1;
	ibex::IntervalVector q1 = quatexp(v1);

	ibex::IntervalVector u2(3);
	u2[0] = cos(phi2);
	u2[1] = sin(phi2);
	u2[2] = 0;
	ibex::IntervalVector v2 = theta2/2 * u2;
	ibex::IntervalVector q2 = quatexp(v2);

	ibex::IntervalVector qrel = slerp(q1,q2,t);

//	std::cout << "v1 " << v1 << std::endl;
//	std::cout << "v2 " << v2 << std::endl;
//	std::cout << "q1 " << q1 << std::endl;
//	std::cout << "q2 " << q2 << std::endl;
//	std::cout << "t " << t << std::endl;
//	std::cout << "qrel " << qrel << std::endl;

	// Swing limit (theta max)
	ibex::IntervalVector qrel_vect = qrel.subvector(1,3);
	ibex::Interval theta = 2 *atan2(norm(qrel_vect), qrel[0]);
	return cos(theta);
}






