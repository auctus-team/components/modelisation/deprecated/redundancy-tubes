/*
 * Redundant_workspace_tube_discrete.cpp
 *
 *  Created on: Jan 22, 2019
 *      Author: jpickard
 */


#include "ibex.h"
#include "vibes.h"
#include <iostream>
#include <fstream>

int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol);
ibex::Interval Rot_Ext_Max(ibex::Interval alpha, ibex::Interval beta);
ibex::Interval Rot_Int_Max(ibex::Interval alpha, ibex::Interval beta);
ibex::IntervalVector slerp(ibex::IntervalVector &q1, ibex::IntervalVector &q2, ibex::Interval t);
ibex::Interval polygon_swing_xz_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi);
ibex::Interval norm(ibex::IntervalVector &v);

int main() {

	bool check_shoulder_swing_limits = true;
	bool check_hand_swing_limits = true;
	ibex::Interval d3 = 28.74;
	ibex::Interval d8 = 3.76;

	// Clear directory for new list
	int dir_clean = system("exec rm -r manifold/list/*");

	// ################################################
	// SLERP ROUTINE TO GENERATE CONSTRAINT G
	// ################################################
	// Quaternion constraints
	int num_points = 8;
	double _thetaswing[num_points] = {1.04719755119660,	2.26892802759263,	0.872664625997165,	0.523598775598299,	2.09439510239320,	1.74532925199433,	2.09439510239320,	1.04719755119660};
	double _phiswing[num_points] = {-3.14159265358979,	-2.79252680319093,	-1.91986217719376,	-0.872664625997165,	0.0,	0.872664625997165,	1.91986217719376,	3.14159265358979};
	ibex::IntervalVector thetaswing = ibex::IntervalVector(ibex::Vector(8, _thetaswing));
	ibex::IntervalVector phiswing = ibex::IntervalVector(ibex::Vector(8, _phiswing));

	// Lists
	std::vector<ibex::Interval> phiswing_list;
	std::vector<ibex::Interval> thetaswing_list;
	std::vector<ibex::Interval> g_list;

	// Check quaternion pairs
	for (int index=0; index<num_points-1; index++){
		ibex::IntervalVector qi(4);
		qi[0] = (cos((0.5)*thetaswing[index]));
		qi[1] = (sin(phiswing[index])*sin((0.5)*thetaswing[index]));
		qi[2] = (0);
		qi[3] = (cos(phiswing[index])*sin((0.5)*thetaswing[index]));

		ibex::IntervalVector qj(4);
		qj[0] = (cos((0.5)*sign(thetaswing[index+1])*thetaswing[index+1]));
		qj[1] = (sin(phiswing[index+1])*sin((0.5)*thetaswing[index+1]));
		qj[2] = (0);
		qj[3] = (cos(phiswing[index+1])*sin((0.5)*thetaswing[index+1]));

		ibex::Interval t = 0;
		ibex::Interval dt = 1.0/75.0;
		while(t.ub()<=1.0){
			// Slerp to find intermediate quaternion
			ibex::IntervalVector qrel = slerp(qi, qj, t);

			// Convert quaternion to axis and angle
			ibex::IntervalVector u = qrel.subvector(1,3);
			ibex::Interval u_norm = norm(u);
			u = 1 / u_norm * qrel.subvector(1,3); // normalize
			ibex::Interval theta = 2 * atan2(u_norm, qrel[0]);
			ibex::Interval phi = atan2(u[0], u[2]);

//			std::cout << "theta: " << theta << std::endl;
//			std::cout << "phi: " << phi  << "\t" << t << std::endl;

			// Save theta and phi to list
			phiswing_list.push_back(phi);
			thetaswing_list.push_back(theta);
			g_list.push_back(cos(theta));

			// Increment t
			t += dt;
		}
	}

	// ################################################
	// CREATE SOLVER
	// ################################################
	ibex::System system("examples/Human-Arm/minibex/kinematics_and_shoulder_axial.mbx");
	std::cout << system << std::endl;
	// Contractors
	ibex::CtcHC4 hc4_1(system,0.01);
	ibex::CtcHC4 hc4acid_1(system,0.1,true);
	ibex::CtcAcid acid_1(system, hc4acid_1);
	ibex::CtcCompo compo_kinematics(hc4_1,acid_1);

	/* Create a smear-function bisection heuristic. */
	ibex::SmearSumRelative bisector(system, 1e-02);

	/* Create a "stack of boxes" (CellStack) (depth-first search). */
	ibex::CellStack buff;

	/* Vector precisions required on variables */
	ibex::Vector prec(6, 1e-02);

	/* Create a solver with the previous objects */
	ibex::Solver s(system, compo_kinematics, bisector, buff, prec, prec);

	// ################################################
	// CREATE HAND TWIST TRAJECTORY
	// ################################################
	struct HandTwist {
	  double tX = 20;
	  double tY = 0;
	  double tZ = 20;
	  double AX = 0;
	  double AY = 0;
	  double AZ = 1;
	  double gX = tX;
	  double gY = tY;
	  double gZ = tZ;
	} ;

	std::vector<HandTwist> hand_twist_list;
	for (int tY=70; tY>=-70; tY--){
		HandTwist hand_twist;
		hand_twist.tY = static_cast<double>(tY);
		hand_twist_list.push_back(hand_twist);
	}

	// ################################################
	// EVALUATE SOLVER OVER PSI-PHI GRID
	// ################################################
	// Visualize results - plot kinematic chain
	vibes::beginDrawing();

	int spacing = 5;
	int solutionNum = 1;
	std::vector<ibex::IntervalVector> psiphitwistSolutions;
	for (int twisti=0; twisti<hand_twist_list.size(); twisti++){
		HandTwist hand_twist = hand_twist_list[twisti];

		vibes::newFigure("Allowable Psi-Phi Discrete (Swivel-Twist)");
		vibes::axisLimits(-1,2*M_PI+1,-M_PI-1,M_PI+1);
		vibes::setFigureProperty("width",600);
		vibes::setFigureProperty("height",600);
		for (int degpsi=0; degpsi<359; degpsi+=spacing){
			ibex::Interval radpsi;
			radpsi = static_cast<double>(degpsi)*M_PI/180;

			for (int degphi=-180; degphi<179; degphi+=spacing){
					ibex::Interval radphi;
					radphi = static_cast<double>(degphi)*M_PI/180;

					// Set psi and phi in system_box
					ibex::IntervalVector psiphitwist(3);
					psiphitwist[0] = radpsi;
					psiphitwist[1] = radphi;
					psiphitwist[2] = twisti;
					ibex::IntervalVector system_box = system.box;
					system_box[find_var_index(system.f_ctrs.args(), "psi")] = radpsi;
					system_box[find_var_index(system.f_ctrs.args(), "phi")] = radphi;

					// Load hand twist values
					system_box[find_var_index(system.f_ctrs.args(), "tX")] = hand_twist.tX;
					system_box[find_var_index(system.f_ctrs.args(), "tY")] = hand_twist.tY;
					system_box[find_var_index(system.f_ctrs.args(), "tZ")] = hand_twist.tZ;
					system_box[find_var_index(system.f_ctrs.args(), "AX")] = hand_twist.AX;
					system_box[find_var_index(system.f_ctrs.args(), "AY")] = hand_twist.AY;
					system_box[find_var_index(system.f_ctrs.args(), "AZ")] = hand_twist.AZ;
					system_box[find_var_index(system.f_ctrs.args(), "gX")] = hand_twist.tX;
					system_box[find_var_index(system.f_ctrs.args(), "gY")] = hand_twist.tY;
					system_box[find_var_index(system.f_ctrs.args(), "gZ")] = hand_twist.tZ;

					// Check if system_box has a solution
					s.solve(system_box);
					std::cout << psiphitwist << std::endl;
					const ibex::CovIUList& cov = s.get_data();
					std::cout << s.get_data() << std::endl;
					if (cov.size()>0){

						std::vector<ibex::IntervalVector> solverSolutions;
						for (int i=0; i<cov.nb_inner(); i++) solverSolutions.push_back(cov.inner(i));
						for (int i=0; i<cov.nb_unknown(); i++) solverSolutions.push_back(cov.unknown(i));
//						for (int i=0; i<cov.nb_pending(); i++) solverSolutions.push_back(cov.pending(i));

						bool foundsolution = false;
						for (int isol=0; isol<1; isol++){
							// Conditions to check
							bool foundshoulderswingsolution = true;
							bool foundhandswingsolution = true;

							// Continue until solution found
							if (!foundsolution){
								ibex::IntervalVector sol_box = solverSolutions[isol];

								ibex::IntervalVector PEL(3);
								PEL[0] = sol_box[find_var_index(system.f_ctrs.args(), "rx4")];
								PEL[1] = sol_box[find_var_index(system.f_ctrs.args(), "ry4")];
								PEL[2] = sol_box[find_var_index(system.f_ctrs.args(), "rz4")];
								std::cout << "PEL " << PEL << std::endl;

								if (check_shoulder_swing_limits) {
	//								// Check hand swing constraints
	//								ibex::IntervalVector PEL_unit = 1/d3 * PEL;
	//								ibex::IntervalVector a_vect(4,0);
	//								a_vect[2] = -1; // y axis
	//								ibex::IntervalVector a_vect_prime(4,0);
	//								a_vect_prime[1] = PEL_unit[0];
	//								a_vect_prime[2] = PEL_unit[1];
	//								a_vect_prime[3] = PEL_unit[2];
	//								// Compute K
	//								ibex::Interval K = a_vect_prime * a_vect;
	//								ibex::IntervalVector U = ibex::cross(a_vect.subvector(1,3),a_vect_prime.subvector(1,3));
	//								ibex::Interval Anglephi = atan2(U[0],U[2]);
	//	//							std::cout << "PEL_unit " << PEL_unit << std::endl;
	//	//							std::cout << "a_vect_prime " << a_vect_prime << std::endl;
	//								std::cout << "Anglephi " << Anglephi << std::endl;
	//
	//								// Use Slerp to find thetaswing
	//								ibex::Interval gu = polygon_swing_xz_limit(Anglephi, thetaswing_list, phiswing_list);
	//								std::cout << gu << " <= " << K << " <= " << 1 << std::endl;
	//
	//								// Feasible solution found!
	//								if (K.ub() < gu.lb()){
	//									std::cout << "NOT feasible - shoulder swing" << std::endl;
	//									foundshoulderswingsolution = false;
	//								}
								}

								if (check_hand_swing_limits) {
	//								ibex::IntervalMatrix R40(3,3);
	//								R40[0][0] = sol_box[find_var_index(system.f_ctrs.args(), "Rux4")];
	//								R40[1][0] = sol_box[find_var_index(system.f_ctrs.args(), "Rvx4")];
	//								R40[2][0] = sol_box[find_var_index(system.f_ctrs.args(), "Rwx4")];
	//								R40[0][1] = sol_box[find_var_index(system.f_ctrs.args(), "Ruy4")];
	//								R40[1][1] = sol_box[find_var_index(system.f_ctrs.args(), "Rvy4")];
	//								R40[2][1] = sol_box[find_var_index(system.f_ctrs.args(), "Rwy4")];
	//								R40[0][2] = sol_box[find_var_index(system.f_ctrs.args(), "Ruz4")];
	//								R40[1][2] = sol_box[find_var_index(system.f_ctrs.args(), "Rvz4")];
	//								R40[2][2] = sol_box[find_var_index(system.f_ctrs.args(), "Rwz4")];
	//
	//								ibex::IntervalVector r8(3);
	//								r8[0] = 20;
	//								r8[1] = 0;
	//								r8[2] = 20;
	//								ibex::IntervalVector r7(3);
	//								r7[0] = sol_box[find_var_index(system.f_ctrs.args(), "rx7")];
	//								r7[1] = sol_box[find_var_index(system.f_ctrs.args(), "ry7")];
	//								r7[2] = sol_box[find_var_index(system.f_ctrs.args(), "rz7")];
	//
	//								// Check hand swing constraints
	//								ibex::IntervalVector PWH_unit = R40 * ( 1/d8 * (r8-r7));
	//								ibex::IntervalVector a_vect(4,0);
	//								a_vect[3] = 1; // y axis
	//								ibex::IntervalVector a_vect_prime(4,0);
	//								a_vect_prime[1] = PWH_unit[0];
	//								a_vect_prime[2] = PWH_unit[1];
	//								a_vect_prime[3] = PWH_unit[2];
	//								// Compute K
	//								ibex::Interval K = a_vect_prime * a_vect;
	//								ibex::IntervalVector U = ibex::cross(a_vect.subvector(1,3),a_vect_prime.subvector(1,3));
	//								ibex::Interval Anglephi = atan2(U[1],U[0]);
	//	//							std::cout << "PEL_unit " << PEL_unit << std::endl;
	//	//							std::cout << "a_vect_prime " << a_vect_prime << std::endl;
	//								std::cout << "Anglephi " << Anglephi << std::endl;
	//
	//								// Use Slerp to find thetaswing
	//								ibex::Interval gu = polygon_swing_xz_limit(Anglephi, thetaswing_list, phiswing_list);
	//								std::cout << gu << " <= " << K << " <= " << 1 << std::endl;
	//
	//								// Feasible solution found!
	//								if (K.ub() < gu.lb()){
	//									std::cout << "NOT feasible - hand swing" << std::endl;
	//									foundhandswingsolution = false;
	//								}

	//								ibex::IntervalVector Rw4(3);
	//								Rw4[0] = sol_box[find_var_index(system.f_ctrs.args(), "Rwx4")];
	//								Rw4[1] = sol_box[find_var_index(system.f_ctrs.args(), "Rwy4")];
	//								Rw4[2] = sol_box[find_var_index(system.f_ctrs.args(), "Rwz4")];
	//
	//								ibex::IntervalVector r8(3);
	//								r8[0] = 20;
	//								r8[1] = 0;
	//								r8[2] = 20;
	//								ibex::IntervalVector r7(3);
	//								r7[0] = sol_box[find_var_index(system.f_ctrs.args(), "rx7")];
	//								r7[1] = sol_box[find_var_index(system.f_ctrs.args(), "ry7")];
	//								r7[2] = sol_box[find_var_index(system.f_ctrs.args(), "rz7")];
	//
	//								// Check hand swing constraints
	//								ibex::IntervalVector PWH_unit = ( 1/d8 * (r8-r7));
	//								ibex::Interval Dotprod = Rw4 * PWH_unit;
	//
	//								std::cout << cos(1.4) << " <= " << Dotprod << " <= " << 1 << std::endl;
	//								// Check bounds
	//								if (Dotprod.lb() > 1 || Dotprod.ub() < cos(1.4)){
	//									std::cout << "NOT feasible - hand swing" << std::endl;
	//									foundhandswingsolution = false;
	//								}

								}
								// Check if all conditions are satisfied
								if (foundshoulderswingsolution && foundhandswingsolution){
									// Feasible solution found!
									std::cout << "SOLUTION" << std::endl;
									foundsolution = true;
									psiphitwistSolutions.push_back(psiphitwist);
									vibes::drawEllipse (psiphitwist[0].mid(), psiphitwist[1].mid(), 0.01, 0.01, 0.0, "b");
								}
							}
						}

						// No feasible solution found
						if (!foundsolution){
							std::cout << "NO SOLUTION" << std::endl;
							vibes::drawEllipse (psiphitwist[0].mid(), psiphitwist[1].mid(), 0.01, 0.01, 0.0, "r");
						}
						// Save feasible solution
						else {
							// Save solution
							std:string file = "manifold/list/manifold";
							file += std::to_string(solutionNum);
							file += ".txt";
							std::cout << file << std::endl;
							const char *cstr = file.c_str();
//							s.get_data().write(cstr);
							solutionNum++;
						}
					}
					else {
						vibes::drawEllipse (psiphitwist[0].mid(), psiphitwist[1].mid(), 0.01, 0.01, 0.0, "m");
					}
			}
		}
	}
	// Save psiphiSolutions to txt file
	std::ofstream myfile;
	myfile.open ("manifold/psi-phi-workspace.txt");
	for (int isol=0; isol<psiphitwistSolutions.size(); isol++){
		ibex::IntervalVector psiphi = psiphitwistSolutions[isol];
		myfile << psiphi[0].mid() << '\t' << psiphi[1].mid() << '\t' << psiphi[2].mid() << '\n';
	}
	myfile.close();


	return 0;
}

int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol){
	const char *cstr = symbol.c_str();
	for (int var_i=0; var_i<variable_list.size(); var_i++){
//		std::cout << variable_list[var_i].name << '\t' << symbol << std::endl;
		if (strcmp(variable_list[var_i].name, cstr) == 0){
//			std::cout << "Found index: " << var_i << std::endl;
			return var_i;
		}
	}
	return -1;
}

ibex::Interval Rot_Ext_Max(ibex::Interval alpha, ibex::Interval beta){
/*
 * Calculate the maximum external rotation of the humerus using formulation of Wang et al. 1998
 *
 * Input: alpha, beta : azimuth and elevation in radian
 * Output: Phi_Ext: maximum external rotation in radian
 */
	ibex::Interval x = alpha * cos(beta);
	ibex::Interval y = beta;

	double a_j[5] = {-2.459, 9.299, 3.428, -2.987, 0.331};
	double b_jm[5][5] = {{1, 0, 0, 0, 0},
			{1, -1.69, 0, 0, 0},
			{1, 8.615, -2.149, 0, 0},
			{1, 0.859, -8.025, 0.514, 0},
			{1, -13.771, -17.172, 35.46, 2.189}};

	ibex::Interval Phi_Ext = 0;
    for (int j=0; j<5; j++){
    	ibex::Interval P_j = 0;
    	for (int m=0; m<=j; m++){
    		P_j += b_jm[j][m] * pow(x,j-m) * pow(y,m);
    	}
    	Phi_Ext += a_j[j] * P_j;
    }

    return Phi_Ext;
}

ibex::Interval Rot_Int_Max(ibex::Interval alpha, ibex::Interval beta){
/*
 * Calculate the maximum internal rotation of the humerus using formulation of Wang et al. 1998
 *
 * Input: alpha, beta : azimuth and elevation in radian
 * Output: Phi_Ext: maximum internal rotation in radian
 */
	ibex::Interval x = alpha * cos(beta);
	ibex::Interval y = beta;

	double a_j[4] = {-139.27, 18.652, 4.092, -2.081};
	double b_jm[4][4] = {{1, 0, 0, 0},
			{1, -2.235,0,0},
			{1, 7.251, -0.654,0},
			{1, 0.069, -10.035, -1.642}};

	ibex::Interval Phi_Int = 0;
    for (int j=0; j<4; j++){
    	ibex::Interval Pj = 0;
    	for (int m=0; m<=j; m++){
    		Pj += b_jm[j][m] * pow(x,j-m) * pow(y,m);
    	}
    	Phi_Int += a_j[j] * Pj;
    }

    return Phi_Int;
}

ibex::IntervalVector slerp(ibex::IntervalVector &q1, ibex::IntervalVector &q2, ibex::Interval t){
	// ################################################
	// SLERP ROUTINE TO GENERATE CONSTRAINT G
	// ################################################
	ibex::Interval dotqq = (q1[0]*q2[0]+q1[1]*q2[1]+q1[2]*q2[2]+q1[3]*q2[3]);
//	ibex::Interval thetaqq = acos(dotqq);
	ibex::Interval scale0 = sin((1.0 - t) * acos(dotqq)) / sin(acos(dotqq));
	ibex::Interval scale1 = sign(dotqq)*sin((t * acos(dotqq))) / sin(acos(dotqq));
//	std::cout << "scale0 " << scale0 << std::endl;
//	std::cout << "scale1 " << scale1 << std::endl;
	ibex::IntervalVector q1q2t = scale0 * q1 + scale1 * q2;
	ibex::IntervalVector q1q2t_unit = 1/sqrt(sqr(q1q2t[0])+sqr(q1q2t[1])+sqr(q1q2t[2])+sqr(q1q2t[3])) * q1q2t;
	return q1q2t_unit;
}

ibex::Interval norm(ibex::IntervalVector &v){
	ibex::Interval norm_val = 0;
	for (int i=0; i<v.size(); i++)
		norm_val += sqr(v[i]);
	norm_val = sqrt(norm_val);
	return norm_val;
}

ibex::IntervalVector quatexp(ibex::IntervalVector &v){
	ibex::Interval v_norm = norm(v);
	ibex::IntervalVector quat(4);
	quat[0] = cos(v_norm);
	ibex::IntervalVector temp = sin(v_norm)/v_norm * v;
	quat[1] = temp[0];
	quat[2] = temp[1];
	quat[3] = temp[2];
	return quat;
}

ibex::Interval polygon_swing_xz_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi){

	// Find the indices of vphi which bound the value of phi
	int vphi_index1 = 0;
	int vphi_index2 = 0;
	for (int i=0; i<vphi.size(); i++){
		if (phi.lb() >= vphi[i].ub()){
			vphi_index1 = i;
			vphi_index2 = i+1;
		}
	}
	// Check if vphi_index2 needs to be corrected (overflows)
	if (vphi_index2>=vphi.size())
		vphi_index2 = 0;
//	std::cout << vphi.size() << std::endl;
//	std::cout << "vphi_index1 " << vphi_index1 << std::endl;
//	std::cout << "vphi_index2 " << vphi_index2 << std::endl;

	ibex::Interval phi1 = vphi[vphi_index1];
	ibex::Interval phi2 = vphi[vphi_index2];
	ibex::Interval theta1 = vtheta[vphi_index1];
	ibex::Interval theta2 = vtheta[vphi_index2];
//    std::cout << "phi1 " << phi1 << std::endl;
//    std::cout << "phi2 " << phi2 << std::endl;
//    std::cout << "theta1 " << theta1 << std::endl;
//    std::cout << "theta2 " << theta2 << std::endl;

	// Slerp
	ibex::Interval t = (phi - phi1) / (phi2 - phi1);
	ibex::IntervalVector u1(3);
	u1[0] = sin(phi1);
	u1[1] = 0;
	u1[2] = cos(phi1);
	ibex::IntervalVector v1 = theta1/2 * u1;
	ibex::IntervalVector q1 = quatexp(v1);

	ibex::IntervalVector u2(3);
	u2[0] = sin(phi2);
	u2[1] = 0;
	u2[2] = cos(phi2);
	ibex::IntervalVector v2 = theta2/2 * u2;
	ibex::IntervalVector q2 = quatexp(v2);

	ibex::IntervalVector qrel = slerp(q1,q2,t);

//	std::cout << "v1 " << v1 << std::endl;
//	std::cout << "v2 " << v2 << std::endl;
//	std::cout << "q1 " << q1 << std::endl;
//	std::cout << "q2 " << q2 << std::endl;
//	std::cout << "t " << t << std::endl;
//	std::cout << "qrel " << qrel << std::endl;

	// Swing limit (theta max)
	ibex::IntervalVector qrel_vect = qrel.subvector(1,3);
	ibex::Interval theta = 2 *atan2(norm(qrel_vect), qrel[0]);
	return cos(theta);
}








