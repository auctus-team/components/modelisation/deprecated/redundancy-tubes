/*
 * Solve_arm_twist_swivel.cpp
 *
 *  Created on: Dec 5, 2018
 *  Author: Joshua Pickard
 */

#include "ibex.h"
#include "vibes.h"
#include <iostream>
#include <fstream>

int main() {

	// ################################################
	// CREATE SOLVER
	// ################################################
	ibex::System system("examples/Human-Arm/minibex/Arm_minibex_shoulder_slerp_constraints.txt");
	std::cout << system << std::endl;

	// Contractors
	ibex::CtcHC4 hc4(system,0.01);
	ibex::CtcHC4 hc4_2(system,0.1,true);
	ibex::CtcAcid acid(system, hc4_2);
	ibex::CtcCompo compo(hc4,acid);

	/* Create a smear-function bisection heuristic. */
	ibex::SmearSumRelative bisector(system, 1e-02);

	/* Create a "stack of boxes" (CellStack) (depth-first search). */
	ibex::CellStack buff;

	/* Vector precisions required on variables */
	ibex::Vector prec(6, 1e-02);

	/* Create a solver with the previous objects */
	ibex::Solver s(system, compo, bisector, buff, prec, prec);

	// ################################################
	// EVALUATE SOLVER OVER PSI-PHI GRID
	// ################################################
	// Visualize results - plot kinematic chain
	vibes::beginDrawing();
	vibes::newFigure("Allowable Psi-Phi (Swivel-Twist)");
	vibes::axisLimits(-1,M_PI*2+1,-M_PI-1,M_PI+1);
	vibes::setFigureProperty("width",600);
	vibes::setFigureProperty("height",600);

	int spacing = 5;
	int solutionNum = 1;
	std::vector<ibex::IntervalVector> psiphiSolutions;
	for (int degpsi=0; degpsi<359; degpsi+=spacing){
		ibex::Interval radpsi;
		radpsi = static_cast<double>(degpsi)*M_PI/180;

		for (int degphi=-180; degphi<179; degphi+=spacing){
				ibex::Interval radphi;
				radphi = static_cast<double>(degphi)*M_PI/180;

				// Set psi and phi in system_box
				ibex::IntervalVector psiphi(2);
				psiphi[0] = radpsi;
				psiphi[1] = radphi;
				ibex::IntervalVector system_box = system.box;
				system_box[28] = radpsi;
				system_box[29] = radphi;

				// Check if system_box has a solution
				s.solve(system_box);
				std::cout << psiphi << std::endl;
				std::cout << s.get_manifold().size() << std::endl;
				if (s.get_manifold().size()>0){
					psiphiSolutions.push_back(psiphi);
					vibes::drawEllipse (psiphi[0].mid(), psiphi[1].mid(), 0.01, 0.01, 0.0, "b");

					// Save solution
					std:string file = "manifold/list/manifold";
					file += std::to_string(solutionNum);
					file += ".txt";
					std::cout << file << std::endl;
					const char *cstr = file.c_str();
					s.get_manifold().write_txt(cstr);
					solutionNum++;
				}
		}
	}
	// Save psiphiSolutions to txt file
	std::ofstream myfile;
	myfile.open ("manifold/psi-phi-workspace.txt");
	for (int isol=0; isol<psiphiSolutions.size(); isol++){
		ibex::IntervalVector psiphi = psiphiSolutions[isol];
		myfile << psiphi[0].mid() << '\t' << psiphi[1].mid() << '\n';
	}
	myfile.close();


	return 0;
}





