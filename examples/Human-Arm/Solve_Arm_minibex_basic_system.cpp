/*
 * Solve_Arm_minibex_basic_system.cpp
 *
 *  Created on: Dec 18, 2018
 *      Author: jpickard
 */

#include "ibex.h"
#include "LinearAlgebra.h"
#include <cassert>
#include <iostream>
#include <iomanip>

using namespace kcadl;

static ibex::IntervalMatrix InverseTransformation(ibex::IntervalMatrix &A);
static ibex::IntervalMatrix TransformationDHModified(ibex::Interval a, ibex::Interval alpha, ibex::Interval d, ibex::Interval theta);
void printIntervalMatrix(ibex::IntervalMatrix &A);

int main() {
	ibex::System system("examples/Human-Arm/minibex/Arm_kinematics_basic_system.mbx");
	std::cout << system << std::endl;

	// Zero transforms required to compute joint angles
	ibex::Interval d3(28.74,28.74);
	ibex::Interval d5=(27.15,27.15);
	ibex::Interval d8=(3.76,3.76);

	std::vector<ibex::IntervalMatrix> listOfZeroTransforms;
	ibex::IntervalMatrix T01 = TransformationDHModified(0,0,0,0);
	listOfZeroTransforms.push_back(T01);
	ibex::IntervalMatrix T12 = TransformationDHModified(0,M_PI/2,0,0);
	listOfZeroTransforms.push_back(T12);
	ibex::IntervalMatrix T23 = TransformationDHModified(0,-M_PI/2,d3,0);
	listOfZeroTransforms.push_back(T23);
	ibex::IntervalMatrix T34 = TransformationDHModified(0,M_PI/2,0,0);
	listOfZeroTransforms.push_back(T34);
	ibex::IntervalMatrix T45 = TransformationDHModified(0,-M_PI/2,d5,0);
	listOfZeroTransforms.push_back(T45);
	ibex::IntervalMatrix T56 = TransformationDHModified(0,M_PI/2,0,0);
	listOfZeroTransforms.push_back(T56);
	ibex::IntervalMatrix T67 = TransformationDHModified(0,-M_PI/2,0,0);
	listOfZeroTransforms.push_back(T67);
	ibex::IntervalMatrix T78 = TransformationDHModified(0,0,d8,0);
	listOfZeroTransforms.push_back(T78);

	/* ============================ building contractors ========================= */
	ibex::CtcHC4 hc4(system,0.01);

	ibex::CtcHC4 hc4_2(system,0.1,true);

	ibex::CtcAcid acid(system, hc4_2);

	ibex::CtcCompo compo(hc4,acid);
	/* =========================================================================== */

	/* Create a smear-function bisection heuristic. */
	ibex::SmearSumRelative bisector(system, 1e-02);

	/* Create a "stack of boxes" (CellStack) (depth-first search). */
	ibex::CellStack buff;

	/* Vector precisions required on variables */
	ibex::Vector prec(6, 1e-02);

	/* Create a solver with the previous objects */
	ibex::Solver s(system, compo, bisector, buff, prec, prec);

	/* Run the solver */
//	s.cell_limit = 1000;
//	s.time_limit = 100;
//	s.trace = 1;
	s.solve(system.box);

	/* Display the solutions */
	ibex::Manifold manifold = s.get_manifold();
	std::cout << manifold.size() << std::endl;
	std::cout << manifold << std::endl;
	manifold.write_txt("manifold/manifold.txt");

	// Compile the transformation matrices for each segment
	std::vector<ibex::QualifiedBox> solverSolutions;
	solverSolutions.insert(solverSolutions.end(), manifold.inner.begin(), manifold.inner.end());
	solverSolutions.insert(solverSolutions.end(), manifold.boundary.begin(), manifold.boundary.end());
	solverSolutions.insert(solverSolutions.end(), manifold.unknown.begin(), manifold.unknown.end());

	int solutionNum=0;
	while(solverSolutions.size()>0){
		// Structure to save solutions
		std::cout << "#############################" << std::endl;
		std::cout << "Solution: " << solutionNum << std::endl;
		solutionNum++;
		ibex::IntervalVector box = solverSolutions.back().existence();
		solverSolutions.pop_back();
//		std::cout << box << std::endl;

		// Generate transformation matrices
		std::vector<ibex::IntervalMatrix> listOfTransforms;
		int n = 8;
		for (int i=0; i<8; i++){
			ibex::Interval Rux = box[i];
			ibex::Interval Ruy = box[i+n];
			ibex::Interval Ruz = box[i+2*n];
			ibex::Interval Rvx = box[i+3*n];
			ibex::Interval Rvy = box[i+4*n];
			ibex::Interval Rvz = box[i+5*n];
			ibex::Interval Rwx = box[i+6*n];
			ibex::Interval Rwy = box[i+7*n];
			ibex::Interval Rwz = box[i+8*n];
			ibex::Interval rx = box[i+9*n];
			ibex::Interval ry = box[i+10*n];
			ibex::Interval rz = box[i+11*n];
			ibex::IntervalMatrix Transform(4,4);
			Transform[0][0] = Rux;
			Transform[1][0] = Ruy;
			Transform[2][0] = Ruz;
			Transform[0][1] = Rvx;
			Transform[1][1] = Rvy;
			Transform[2][1] = Rvz;
			Transform[0][2] = Rwx;
			Transform[1][2] = Rwy;
			Transform[2][2] = Rwz;
			Transform[0][3] = rx;
			Transform[1][3] = ry;
			Transform[2][3] = rz;
			Transform[3][0] = 0;
			Transform[3][1] = 0;
			Transform[3][2] = 0;
			Transform[3][3] = 1;
//			std::cout << "Transform" << std::endl;
//			std::cout << "T" << 0 << ',' << i+1 << std::endl;
//			std::cout << Transform << std::endl;
			listOfTransforms.push_back(Transform);
		}

		// Solve for joint angles using exponential matrices
		for (int joint=0; joint<n-1; joint++){
			ibex::IntervalMatrix T_0_i = listOfTransforms[joint];
			ibex::IntervalMatrix T_im1_i = T_0_i;
			std::cout << joint << std::endl;
			if (joint > 0){
				// Transforms i,i+1
				ibex::IntervalMatrix T_0_im1 = listOfTransforms[joint-1];
				std::cout << "T_0_im1" << std::endl;
				std::cout << T_0_im1 << std::endl;
				T_im1_i = InverseTransformation(T_0_im1) * T_0_i;
			}

			// Zero Transform T_i_prev_ZERO
			ibex::IntervalMatrix T_im1_i_ZERO = listOfZeroTransforms[joint];

			std::cout << "T_0_i" << std::endl;
			std::cout << T_0_i << std::endl;
			printIntervalMatrix(T_0_i);
			std::cout << "T_im1_i" << std::endl;
			std::cout << T_im1_i << std::endl;
			std::cout << "InverseTransformation(T_im1_i)" << std::endl;
			std::cout << InverseTransformation(T_im1_i) << std::endl;
			std::cout << "T_im1_i_ZERO" << std::endl;
			std::cout << T_im1_i_ZERO << std::endl;

			// Compute expontential matrix
			ibex::IntervalMatrix exponentialMatrix = InverseTransformation(T_im1_i) * T_im1_i_ZERO;

			std::cout << "exponentialMatrix" << std::endl;
			std::cout << exponentialMatrix << std::endl;

			// Paden-Kahan subproblem 1:
			ibex::IntervalVector pointR_3(3, 0); // Point on twist axis
			pointR_3[2] = -1;
			ibex::IntervalVector pointP_3(3, 0); // Point not on twist axis
			pointP_3[1] = 1;
			ibex::IntervalVector pointP_4(4,1); // Used for multiplication with exponentialMatrix
			pointP_4[0] = pointP_3[0];
			pointP_4[1] = pointP_3[1];
			pointP_4[2] = pointP_3[2];
			ibex::IntervalVector pointQ_4 = exponentialMatrix * pointP_4; // Point not on twist axis
			ibex::IntervalVector pointQ_3 = pointQ_4.subvector(0,2);
			ibex::IntervalVector vectorU = pointP_3 - pointR_3;
			ibex::IntervalVector vectorV = pointQ_3 - pointR_3;
			ibex::IntervalVector vectorW(3, 0); // Unit vector along twist axis
			vectorW[2] = 1;
			ibex::IntervalMatrix matrixW(3,1);
			matrixW[0][0] = vectorW[0];
			matrixW[1][0] = vectorW[1];
			matrixW[2][0] = vectorW[2];
			ibex::IntervalVector vectorU2 = vectorU - matrixW * matrixW.transpose() * vectorU; // Projected onto XY plane
			ibex::IntervalVector vectorV2 = vectorV - matrixW * matrixW.transpose() * vectorW; // Projected onto XY plane

			std::cout << "dots " << vectorW *vectorU  << '\t' << vectorW *vectorV  << std::endl;
			std::cout << "norms " << ibex::norm(vectorU2.mid()) << '\t' << ibex::norm(vectorV2.mid())  << std::endl;

			ibex::Interval tan2y = vectorW*(ibex::cross(vectorU2,vectorV2));
			ibex::Interval tan2x = vectorU2*vectorV2;
			ibex::Interval theta = atan2(tan2y, tan2x);
			std::cout << "theta: " << joint+1 << theta << "rads\t" << theta*180/M_PI << "degs" << std::endl;

//			// Save joint angle to vector
//			jointValues[joint] = theta;
		}
	}

	/* Report performances */
	std::cout << "cpu time used=" << s.get_time() << "s."<< std::endl;
	std::cout << "number of cells=" << s.get_nb_cells() << std::endl;
	return 0;
}

inline ibex::IntervalMatrix InverseTransformation(ibex::IntervalMatrix &A) {
	ibex::IntervalMatrix inverseTransform = A;
	// Orientation
	ibex::IntervalMatrix Ainv = A.submatrix(0,2,0,2).transpose();
	inverseTransform.put(0,0,Ainv);
	// Position
	ibex::IntervalVector P = A.col(3).subvector(0,2);
	inverseTransform.put(0,3, -Ainv*P,false);


	return inverseTransform;
}

inline ibex::IntervalMatrix TransformationDHModified(ibex::Interval a, ibex::Interval alpha, ibex::Interval d, ibex::Interval theta){
	ibex::IntervalMatrix Transformation(4,4);
	Transformation[0][0] = cos(theta);
	Transformation[0][1] = -sin(theta);
	Transformation[0][2] = 0;
	Transformation[0][3] = a;
	Transformation[1][0] = sin(theta)*cos(alpha);
	Transformation[1][1] = cos(theta)*cos(alpha);
	Transformation[1][2] = -sin(alpha);
	Transformation[1][3] = -d*sin(alpha);
	Transformation[2][0] = sin(theta)*sin(alpha);
	Transformation[2][1] = cos(theta)*sin(alpha);
	Transformation[2][2] = cos(alpha);
	Transformation[2][3] = d*cos(alpha);
	Transformation[3][0] = 0;
	Transformation[3][1] = 0;
	Transformation[3][2] = 0;
	Transformation[3][3] = 1;
	return Transformation;
}

double roundup5decimal(double var)
{
	double value = (int)(var * 100000 + .5);
	return (double)value / 100000;
}

double rounddown5decimal(double var)
{
	double value = (int)(var * 100000 - .5);
	return (double)value / 100000;
}

void printIntervalMatrix(ibex::IntervalMatrix &A){
	for (int i=0; i<A.nb_rows(); i++){
		for (int j=0; j<A.nb_cols(); j++){
			std::cout << '[' << rounddown5decimal(A[i][j].lb()) << ',' << roundup5decimal(A[i][j].ub()) << "]\t";
		}
		std::cout << std::endl;
	}
}






