Redundancy Tube Evaluation {#page2}
=========

# Robot-Operating-System (ROS) Tool
To install ROS packages:
```bash
cd catkin_ws && ./install.sh
```

To run human_arm_redundancy_modelling package use:

```bash
source /opt/ros/melodic/setup.bash
source ./devel/setup.bash
export LC_NUMERIC="en_US.UTF-8"
roslaunch human_arm_redundancy_modelling redundancy_gui.launch
```

This will open RViz and a python tool to aid in visualising and setting the redundancy parameters. 
Only valid poses are sent to RViz. 
Clicking the button "Compute Redundant Workspace at Pose" generates the redundant workspace at a pose 
(currently the default pose) and plots the inside and outside boxes in the figure (this can take a while). 
The redundant angles: hand twist and elbow swivel, can be adjusted while the workspace is computing.

Hand twist:

![hand_twist](hand_twist.gif)

Elbow swivel:

![elbow_swivel](elbow_swivel.gif)

   - If RViz does not display objects, try:

        `export LC_NUMERIC="en_US.UTF-8"`

   - Check and visualise URDF/XACRO files

        `check_urdf *.urdf`

        `urdf_to_graphiz *.urdf`

   - Source setup.bash

        `cd ~/catkin_ws && source ./devel/setup.bash`

   - Launch files

        `roslaunch human_arm_redundancy_modelling display.launch`

# Computing Redundant Workspace / Redundancy Tube
The redundant workspace and redundancy tube can also be computed without utilising ROS.
By default, the inside, outside, and boundary boxes are saved to `/tmp/workspace.txt`.

![redundant_workspace_computation](redundant_workspace_computation.gif)

To compute the redundant workspace adjust the pose in `examples/redundancy-tube/minibex/pose.mbx` then run:

```bash
./examples/redundancy-tube/Redundant_workspace
```

To compute the redundancy tube adjust the parameters in `examples/redundancy-tube/minibex/trajectory.mbx`

```bash
./examples/redundancy-tube/Redundant_workspace_tube
```

Many arguments can be specified for the redundancy tube solver:

- -h : displays help instructions
- -t < tmin > < tmax > : trajectory interval (in the range [0,1]) to be evaluated
- -sl < slices > : slice the trajectory with a specified number slices
- -i < input file name > : name of input file to load for further refinement
- -ri < inner loop resolution > : the resolution used in the inner loop
- -ro < outer loop angle resolution > < outer loop trajectory resolution >: the two resolutions used in the outer loop

Examples:

To compute 5 slices (t=0,0.25,0.5,0.75,1) of the redundancy tube with specific resolutions use:
```bash
./examples/redundancy-tube/Redundant_workspace_tube -t 0 1 -sl 5 -ri 0.1 -ro 0.05 0.01
```

To compute the redundancy tube with specific resolutions use:
```bash
./examples/redundancy-tube/Redundant_workspace_tube -t 0 1 -ri 0.1 -ro 0.05 0.01
```

# Visualise the Redundant Workspace / Redundancy Tube
A plotting tool utilising VTK can be used to visualise the redundant workspace or redundancy tube. 
This can be used in parallel with the redundant workspace and redundancy tube solvers to visualise their progress. 

![vtk_plotter-redundant_workspace](vtk_plotter-redundant_workspace.png)
![vtk_plotter-redundancy_tube_slices](vtk_plotter-redundancy_tube_slices.png)
![vtk_plotter-redundancy_tube](vtk_plotter-redundancy_tube.png)

The following keyboard shortcuts are enabled:

- 'q'/'e' - exit
- 'r' - refresh
- 'v' - set to default view
- 'w' - wireframe view
- 's' - surface view
- 'z' - set to Z axis view
- 'y' - set to Y axis view
- 'x' - set to X axis view
- 'i' - toggle inside box visibility
- 'o' - toggle outside box visibility
- 'b' - toggle boundary box visibility

To run the plotting tool with the default input file (/tmp/workspace.txt) run:

```bash
cd utilities/kcadl_vtk_plotter
python vtk_plotter.py
```

Other input files can be viewed with:
```bash
cd utilities/kcadl_vtk_plotter
python vtk_plotter.py -i <input_file>
```
Several example files are provided in utilities/kcadl_vtk_plotter/data.

Each execution of kcadl_vtk_plotter generates 3 stl files in the data directory which can be utilised in external software:
- boundary_mesh.stl
- inside_mesh.stl
- outside_mesh.stl

