Installation {#page}
=========

Tested on Ubuntu 16.04, Ubuntu 18.04

To install run:

```bash
git clone https://gitlab.inria.fr/auctus/redundancy-tubes.git
cd redundancy-tubes
./install.sh
```

(Option 1) To build ROS packages run:
```bash
cd redundancy-tubes/catkin_ws
catkin build
source ./devel/setup.bash
```

(Option 2) To install ROS and build ROS packages run:
```bash
cd redundancy-tubes/catkin_ws
./install.sh
```
