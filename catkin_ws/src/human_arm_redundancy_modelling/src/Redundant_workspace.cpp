/*
 * redundant_workspace.cpp
 *
 *  Created on: Jan 11, 2019
 *      Author: jpickard
 */

#include "ibex.h"
//#include "vibes.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <utility>
#include "LinearAlgebra.h"
#include <ctime>

#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Vector3.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float32.h>
#include "std_msgs/String.h"
#include "human_arm_redundancy_modelling/RedundancyBox.h"
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <cmath>

#if defined(_OPENMP)
#include <omp.h>
#include <thread>
#endif

//int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, const char* symbol);
int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol);
void copy_vars(const ibex::Array<const ibex::ExprSymbol> &variable_list1, ibex::IntervalVector &box1,
		const ibex::Array<const ibex::ExprSymbol> &variable_list2, ibex::IntervalVector &box2);
std::pair<ibex::IntervalVector,ibex::IntervalVector> bisect_at(ibex::IntervalVector &Vector, int index);
int max_diam_index(ibex::IntervalVector &Vector);

int verify_shoulder_axial_rotation(ibex::System &axial_system, ibex::CtcCompo &axial_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, ibex::Interval &psi,
		int verbose);
int verify_swing(ibex::IntervalVector &thetaswing, ibex::IntervalVector &phiswing,
		ibex::System &swing_system, ibex::CtcCompo &swing_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box,
		int verbose);
int kinematics_existence(ibex::System &system_kinematics, ibex::IntervalVector &kinematics_box, int verbose);

int main(int argc, char** argv)  {
	// ROS node initialization
	ros::init(argc, argv, "redundant_workspace_solver");
	ros::NodeHandle node;

	// ROS publisher
	ros::Publisher redundancy_box_pub = node.advertise<human_arm_redundancy_modelling::RedundancyBox>("redundancy_box", 100);
	ros::Rate loop_rate(10);

	// Verbose flag (-1: nothing, 0: warnings only, 1: general messages, 2: more information)
	int verbose = -1;

	// Setup OpenMP
	int num_threads = 1;
#if defined(_OPENMP)
	unsigned int nthreads = std::thread::hardware_concurrency();
	omp_set_num_threads(nthreads);
	num_threads = omp_get_num_threads();
#endif

	// Clear directory for new list
	int dir_clean = system("exec rm -r manifold/list/*");

	/* ################################################
	LOAD SYSTEMS FROM MINIBEX FILES
	################################################ */
	enum{
		kinematics=0,
		shoulder_axial,
		shoulder_swing,
		hand_swing
	};
	std::vector<const char*> system_filenames;
	system_filenames.push_back("../../../src/human_arm_redundancy_modelling/minibex/kinematics_ZYX.mbx");
	system_filenames.push_back("../../../src/human_arm_redundancy_modelling/minibex/shoulder_axial.mbx");
	system_filenames.push_back("../../../src/human_arm_redundancy_modelling/minibex/shoulder_swing.mbx");
	system_filenames.push_back("../../../src/human_arm_redundancy_modelling/minibex/hand_swing.mbx");

	// Load system to list
	std::vector<ibex::System> system_list;
	for (int system_i=0; system_i<system_filenames.size(); system_i++){
		// Filename
		const char * filename = system_filenames[system_i];

		// Create system
		try{
			ibex::System system(filename);
			system_list.push_back(system);
		} catch (...){
			std::cerr << "Error in minibex file: " << filename << std::endl;
			return 0;
		}
	}

	// Shoulder quaternion constraints (To approximate Wang et al. 1998.)
	int num_shoulder_points = 18;
	double _shoulderphiswing[num_shoulder_points] = {-M_PI, -2.047099962, -1.570796327, -.8868510796, -.7439121072, -.6839452473, -.3872228929, 0., .2088928627, .6224288550, .6938260469, .8868510796, 1.570796327, 2.254741574, 2.356194490, 2.563319152, 2.780739254, M_PI};
	double _shoulderthetaswing[num_shoulder_points] = {.9995080490, .8345071701, .7853981634, .9995080490, 1.742730040, 1.900344531, 2.069514319, 2.112549334, 2.091560103, 2.069514319, 1.742730040, .9995080490, .7853981634, .9995080490, 1.107148718, 1.398862614, 1.170944762, .9995080490};
	ibex::IntervalVector shoulderthetaswing = ibex::IntervalVector(ibex::Vector(num_shoulder_points, _shoulderthetaswing));
	ibex::IntervalVector shoulderphiswing = ibex::IntervalVector(ibex::Vector(num_shoulder_points, _shoulderphiswing));

	// Wrist quaternion constraints (To approximate Gehrmann at al. 2008.)
	int num_wrist_points = 9;
	double _wristphiswing[num_wrist_points] = {-M_PI, -2.356194490192345, -1.570796326794897, -0.785398163397448, 0 , 0.785398163397448, 1.570796326794897, 2.356194490192345, M_PI};
	double _wristthetaswing[num_wrist_points] = {0.698131700797732, 0.698131700797732, 0.698131700797732, 0.698131700797732, 0.698131700797732, 0.523598775598299, 0.785398163397448, 1.396263401595464, 0.698131700797732};
	ibex::IntervalVector wristthetaswing = ibex::IntervalVector(ibex::Vector(num_wrist_points, _wristthetaswing));
	ibex::IntervalVector wristphiswing = ibex::IntervalVector(ibex::Vector(num_wrist_points, _wristphiswing));

	/* ################################################
	EVALUATE SWIVEL-TWIST WORKSPACE
	################################################ */
	// Visualize results - plot kinematic chain
//	vibes::beginDrawing();
//	vibes::newFigure("Allowable Psi-Phi (Swivel-Twist)");
//	vibes::axisLimits(-M_PI/2-1,3*M_PI/2+1,-M_PI-1,M_PI+1);
//	vibes::setFigureProperty("width",600);
//	vibes::setFigureProperty("height",600);

	// Redundant angles
	ibex::IntervalVector psiphi(2);
	psiphi[0] = ibex::Interval(-M_PI/2,3*M_PI/2); // swivel angle
	psiphi[1] = ibex::Interval(-M_PI,M_PI); // twist angle
	std::deque<ibex::IntervalVector> psiphi_list;
	psiphi_list.push_back(psiphi);

	//  Redundant angles bisector
	ibex::LargestFirst psiphi_bisector(ibex::Vector(2,0.01), 0.5);

	// Variables not to be bisected.
	std::vector<std::string> do_not_bisect_list = 	{"d3",
													"d5",
													"d8",
													"phi",
													"psi",
													"tX",
													"tY",
													"tZ"};

	// Solver
	bool repeat = true;
#pragma omp parallel shared(psiphi_list, repeat)
{
	int thread_id = 0;
#if defined(_OPENMP)
		thread_id = omp_get_thread_num();
//		std::cout << "thread: " << thread_id << std::endl;
#endif

	// SYSTEM
	// Copy systems for each thread
	ibex::System system_kinematics(system_list[kinematics], ibex::System::COPY);
	ibex::System system_shoulder_axial(system_list[shoulder_axial], ibex::System::COPY);
	ibex::System system_shoulder_swing(system_list[shoulder_swing], ibex::System::COPY);
	ibex::System system_hand_swing(system_list[hand_swing], ibex::System::COPY);

	// CONTRACTORS
	// KINEMATICS //
	// Build contractors (rebuilding in loop improves contraction performance)
	ibex::CtcHC4 hc4_kinematics(system_kinematics,0.01);
	ibex::CtcHC4 hc4acid_kinematics(system_kinematics,0.1,true);
	ibex::CtcAcid acid_kinematics(system_kinematics, hc4acid_kinematics);
	ibex::CtcCompo kinematics_contractor(hc4_kinematics,acid_kinematics);

	// SHOULDER AXIAL ROTATION //
	// Build contractors
	ibex::CtcHC4 hc4_shoulder_axial(system_shoulder_axial,0.01);
	ibex::CtcHC4 hc4acid_shoulder_axial(system_shoulder_axial,0.1,true);
	ibex::CtcAcid acid_shoulder_axial(system_shoulder_axial, hc4acid_shoulder_axial);
	ibex::CtcCompo shoulder_axial_contractor(hc4_shoulder_axial,acid_shoulder_axial);

	// SHOULDER SWING //
	// Build contractors
	ibex::CtcHC4 hc4_shoulder_swing(system_shoulder_swing,0.01);
	ibex::CtcHC4 hc4acid_shoulder_swing(system_shoulder_swing,0.1,true);
	ibex::CtcAcid acid_shoulder_swing(system_shoulder_swing, hc4acid_shoulder_swing);
	ibex::CtcCompo shoulder_swing_contractor(hc4_shoulder_swing,acid_shoulder_swing);

	// HAND SWING //
	// Build contractors
	ibex::CtcHC4 hc4_hand_swing(system_hand_swing,0.01);
	ibex::CtcHC4 hc4acid_hand_swing(system_hand_swing,0.1,true);
	ibex::CtcAcid acid_hand_swing(system_hand_swing, hc4acid_hand_swing);
	ibex::CtcCompo hand_swing_contractor(hc4_hand_swing,acid_hand_swing);

	// VARIABLE LOOKUP TABLE //
	int system_kinematics_psi = find_var_index(system_kinematics.f_ctrs.args(), "psi");
	int system_kinematics_phi = find_var_index(system_kinematics.f_ctrs.args(), "phi");
	std::vector<int> do_not_bisect_list_indices;
	for (int var_i=0; var_i<do_not_bisect_list.size(); var_i++)
		do_not_bisect_list_indices.push_back(find_var_index(system_kinematics.f_ctrs.args(), do_not_bisect_list[var_i]));

	while(repeat){
		if(psiphi_list.size()>0 && thread_id < psiphi_list.size()){
//			std::cout << "thread: " << thread_id << " psiphi_list.size(): " << psiphi_list.size() << std::endl;
			// Pop redundant angles from list
			ibex::IntervalVector psiphi_(2);
	#pragma omp critical /* Threads update by turns */
			{
				psiphi_ = psiphi_list.front();
//				if (verbose >= 0) std::cout << "psiphi_ " << psiphi_ << std::endl;
				psiphi_list.pop_front();
			}

			// KINEMATICS//
			ibex::IntervalVector kinematics_box = system_kinematics.box;
			kinematics_box[system_kinematics_psi] = psiphi_[0];
			kinematics_box[system_kinematics_phi] = psiphi_[1];

			// List to store kinematics variables
			std::vector<ibex::IntervalVector> kinematic_list;
			kinematic_list.clear();
			kinematic_list.push_back(kinematics_box);
			ibex::IntervalVector psipsi_simp(2);
			// Vector used for bisection (some elements are set to zero to avoid bisection)
			ibex::IntervalVector kinematics_box_bisection(kinematics_box.size());
			int boundary_size = 0;
			int solution_size = 0;
			int num_bisections = 0;
			int num_nosolutions = 0;
			int kinematics_verified = 0;
			int shoulder_axial_rotation_verified = 0;
			int shoulder_swing_verified = 0;
			int hand_swing_verified = 0;
			bool verify_shoulder_axial = true;
			bool verify_shoulder_swing = true;
			bool verify_hand_swing = true;
			int classification = 0;
			int bisect_index;

			int max_bisections = 5;
			if (psiphi_.diam().max() < 0.1)
				max_bisections = 100;

			// VERIFICATION LOOP //
			while(kinematic_list.size()>0 && num_bisections<max_bisections && boundary_size<1){// && solution_size<1){
				/* ################################################
				CHECK VERIFICATION OF KINEMATICS
				################################################ */
				kinematics_verified = 0;
				// Contract
				kinematics_contractor.contract(kinematic_list.back());

				// Make sure not empty (empty means no kinematic solution)
				if (kinematic_list.back().is_empty()){
					if (verbose > 0) std::cout << "kinematics_box empty" << std::endl;
					kinematics_verified = -2; // treated differently from following constraints
				} else {
					// Check if phi and psi were modified by simplification (TODO: prevent this in the contractions)
					psipsi_simp[0] = kinematic_list.back()[system_kinematics_psi];
					psipsi_simp[1] = kinematic_list.back()[system_kinematics_phi];
					if (psipsi_simp==psiphi_)
						kinematics_verified = kinematics_existence(system_kinematics, kinematic_list.back(), verbose);
					else{
						if (verbose > 0) std::cout << "kinematics_box simplified angles" << std::endl;
						kinematics_verified = 0;
					}
				}

				/* ################################################
				CHECK VERIFICATION OF CONSTRAINTS
				################################################ */
				shoulder_axial_rotation_verified = 0;
				shoulder_swing_verified = 0;
				hand_swing_verified = 0;

				// SHOULDER AXIAL ROTATION //
				if (verify_shoulder_axial && kinematics_verified >= 0){
					if (verbose > 0) std::cout << "SHOULDER AXIAL ROTATION" << std::endl;
					shoulder_axial_rotation_verified = verify_shoulder_axial_rotation(system_shoulder_axial,
						shoulder_axial_contractor, system_kinematics, kinematic_list.back(), psiphi_[0], verbose);
				} else {
					// Default value
					shoulder_axial_rotation_verified = 1;
				}

				// SHOULDER SWING //
				if (verify_shoulder_swing && kinematics_verified >= 0 && shoulder_axial_rotation_verified >= 0) {
					// SHOULDER SWING //
					if (verbose > 0) std::cout << "SHOULDER SWING" << std::endl;
					shoulder_swing_verified = verify_swing(shoulderthetaswing, shoulderphiswing,
							system_shoulder_swing, shoulder_swing_contractor,
							system_kinematics, kinematic_list.back(), verbose);
				} else {
					// Default value
					shoulder_swing_verified = 1;
				}

				// HAND SWING //
				if (verify_hand_swing && kinematics_verified >= 0 && shoulder_axial_rotation_verified >= 0 && shoulder_swing_verified >= 0) {
					// HAND SWING //
					if (verbose > 0) std::cout << "HAND SWING" << std::endl;
					hand_swing_verified = verify_swing(wristthetaswing, wristphiswing,
							system_hand_swing, hand_swing_contractor,
							system_kinematics, kinematic_list.back(), verbose);
				} else {
					// Default value
					hand_swing_verified = 1;
				}

				// Determine classification
				if (kinematics_verified == -2)
					classification = -2;
				else if (kinematics_verified == 1 &&
						shoulder_axial_rotation_verified == 1 &&
						shoulder_swing_verified ==1 &&
						hand_swing_verified == 1)
					classification = 1;
				else if (kinematics_verified == -1 ||
						shoulder_axial_rotation_verified == -1 ||
						shoulder_swing_verified == -1 ||
						hand_swing_verified == -1)
					classification = -1;
				else
					classification = 0;

				// BISECTION
				if (classification == -2){
					// There does not exist a solution for the kinematics (does not count towards final classification)
					if (verbose > 0) std::cout << "NO SOLUTION - kinematics empty" << std::endl;
					kinematic_list.pop_back();
				}else if (classification == -1){
					// There does not exist a solution for all of the constraints (does not count towards final classification)
					if (verbose > 0) std::cout << "NO SOLUTION - kinematics" << std::endl;
					num_nosolutions++;
					kinematic_list.pop_back();
				}else if (classification == 1){
					if (verbose > 0) std::cout << "SOLUTION - kinematics" << std::endl;
					solution_size++;
					kinematic_list.pop_back();
				}else if (classification == 0){
					// bisect kinematics_box along rotation matrix elements
					kinematics_box_bisection = kinematic_list.back();
					// Variables not to be bisected.
					for (int var_i=0; var_i<do_not_bisect_list_indices.size(); var_i++)
						kinematics_box_bisection[do_not_bisect_list_indices[var_i]] = 0;

					if (kinematics_box_bisection.max_diam() > 0.01){
						bisect_index = max_diam_index(kinematics_box_bisection);
						std::pair<ibex::IntervalVector,ibex::IntervalVector> bisected = bisect_at(kinematic_list.back(), bisect_index);
						kinematic_list.pop_back();
						// append bisected to list
						kinematic_list.push_back(bisected.first);
						kinematic_list.push_back(bisected.second);
						num_bisections++;
						if (verbose > 1) std::cout << "BISECT - kinematics: " << system_kinematics.f_ctrs.args()[bisect_index] << '\t' << kinematics_box_bisection[bisect_index] << std::endl;
					} else {
						if (verbose > 0) std::cout << "BOUNDARY - kinematics" << std::endl;
						boundary_size++;
						kinematic_list.pop_back();
					}
				}
			}

			// Add any remaining kinematic boxes to boundary list
			if (verbose >= 0) std::cout << "Number of solutions: " << solution_size << std::endl;
			if (verbose >= 0) std::cout << "Number of boundaries: " << boundary_size << std::endl;
			if (verbose >= 0) std::cout << "Number of unclassified: " << kinematic_list.size() << std::endl;
			if (verbose >= 0) std::cout << "Number of no solution: " << num_nosolutions << std::endl;

			/* ################################################
			DETERMINE CLASSIFICATION
			################################################ */
			// Determine box classification
			if (solution_size >= 1 && boundary_size == 0 && kinematic_list.size() == 0 && num_nosolutions == 0) // TODO: SAFE - getting solution_size>=1 only may require ensuring contractors do not contract constant variables
				classification = 1;
			else if(solution_size == 0 && boundary_size == 0 && kinematic_list.size() == 0 && num_nosolutions >= 0)
				classification = -1; // OUTSIDE
			else
				classification = 0; // BOUNDARY

			/* ################################################
			APPLY BISECTION
			################################################ */
#pragma omp critical /* Threads update by turns */
			{
				if (verbose >= 0) std::cout << "thread: " << thread_id << " psiphi_list.size(): " << psiphi_list.size() << std::endl;
				if (verbose >= 0) std::cout << "psiphi_ " << psiphi_ << std::endl;
//				vibes::drawBox(psiphi_[0].lb(), psiphi_[0].ub(), psiphi_[1].lb(), psiphi_[1].ub(), "k");
				human_arm_redundancy_modelling::RedundancyBox box;
				box.elbow_swivel_lb = psiphi_[0].lb();
				box.elbow_swivel_ub = psiphi_[0].ub();
				box.hand_twist_lb = psiphi_[1].lb();
				box.hand_twist_ub = psiphi_[1].ub();
				box.classification = 100;
				redundancy_box_pub.publish(box);
				ros::spinOnce();
				loop_rate.sleep();
				if (classification == 1){
					if (verbose >= 0) std::cout << "SOLUTION - psiphi" << std::endl;
	//				vibes::drawBox(psiphi_[0].lb(), psiphi_[0].ub(), psiphi_[1].lb(), psiphi_[1].ub(), "b[b]");
					human_arm_redundancy_modelling::RedundancyBox box;
					box.elbow_swivel_lb = psiphi_[0].lb();
					box.elbow_swivel_ub = psiphi_[0].ub();
					box.hand_twist_lb = psiphi_[1].lb();
					box.hand_twist_ub = psiphi_[1].ub();
					box.classification = 1;
					redundancy_box_pub.publish(box);
					ros::spinOnce();
					loop_rate.sleep();
				}else if (classification == -1){
					if (verbose >= 0) std::cout << "NO SOLUTION - psiphi" << std::endl;
	//				vibes::drawBox(psiphi_[0].lb(), psiphi_[0].ub(), psiphi_[1].lb(), psiphi_[1].ub(), "r[r]");
					human_arm_redundancy_modelling::RedundancyBox box;
					box.elbow_swivel_lb = psiphi_[0].lb();
					box.elbow_swivel_ub = psiphi_[0].ub();
					box.hand_twist_lb = psiphi_[1].lb();
					box.hand_twist_ub = psiphi_[1].ub();
					box.classification = -1;
					redundancy_box_pub.publish(box);
					ros::spinOnce();
					loop_rate.sleep();
				}else{
					// bisect psiphi_
					try {
						std::pair<ibex::IntervalVector,ibex::IntervalVector> bisected = psiphi_bisector.bisect(psiphi_);
						// append bisected to list
						psiphi_list.push_back(bisected.first);
						psiphi_list.push_back(bisected.second);
						if (verbose >= 0) std::cout << "BISECT - psiphi" << std::endl;
					} catch (...) {
						if (verbose >= 0) std::cout << "BOUNDARY - psiphi" << std::endl;
	//					vibes::drawBox(psiphi_[0].lb(), psiphi_[0].ub(), psiphi_[1].lb(), psiphi_[1].ub(), "y[y]");
						human_arm_redundancy_modelling::RedundancyBox box;
						box.elbow_swivel_lb = psiphi_[0].lb();
						box.elbow_swivel_ub = psiphi_[0].ub();
						box.hand_twist_lb = psiphi_[1].lb();
						box.hand_twist_ub = psiphi_[1].ub();
						box.classification = 0;
						redundancy_box_pub.publish(box);
						ros::spinOnce();
						loop_rate.sleep();
					}
				}
			}
		} else {
			// Check if psiphi_list.size()==0 for all threads
			if (thread_id==0)
				repeat = false;
			else
				sleep(1);
		}
	}
}
	return 0;
}


int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol){
	const char *cstr = symbol.c_str();
	for (int var_i=0; var_i<variable_list.size(); var_i++){
//		std::cout << variable_list[var_i].name << '\t' << symbol << std::endl;
		if (strcmp(variable_list[var_i].name, cstr) == 0){
//			std::cout << "Found index: " << var_i << std::endl;
			return var_i;
		}
	}
	return -1;
}

/*
 * Update box 2 with the values from box1 by matching the variables.
 */
void copy_vars(const ibex::Array<const ibex::ExprSymbol> &variable_list1, ibex::IntervalVector &box1,
		const ibex::Array<const ibex::ExprSymbol> &variable_list2, ibex::IntervalVector &box2){

	for (int var_i=0; var_i<variable_list1.size(); var_i++){
		for (int var_j=0; var_j<variable_list2.size(); var_j++){
//			std::cout << variable_list1[var_i].name << '\t' << variable_list2[var_j].name << std::endl;
			if (strcmp(variable_list1[var_i].name, variable_list2[var_j].name) == 0){
//				std::cout << "Match found.. updating box2" << std::endl;
				box2[var_j] = box1[var_i];
			}
		}
	}
}

/*
 * Bisect interval vector at index.
 */
std::pair<ibex::IntervalVector,ibex::IntervalVector> bisect_at(ibex::IntervalVector &Vector, int index){

	ibex::IntervalVector Vector1 = Vector;
	Vector1[index] = ibex::Interval(Vector[index].lb(),Vector[index].mid());
	ibex::IntervalVector Vector2 = Vector;
	Vector2[index] = ibex::Interval(Vector[index].mid(),Vector[index].ub());
	std::pair<ibex::IntervalVector,ibex::IntervalVector> pair = std::make_pair(Vector1,Vector2);
	return pair;
}

int max_diam_index(ibex::IntervalVector &Vector){
	ibex::Vector Diams = Vector.diam();
	int max_index = 0;
	double max_diam = Diams[max_index];
	for (int i=1; i<Diams.size(); i++){
		if (Diams[i] > Diams[max_index]){
			max_diam = Diams[i];
			max_index = i;
		}
	}
	return max_index;
}

int verify_shoulder_axial_rotation(ibex::System &axial_system, ibex::CtcCompo &axial_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, ibex::Interval &psi, int verbose){

	ibex::IntervalVector shoulder_axial_box = axial_system.box;
	copy_vars(kinematics_system.f_ctrs.args(), kinematics_box,
			axial_system.f_ctrs.args(), shoulder_axial_box);

	// Evaluate alpha and beta without constraints on limits for comparison with system box after contraction
	ibex::Interval rx4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "rx4")];
	ibex::Interval ry4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "ry4")];
	ibex::Interval rz4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "rz4")];
	ibex::Interval beta = atan2(rz4, sqrt(sqr(rx4)+sqr(ry4)));
	ibex::Interval alpha = atan2(rx4, -ry4);

	// Contract
	axial_contractor.contract(shoulder_axial_box);

	// Check if alpha and beta are not empty
	ibex::Interval alpha_ = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "alpha")];
	ibex::Interval beta_ = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "beta")];

	if (alpha_.is_empty() || beta_.is_empty()){
		if (verbose > 0) std::cout << "shoulder_axial not valid: alpha/beta empty" << std::endl;
		return -1;
	} else if (alpha.ub() < -1.7453 || alpha.lb() > 2.2689 || beta.ub() < -1.3963 || beta.lb() > 1.3963){
		// alpha, beta limits dissatisfied
		if (verbose > 0) std::cout << "shoulder_axial not valid: alpha/beta range" << std::endl;
		return -1;
	} else {
		// Check constraint satisfaction: psi < EXT; psi > INT;
		ibex::Interval EXT = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "EXT")];
		ibex::Interval INT = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "INT")];
		if (verbose > 0) std::cout << "axial_rotation " << INT << '\t' << psi << '\t' << EXT << std::endl;
		if (psi.ub() < EXT.lb() && psi.lb() > INT.ub() &&
				alpha.lb() >= -1.7453 && alpha.ub() <= 2.2689 && beta.lb() >= -1.3963 && beta.ub() <= 1.3963){
			// alpha, beta limits and INT, EXT limits satisfied
			if (verbose > 0) std::cout << "shoulder_axial valid" << std::endl;
			return 1;
		} else if (psi.lb() > EXT.ub() || psi.ub() < INT.lb()){
			if (verbose > 0) std::cout << "shoulder_axial not valid" << std::endl;
			return -1;
		} else{
			// alpha, beta limits or INT, EXT limits partially satisfied
			if (verbose > 0) std::cout << "shoulder_axial partially valid" << std::endl;
			return 0;
		}
	}
}

int verify_swing(ibex::IntervalVector &thetaswing, ibex::IntervalVector &phiswing,
		ibex::System &swing_system, ibex::CtcCompo &swing_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, int verbose){
	int num_inside_classifications = 0;
	int num_outside_classifications = 0;
	int shoulder_swing_pair_verified = 0;
	bool has_boundary = false;
	ibex::IntervalVector swing_box_orig = swing_system.box;
	copy_vars(kinematics_system.f_ctrs.args(), kinematics_box, swing_system.f_ctrs.args(), swing_box_orig);
	for (int pair=0; pair<thetaswing.size()-1; pair++){

		// Loop over t in [0,1] until swing constraint is satisfied for all t or all t are unsatisfied
		std::vector<ibex::Interval> t_list;
		t_list.push_back(ibex::Interval(0,1));
		int num_pair_inside_classifications = 0;
		int num_pair_outside_classifications = 0;
		ibex::IntervalVector swing_box = swing_box_orig;
		ibex::Interval t,t_orig,K,G;
		while(t_list.size()>0 && !has_boundary){
			// Pop t
			t = t_list.back();
			t_list.pop_back();

			// Copy t to check solution existence after filtering
			t_orig = t;

			// Update box
			swing_box = swing_box_orig;

			// Update theta and gamma values for quaternion pair
			swing_box[find_var_index(swing_system.f_ctrs.args(), "theta1")] = thetaswing[pair];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "gamma1")] = phiswing[pair];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "theta2")] = thetaswing[pair+1];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "gamma2")] = phiswing[pair+1];

			// Set t value
			swing_box[find_var_index(swing_system.f_ctrs.args(), "t")] = t;

			// Contract
			swing_contractor.contract(swing_box);

			// Get simplified t value
			t = swing_box[find_var_index(swing_system.f_ctrs.args(), "t")];
			if (verbose > 1) std::cout << "t_orig " << t_orig << " t " << t << '\t' << std::endl;

			// Check constraint G <= K <= 1
			K = swing_box[find_var_index(swing_system.f_ctrs.args(), "K")];
			G = swing_box[find_var_index(swing_system.f_ctrs.args(), "G")];

			if (verbose > 1) std::cout << "swing: " << pair << '\t' << G << '\t' << K << '\t' << 1 << std::endl;

			// Handle empty cases
			if (!K.is_empty() && !G.is_empty()){
				// Determine classification
				if (K.ub() <= 1 && K.lb() >= G.ub()){
					if (verbose > 1) std::cout << "swing pair valid" << std::endl;
					num_pair_inside_classifications++;
				} else if (K.lb() > 1 || K.ub() < G.lb()){
					num_pair_outside_classifications++;
					if (verbose > 1) std::cout << "swing pair not valid: " << std::endl;
				} else{
					if (t.diam()>0.0001){
						t_list.push_back(ibex::Interval(t.lb(),t.mid()));
						t_list.push_back(ibex::Interval(t.mid(),t.ub()));
						if (verbose > 1) std::cout << "BISECTION" << std::endl;
					} else{
						if (verbose > 1) std::cout << "BOUNDARY" << std::endl;
						has_boundary = true;
					}
				}
			}
		}

		if (verbose > 1) std::cout << "num_pair_inside_classifications "  << num_pair_inside_classifications << std::endl;
		if (verbose > 1) std::cout << "num_pair_outside_classifications "  << num_pair_outside_classifications << std::endl;

		if (num_pair_inside_classifications>0 && num_pair_outside_classifications==0 && !has_boundary){
			// swing pair constraint is valid for all t.
			num_inside_classifications++;
			if (verbose > 1) std::cout << "swing pair valid "  << std::endl;
		}
		else if (num_pair_outside_classifications>0  && num_pair_inside_classifications==0 && !has_boundary){
			// swing pair constraint is invalid for some t.
			num_outside_classifications++;
			if (verbose > 1) std::cout << "swing pair not valid " << std::endl;
		} else if (has_boundary || (num_pair_inside_classifications>0 && num_pair_outside_classifications>0)) {
			// if both inside and outside classifications are found then there must exist a boundary (there seems to be a rare
			// case where the bisection does not detect the boundary but does find inside and outside classifications)
			has_boundary = true;
			if (verbose > 1) std::cout << "swing pair partially valid "  << std::endl;
			break;
		} else {
			if (verbose > 1) std::cout << "swing pair empty" << std::endl;
		}

	}

	if (verbose > 0) std::cout << "num_inside_classifications "  << num_inside_classifications << std::endl;
	if (verbose > 0) std::cout << "num_outside_classifications "  << num_outside_classifications << std::endl;
	// Determine swing classification
	if (num_outside_classifications>0 && !has_boundary){
		if (verbose > 0) std::cout << "swing not valid "  << std::endl;
		return -1; // outside
	} else if (num_inside_classifications>0 && !has_boundary){
		if (verbose > 0) std::cout << "swing valid "  << std::endl;
		return 1; // inside
	} else{
		if (verbose > 0) std::cout << "swing partially valid "  << std::endl;
		return 0; // boundary
	}
}

int kinematics_existence(ibex::System &system_kinematics, ibex::IntervalVector &kinematics_box, int verbose){
	// Existence function
	ibex::Interval d3=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "d3")];
	ibex::Interval d5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "d5")];
	ibex::Interval rx5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"rx5")];
	ibex::Interval ry5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"ry5")];
	ibex::Interval rz5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"rz5")];

	ibex::Interval wrist_dist = sqrt(sqr(rx5) + sqr(ry5) + sqr(rz5));
	if (verbose > 1) std::cout << "wrist_dist: " << wrist_dist << std::endl;
	if (verbose > 1) std::cout << "d3 + d5: " << d3 + d5 << std::endl;
	if (wrist_dist.ub() < (d3 + d5).lb()){
		return 1;
	} else if (wrist_dist.lb() > (d3 + d5).ub()){
		return -1;
	} else {
		return 0;
	}

}











