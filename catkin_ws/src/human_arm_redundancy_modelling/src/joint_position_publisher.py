#!/usr/bin/env python

import numpy as np
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header

if __name__ == '__main__':
    pub = rospy.Publisher('joint_states', JointState, queue_size=10)
    rospy.init_node('joint_position_publisher')
    rate = rospy.Rate(10) # 10hz
    joint_state_msg = JointState()
    joint_state_msg.name = ['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7']
    joint_state_msg.position = [-1.57,3.14,0.5,0,0,0,0]

    while not rospy.is_shutdown():
      joint_state_msg.header.stamp = rospy.Time.now()
      pub.publish(joint_state_msg)
      rate.sleep()
