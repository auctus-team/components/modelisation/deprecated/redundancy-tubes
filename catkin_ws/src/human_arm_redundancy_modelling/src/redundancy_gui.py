#!/usr/bin/env python

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.animation import FuncAnimation
import rospy
import os

from human_arm_redundancy_modelling.msg import Redundancy, RedundancyBox
from math import pi

from matplotlib.widgets import Slider, Button, RadioButtons
from geometry_msgs.msg import PoseStamped
import pylab as pl

elbow_swivel_list = []
hand_twist_list = []
redundancy_box_list = []
redundancy_box_counter = 0
redundancy_box_last_plotted = 0
elbow_swivel_box = [-pi/2,3*pi/2]
hand_twist_box = [-pi,pi]
redundancy_box = [[[0],[0]],0]
twist_point_x = 15
twist_point_y = -20
twist_point_z = 20

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.5)
plt.axis([-pi/2-1,3*pi/2+1,-pi-1,pi+1])
ax.set_xlabel('Elbow swivel angle (rads)')
ax.set_ylabel('Hand twist angle (rads)')
ax.set_aspect('equal')

hplotangle, = ax.plot([], [], marker='o', linestyle='none', markersize=3, color="green")

redundancy_box = [[[elbow_swivel_box[0],hand_twist_box[0]], [elbow_swivel_box[0],hand_twist_box[1]], [elbow_swivel_box[1],hand_twist_box[1]], [elbow_swivel_box[1],hand_twist_box[0]]],100]
redundancy_box_list.append(redundancy_box)
hplotbox = plt.Polygon(redundancy_box[0],closed=True, fill=None, edgecolor='black')
ax.add_patch(hplotbox)

def callback_redundancies(data):
    # Get plot data
    elbow_swivel_list.append(data.elbow_swivel)
    hand_twist_list.append(data.hand_twist)

def callback_redundancy_box(data):
    # Get plot data
    global redundancy_box_list, redundancy_box_counter
    redundancy_box = [[[data.elbow_swivel_lb,data.hand_twist_lb], [data.elbow_swivel_lb,data.hand_twist_ub], [data.elbow_swivel_ub,data.hand_twist_ub], [data.elbow_swivel_ub,data.hand_twist_lb]], data.classification]
    redundancy_box_list.append(redundancy_box)
    redundancy_box_counter = redundancy_box_counter + 1

def updatePlot(num):
    if rospy.is_shutdown():
        plt.close()
        return
    global redundancy_box_list, redundancy_box_counter, redundancy_box_last_plotted
    hplotangle.set_xdata(elbow_swivel_list)
    hplotangle.set_ydata(hand_twist_list)
    
    while redundancy_box_last_plotted < redundancy_box_counter:
        redundancy_box_last_plotted = redundancy_box_last_plotted + 1
        redundancy_box = redundancy_box_list[redundancy_box_last_plotted]
        #print redundancy_box
        classification = redundancy_box[1]
        if classification == -1:
            hplotbox = plt.Polygon(redundancy_box[0],closed=True, facecolor='red', edgecolor='red', linewidth=0.5)
            ax.add_patch(hplotbox)
        elif classification == 1:
            hplotbox = plt.Polygon(redundancy_box[0],closed=True, facecolor='blue', edgecolor='blue', linewidth=0.5)
            ax.add_patch(hplotbox)
        elif classification == 0:
            hplotbox = plt.Polygon(redundancy_box[0],closed=True, facecolor='yellow', edgecolor='yellow', linewidth=0.5)
            ax.add_patch(hplotbox)
        else :
            hplotbox = plt.Polygon(redundancy_box[0],closed=True, facecolor='None', edgecolor='black', linewidth=0.5)
            ax.add_patch(hplotbox)

    # Force publishing
    update(0)

ani = FuncAnimation(fig, updatePlot, interval=1)

axcolor = 'lightgoldenrodyellow'
axdisbablecolor = 'lightgrey'
ax_hand_twist = plt.axes([0.25, 0.35, 0.65, 0.03], facecolor=axcolor)
ax_elbow_swivel = plt.axes([0.25, 0.3, 0.65, 0.03], facecolor=axcolor)
helbow_swivel = Slider(ax_elbow_swivel, 'elbow_swivel', -pi/2, 3*pi/2, valinit=2)
hhand_twist = Slider(ax_hand_twist, 'hand_twist', -pi, pi, valinit=0.0)

ax_twist_point_x = plt.axes([0.25, 0.25, 0.65, 0.03], facecolor=axcolor)
ax_twist_point_y = plt.axes([0.25, 0.2, 0.65, 0.03], facecolor=axcolor)
ax_twist_point_z = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
htwist_point_x = Slider(ax_twist_point_x, 'twist_point_x', -100, 100, valinit=twist_point_x, color='red')
htwist_point_y = Slider(ax_twist_point_y, 'twist_point_y', -100, 100, valinit=twist_point_y, color='red')
htwist_point_z = Slider(ax_twist_point_z, 'twist_point_z', -100, 100, valinit=twist_point_z, color='red')

def update(val):
    # publish values to redundancies
    global path_pub, redundancies_pub, rate
    pub_Redundancy = Redundancy()
    pub_Redundancy.elbow_swivel = helbow_swivel.val
    pub_Redundancy.hand_twist = hhand_twist.val
    pub_Redundancy.twist_point.x = htwist_point_x.val
    pub_Redundancy.twist_point.y = htwist_point_y.val
    pub_Redundancy.twist_point.z = htwist_point_z.val
    pub_Redundancy.twist_axis.x = 0
    pub_Redundancy.twist_axis.y = 0
    pub_Redundancy.twist_axis.z = 1
    pub_Redundancy.twist_refaxis.x = 1
    pub_Redundancy.twist_refaxis.y = 0
    pub_Redundancy.twist_refaxis.z = 0
    redundancies_pub.publish(pub_Redundancy)

    p = PoseStamped()
    p.header.stamp = rospy.Time.now()
    p.header.frame_id = "base_link"
    p.pose.position.x = 0.05
    p.pose.position.y = -0.20
    p.pose.position.z = 0.20
    # Make sure the quaternion is valid and normalized
    p.pose.orientation.x = 0.5
    p.pose.orientation.x = 0.5
    p.pose.orientation.x = 0.5
    p.pose.orientation.w = 0.5
    path_pub.publish(p)
    rate_user.sleep()

helbow_swivel.on_changed(update)
hhand_twist.on_changed(update)
htwist_point_x.on_changed(update)
htwist_point_y.on_changed(update)
htwist_point_z.on_changed(update)

resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')

def reset(event):
    global elbow_swivel_list, hand_twist_list
    helbow_swivel.reset()
    hhand_twist.reset()
    htwist_point_x.reset()
    htwist_point_y.reset()
    htwist_point_z.reset()
    elbow_swivel_list = []
    hand_twist_list = []
button.on_clicked(reset)

workspaceax = plt.axes([0.2, 0.025, 0.5, 0.04])
workspacebutton = Button(workspaceax, 'Compute Redundant Workspace at Pose', color=axcolor, hovercolor='0.975')

def get_redundant_workspace(event):
    htwist_point_x.set_active(False)
    htwist_point_y.set_active(False)
    htwist_point_z.set_active(False)
    workspacebutton.set_active(False)
    #htwist_point_x.color = 'red'
    #htwist_point_y.color = 'red'
    #htwist_point_z.color = 'red'
    #workspacebutton.color = 'red'
    cmd = 'roslaunch human_arm_redundancy_modelling redundant_workspace_solver.launch &'
    os.system(cmd) # returns the exit status
workspacebutton.on_clicked(get_redundant_workspace)

if __name__ == '__main__':
    global path_pub, redundancies_pub, rate
    rospy.init_node('redundancy_gui', anonymous=True)
    rospy.Subscriber("redundancies", Redundancy, callback_redundancies)
    rospy.Subscriber("redundancy_box", RedundancyBox, callback_redundancy_box)
    redundancies_pub = rospy.Publisher("redundancies", Redundancy, queue_size=10)
    path_pub = rospy.Publisher('pose', PoseStamped, queue_size=1)
    rate_user = rospy.Rate(5) # 1hz
    plt.show()
