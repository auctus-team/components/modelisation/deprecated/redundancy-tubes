#!/usr/bin/env bash

stage_welcome()
{
  prompt_str $1 $2 "This program will install the kcadl ros packages on your computer. Press any key to continue, or Ctrl^C to exit"
  sudo updatedb
}

stage_prerequisite()
{
  print_stage $1 $2 "Installing required packages..."
  sudo apt install -y python python2.7 flex bison gcc g++ make pkg-config zlib1g-dev qt5-default libqt5svg5-dev cmake git python-pip python-matplotlib python-numpy python-scipy
  pip uninstall matplotlib && pip install matplotlib==2.0.2 --user
  sudo apt install -y python3-pip python-numpy python-scipy python-matplotlib
  pip install vtk numpy-stl
}

stage_install_ros()
{
  print_stage $1 $2 "Installing ROS..."
  read -p "Select ROS version: (1) melodic, (2) kinetic " distro
  if test $distro == "1"; then 
    DISTRO="melodic"
  elif test $distro == "2"; then 
    DISTRO="kinetic"
  fi
  echo "Using distro: $DISTRO"
  sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
  sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
  sudo apt update
  sudo apt install ros-$DISTRO-desktop-full
}

stage_ros_configure()
{
  print_stage $1 $2 "Configuring ROS..."
  sudo rosdep init
  rosdep update
  source /opt/ros/$DISTRO/setup.bash
  sudo apt update
}

stage_ros_scripts()
{
  print_stage $1 $2 "Installing ROS add-ons..."
  sudo apt install python-rosinstall python-rosinstall-generator python-wstool python-catkin-pkg python-catkin-tools ros-$DISTRO-catkin ros-$DISTRO-moveit libqwt-dev ros-$DISTRO-rqt libomp_dev
  sudo apt install ros-$DISTRO-rqt-multiplot
}

stage_build_ros_package()
{	
  print_stage $1 $2 "Building ROS packages..."
  catkin build
  source ./devel/setup.bash
}

print_bold()
{
    echo -e "$(tput bold)$1$(tput sgr0)"
}

print_stage()
{
  print_bold "\n[$1/$2] $3"
}

prompt_str()
{
    read -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

prompt_str_pass()
{
    read -s -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

echo "################################"
echo "#   ROS PACKAGES INSTALLER     #"
echo "################################"
echo ""

STAGE_COUNT=4

PROJECT_NAME="ROS packages"
DISTRO="melodic"

stage_welcome
stage_prerequisite 1 $STAGE_COUNT
#stage_install_ros 2 $STAGE_COUNT
stage_ros_configure 2 $STAGE_COUNT
stage_ros_scripts 3 $STAGE_COUNT
stage_build_ros_package 4 $STAGE_COUNT
echo -e "\nInstallation finished !\n"



