function plot_redundancy_tube()

close all;

% Import an STL mesh, returning a PATCH-compatible face-vertex structure
% inside_mesh
[v_, f, n, c, stltitle] = stlread('../inside_mesh.stl');
v = v_;
v(:,2) = v_(:,3) / 10;
v(:,3) = v_(:,2);
patch('Faces',f,'Vertices',v,'FaceVertexCData',c,'FaceColor','blue','EdgeColor','none','FaceAlpha',1);
% % outside_mesh
% [v_, f, n, c, stltitle] = stlread('../outside_mesh.stl');
% v = v_;
% v(:,2) = v_(:,3) / 10;
% v(:,3) = v_(:,2);
% patch('Faces',f,'Vertices',v,'FaceVertexCData',c,'FaceColor','red','EdgeColor','red');
% boundary_mesh
[v_, f, n, c, stltitle] = stlread('../boundary_mesh.stl');
v = v_;
v(:,2) = v_(:,3) / 10;
v(:,3) = v_(:,2);
patch('Faces',f,'Vertices',v,'FaceVertexCData',c,'FaceColor','yellow','EdgeColor','none','FaceAlpha',1);

% Motion in tube
xyz=[1.6,0,0;
1.8,0.2,-0.4;
2.0,0.4,-0.2;
2.01,0.6,-0.1;
1.9,0.8,0.1;
1.8,1,0.2]';
hold on;
% plot3(xyz(1,:),xyz(2,:),xyz(3,:),'ro','LineWidth',2);
fnplt(cscvn(xyz(:,[1:end])),'r',2);

xyz=[1.6,0,-0.6;
1.85,0.2,-0.5;
2.01,0.4,-0.4;
1.97,0.6,-0.3;
1.9,0.8,0.0;
1.5,1,-0.1]';
hold on;
% plot3(xyz(1,:),xyz(2,:),xyz(3,:),'mo','LineWidth',2);
fnplt(cscvn(xyz(:,[1:end])),'m',2);

% Adjust plot
axis([1.2272    2.2335         0    1.0000   -1.2763    0.6872]);
pbaspect([1 1 1])
daspect([1 0.2 1])
xlabel('Swivel angle $\psi$ (rad)', 'interpreter', 'latex');
zlabel('Twist angle $\phi$ (rad)', 'interpreter', 'latex');
ylabel('Path $t$', 'interpreter', 'latex');
xticks([1, 1.5, 2, 2.5, 3])
zticks([-1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5])
yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
view(35,15);
