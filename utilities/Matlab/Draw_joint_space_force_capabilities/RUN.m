% Add src files to path
addpath('../src/');

close all;
clear;
figure;
hold on;

Fiso_JS = load('/tmp/joint_space_braking_force.txt');

% Force capabilities
FC = Fiso_JS(:,end);

% Coloring
nb_colors = 100;
colormap(hot(nb_colors));

% scatter(Fiso_JS(:,1),Fiso_JS(:,3),[],FC);%,'filled');
scatter3(Fiso_JS(:,1),Fiso_JS(:,3),Fiso_JS(:,5),[],FC);%,'filled');
hcb = colorbar;

xlabel('$q_1$ (rad)','interpreter','latex');
ylabel('$q_4$ (rad)','interpreter','latex');
zlabel('$q_5$ (rad)','interpreter','latex');
axis equal;


colorTitleHandle = get(hcb,'Title');
titleString = '$f_b$ (N)';
set(colorTitleHandle ,'String',titleString,'interpreter','latex');

% for index=1:length(Fiso_JS)     
%     % get force enclosure
%     Fiso_i = Fiso_JS(index,:);
%     f_box = create_box(2,Fiso_i([5,9]),Fiso_i([6,10]));
% %     disp(f_box);
%     color_index = floor(nb_colors * (Fiso_i(end) - FC_min) / (FC_max - FC_min))+1;
%     plotbox(f_box,myColorMap(color_index,:),myColorMap(color_index,:),0.5); 
% end
