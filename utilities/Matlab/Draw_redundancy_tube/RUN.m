% Add src files to path
addpath('../src/');

clear;
clc;
close all;

boxes = importdata('/tmp/workspace.txt');
dim = 2;

figure;
hold on;
inside_boxes = {};
outside_boxes = {};
boundary_boxes = {};
for ibox=1:size(boxes,1)
    % get next box
    box = boxes(ibox,:);
    Clb = [box(1),box(3),box(5)];
    Cub = [box(2),box(4),box(6)];
    classification = box(7);
    Cbox = create_box(dim,Clb,Cub);
    if classification == 1
        inside_boxes{end+1} = Cbox;
    elseif classification == -1
        outside_boxes{end+1} = Cbox;
    elseif classification == 0
        boundary_boxes{end+1} = Cbox;
    end       
end

for box=1:length(boundary_boxes)
    plotbox(boundary_boxes{box},'none','y',0.6);
end
for box=1:length(inside_boxes)
    plotbox(inside_boxes{box},'none','b',0.6);
end
for box=1:length(outside_boxes)
    plotbox(outside_boxes{box},'none','r',0.6);
end

if dim==2
    axis equal;
    xlabel('Swing angle $\psi$ (rad)', 'interpreter', 'latex');
    ylabel('Twist angle $\phi$ (rad)', 'interpreter', 'latex','Rotation',90);
elseif dim==3
    pbaspect([1 1 1])
    daspect([1 1 0.1])
    xlabel('Swing angle $\psi$ (rad)', 'interpreter', 'latex');
    ylabel('Twist angle $\phi$ (rad)', 'interpreter', 'latex');
    zlabel('Path $t$', 'interpreter', 'latex');
end
