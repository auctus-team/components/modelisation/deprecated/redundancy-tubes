% Add src files to path
addpath('../src/');

clear;
clc;
close all;

phi_theta = importdata("../../../examples/redundancy_tube/data/shoulder_swing.txt", '\t');
figure;
plot_spherical_polygon(phi_theta);
title('shoulder swing');

phi_theta = importdata("../../../examples/redundancy_tube/data/wrist_swing.txt", '\t');
figure;
plot_spherical_polygon(phi_theta);
title('wrist swing');