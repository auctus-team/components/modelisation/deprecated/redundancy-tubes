% force_capabilities_twolinkmanip.m
err = infsup(-0.01,0.01);
err2 = infsup(-0.001,0.001);
% err = infsup(-0.0,0.0);
% err2 = infsup(-0.0,0.0);

qk = [1;-2] + err;
dqk = [2;1] + err;
ddqk = [-1.5;1] + err;
qlim = [infsup(0,3);infsup(-3,3)];
dqlim = [infsup(-3,3);infsup(-3,3)];
ddqlim = [infsup(-5,5);infsup(-5,5)];
dddqlim = [infsup(-500,500);infsup(-500,500)];
taulim = [infsup(-50,50);infsup(-50,50)];

m_1 = 1  + err;
m_2 = 1.5 + err;
l_1 = 0.4 + err2;
l_2 = 0.5 + err2;

t = infsup(0,0.01);
ddqk1 = ddqk + dddqlim * t;
dqk1 = dqk + ddqlim * t + 1/2 * dddqlim * t^2;
qk1 = qk + dqlim * t + 1/2 * ddqlim * t^2 + 1/6 * dddqlim * t^3;

q_1 = qk1(1);
q_2 = qk1(2);
dq_1 = dqk1(1);
dq_2 = dqk1(2);
g = 9.81;

J_qk1 = [-l_2 *sin(q_1) - l_2 *sin(q_1 + q_2), -l_2*sin(q_1 + q_2);
        l_1 *cos(q_1) + l_2 *cos(q_1 + q_2), l_2*cos(q_1 + q_2)];
    
M_qk1 = [l_2^2* m_2 + 2 *l_1* l_2 *m_2 *cos(q_2) + l_1^2*(m_1+m_2), l_2^2 *m_2 + l_1* l_2 *m_2 *cos(q_2);
    l_2^2 *m_2 + l_1 *l_2 *m_2 *cos(q_2) , l_2^2 *m_2];

c_qk1_dqk1 = [-m_2 *l_1* l_2 *sin(q_2)*dq_2^2 - 2 *m_2 *l_1 *l_2 *sin(q_2) *dq_1 *dq_2;
        m_2 *l_1 *l_2 *sin(q_2) *dq_1^2];
    
g_dk1 = [m_2 *l_2 *g *cos(q_1 + q_2) + (m_1 +m_2) *l_1 *g *cos(q_1);
        m_2 *l_2 *g *cos(q_1 + q_2)];
  
taudyn = M_qk1*ddqk1 + c_qk1_dqk1 + g_dk1;
taueff = infsup(inf(taulim) + sup(taudyn), sup(taulim) + inf(taudyn));

radius = tolerance_set_inscribed_sphere(J_qk1', taueff, [0,0]', true)
% radius = tolerance_set_inscribed_sphere(mid(J_qk1)', taueff, [0,0]', true)
title('')
xlabel('$f_x$','interpreter','latex','FontSize', 20);
ylabel('$f_y$','interpreter','latex','FontSize', 20);
alpha(0.3);