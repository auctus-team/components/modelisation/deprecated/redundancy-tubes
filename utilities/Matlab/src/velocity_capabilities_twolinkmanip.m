clear;
clc;
close all;

q = [infsup(0,1);infsup(-1,1)];
delta_q = diam(q);
count = 1000;
res_q = delta_q/count;
l_1 = 0.4 + infsup(-0.001,0.001);
l_2 = 0.5 + infsup(-0.001,0.001);
dq = [infsup(-3,3);infsup(-3,3)];

xc = [0,0];
plot = false;

% Branch and bound
box_list = cell(count*count,1);
vert_list = nan(count*count,2);
rad_list = nan(count*count,1);
listindex = 1;
k = [1,2,3,4,1]';
for q_1i=1:count
    for q_2i=1:count
        q_1 = infsup(delta_q(1)*(q_1i/count) + inf(q(1)) - res_q(1),...
            delta_q(1)*(q_1i/count) + inf(q(1)));
        q_2 = infsup(delta_q(2)*(q_2i/count) + inf(q(2)) - res_q(2),...
            delta_q(2)*(q_2i/count) + inf(q(2)));
        Jq = [-l_2 *sin(q_1) - l_2 *sin(q_1 + q_2), -l_2*sin(q_1 + q_2);
            l_1 *cos(q_1) + l_2 *cos(q_1 + q_2), l_2*cos(q_1 + q_2)];
        
        r = 100000;
        for i=1:length(dq)
            num = (rad(dq) - abs(pinv(mid(Jq))) * rad(Jq) * mag(dq));
            r = min(r,(num(i)) / (rad(dq(i))));
        end
        y = mid(dq) + r * infsup(-1,1)*ones(length(dq),1);
        box_y = create_box(size(y,1),inf(y),sup(y));
        b_vertices = mid(Jq) * box_y';
        
        % Compute convex hull
%         k = convhulln(b_vertices', {'QR0','Qt'});
%         k = boundary(b_vertices(1,:)', b_vertices(2,:)',0);
        k = [1,2,3,4,1]';

        % get maximum inscribed sphere
        radius = 100000;
        for index=1:size(k,1)-1
            % get facet points
            facet_points = b_vertices(:,[k(index),k(index+1)])'; 

            % determine hyperplane equation
            p1 = [facet_points(1,:),0];
            p2 = [facet_points(2,:),0];
            p3 = [facet_points(1,:),1];
            normal = cross(p1 - p2, p1 - p3);
            normal = normal / norm(normal);
            d = abs(p1(1)*normal(1) + p1(2)*normal(2) + p1(3)*normal(3));
            radius = min(radius, d);
        end      
        qbox = create_box(2,inf([q_1,q_2]),sup([q_1,q_2]));
        box_list{listindex} = qbox;
        vert_list(listindex,:) = mid([q_1,q_2])';
        rad_list(listindex) = radius;
        
%         if plot
%             k_b_vertices = boundary(b_vertices(1,:)', b_vertices(2,:)',0);
%             fill(b_vertices(1,k_b_vertices), b_vertices(2,k_b_vertices), 'b');
%             alpha(0.3);
%             hold on;
%             plot(b_vertices(1,:),b_vertices(2,:),'bo');        
%             axis equal;
%             xlabel('$\Delta{x}$','interpreter','latex','FontSize', 20);
%             ylabel('$\Delta{y}$','interpreter','latex','FontSize', 20);
%             zlabel('$\Delta{z}$','interpreter','latex','FontSize', 20);
%             hold on;
%             th = 0:pi/50:2*pi;
%             x = radius * cos(th) + xc(1);
%             y = radius * sin(th) + xc(2);
%             fill(x,y, 'g');  
%             alpha(0.3);
%         end   
        listindex = listindex+1;
    end
end

% Plotting
cVect = rad_list;
colormap(jet(100));
% hScat = scatter(vert_list(:,1), vert_list(:,2), 1, cVect, 'Filled');
% hcbar = colorbar;
for i=1:length(box_list)
    boxi = box_list{i};
    fill(boxi(1:4,1)',boxi(1:4,2)',cVect(i));
    hold on;
end
hcbar = colorbar;
    