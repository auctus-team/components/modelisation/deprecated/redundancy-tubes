% Plotting
close all;
clc;
clear;
% figure;

skin_color = [234,192,134]/255;
arm_rad = 1;

% Plot shoulder
[X,Y,Z] = sphere(20);
X = arm_rad*X;
Y = arm_rad*Y;
Z = arm_rad*Z;
surf(X,Y,Z, 'Edgecolor',skin_color, 'Facecolor',skin_color, 'FaceAlpha',0.5);
hold on;
plot3([0,5],[0,0],[0,0],'r');
plot3([0,0],[0,5],[0,0],'g');
plot3([0,0],[0,0],[0,5],'b');

% Load data from file
data = importdata("/tmp/kinematics.txt");
vdata = data.data;
vnames = strsplit(data.textdata{1});

for line=1:size(vdata,1)
    vdata_line = vdata(line,:);
    
    % Load variables
    for var=1:length(vnames)
        if var < length(vnames)
            lb = vdata(line,(2*(var-1)+1));
            ub = vdata(line,(2*(var-1)+2));  
            eval(strcat(vnames{var}, '= infsup(', num2str(lb), ',', num2str(ub), ');'));       
        else
            val = vdata(line,(2*(var-1)+1));  
            eval(strcat(vnames{var}, '= ', num2str(val), ';'));  
        end
    end
    
    % Plot elbow
    if classification==1
        r3_box = create_box(3, inf([rx3,ry3,rz3]), sup([rx3,ry3,rz3]));
        plotbox(r3_box, 'none','m','0.8');
        
        % Convex hull of r3_box and [0,0,0]
        verts = [r3_box;0,0,0];
        fv = convhulln(verts);
        trisurf(fv, verts(:,1),verts(:,2),verts(:,3),'FaceColor','m','FaceAlpha',0.6);

        % Plot wrist
        r5_box = create_box(3, inf([rx5,ry5,rz5]), sup([rx5,ry5,rz5]));
        plotbox(r5_box, 'none','g','0.8');
        
        % Convex hull of r3_box and [0,0,0]
        verts = [r5_box;r3_box];
        fv = convhulln(verts);
        trisurf(fv, verts(:,1),verts(:,2),verts(:,3),'FaceColor','g','FaceAlpha',0.6);

        % Plot hand
        r7_box = create_box(3, inf([rx7,ry7,rz7]), sup([rx7,ry7,rz7]));
        plotbox(r7_box, 'none','r','0.8');
        
        
    elseif classification==0
        r3_box = create_box(3, inf([rx3,ry3,rz3]), sup([rx3,ry3,rz3]));
        plotbox(r3_box, 'none','y','0.8');

        % Plot wrist
        r5_box = create_box(3, inf([rx5,ry5,rz5]), sup([rx5,ry5,rz5]));
        plotbox(r5_box, 'none','y','0.8');
        
        % Plot hand
        r7_box = create_box(3, inf([rx7,ry7,rz7]), sup([rx7,ry7,rz7]));
        plotbox(r7_box, 'none','y','0.8');
    end
    
    % Plot hand frame
    plot3(mid(tX),mid(tY),mid(tZ),'ko');
end

% Plot spherical polygons
phi_theta = importdata("../../examples/redundancy_tube/data/shoulder_swing.txt", '\t');
plot_spherical_polygon(phi_theta);


% Plotting
xlabel('x (cm)', 'interpreter', 'latex');
ylabel('y (cm)', 'interpreter', 'latex');
zlabel('z (cm)', 'interpreter', 'latex');
axis equal;


