alpha_ = linspace(-1.75,2.27,100);
beta_ = linspace(-1.40,1.40,100);
[alpha,beta] = meshgrid(alpha_,beta_);

EXT = pi + pi/180 .* (0.724559.*beta.^4+(11.73726.*alpha.*cos(beta)-1.535318).*beta.^3+(-5.683932.*alpha.^2.*cos(beta).^2+23.970675.*alpha.*cos(beta)-7.366772).*beta.^2+(-4.558201.*alpha.^3.*cos(beta).^3-2.565833.*alpha.^2.*cos(beta).^2+29.532220.*alpha.*cos(beta)-15.71531).*beta+.331.*alpha.^4.*cos(beta).^4-2.987.*alpha.^3.*cos(beta).^3+3.428.*alpha.^2.*cos(beta).^2+9.299.*alpha.*cos(beta)-2.459);
INT = pi + pi/180 .* (3.417002.*beta.^3+(20.882835.*alpha.*cos(beta)-2.676168).*beta.^2+(-.143589.*alpha.^2.*cos(beta).^2+29.671092.*alpha.*cos(beta)-41.687220).*beta-2.081.*alpha.^3.*cos(beta).^3+4.092.*alpha.^2.*cos(beta).^2+18.652.*alpha.*cos(beta)-139.27);

figure;
mesh(alpha,beta,EXT) %interpolated
hold on;
mesh(alpha,beta,INT) %interpolated

% Plotting
xlabel('Angle $\alpha$ (rad)', 'interpreter', 'latex');
ylabel('Angle $\beta$ (rad)', 'interpreter', 'latex');
zlabel('Swivel angle $\psi$ (rad)', 'interpreter', 'latex');
axis equal;

