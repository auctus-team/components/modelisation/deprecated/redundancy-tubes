close all;
clear;

eps = infsup(-0.001,0.001);
l_1 = 0.65 +eps;
l_2 = 1.11 +eps;
l_3 = 0.3 +eps;

xc = [0,0]';
qd = [infsup(-1,1),infsup(-1,1),infsup(-1,1)]';

q = [infsup(-pi,pi);infsup(-pi,pi)];
% q = [infsup(0.8,1);infsup(0.8,1)];

delta_q = diam(q);
count = 300;
res_q = delta_q/count;

vert = nan(8*count*count, 3);
face = nan(6*count*count, 4);

sphere_radius_list = nan(count*count,1);
width_cube_list = nan(count*count,1);
listindex = 0;
for q_1i=1:count
    for q_2i=1:count
        q_1 = infsup(delta_q(1)*(q_1i/count) + inf(q(1)) - res_q(1),...
            delta_q(1)*(q_1i/count) + inf(q(1)));
        q_2 = infsup(delta_q(2)*(q_2i/count) + inf(q(2)) - res_q(2),...
            delta_q(2)*(q_2i/count) + inf(q(2)));
        q_3 = infsup(0.99,1.01);
                
        Jq = [-sin(q_1+q_2+q_3)*l_3-sin(q_1+q_2)*l_2-sin(q_1)*l_1, -sin(q_1+q_2+q_3)*l_3-sin(q_1+q_2)*l_2, -sin(q_1+q_2+q_3)*l_3;
              cos(q_1+q_2+q_3)*l_3+cos(q_1+q_2)*l_2+cos(q_1)*l_1, cos(q_1+q_2+q_3)*l_3+cos(q_1+q_2)*l_2, cos(q_1+q_2+q_3)*l_3];

        [sphere_radius, width_cube] = linear_transform_inscribed_sphere(Jq, qd, xc, false);
        
        verti = [
            inf(q_1) inf(q_2) inf(q_3);
            sup(q_1) inf(q_2) inf(q_3);
            sup(q_1) sup(q_2) inf(q_3);
            inf(q_1) sup(q_2) inf(q_3);
            inf(q_1) inf(q_2) sup(q_3);
            sup(q_1) inf(q_2) sup(q_3);
            sup(q_1) sup(q_2) sup(q_3);
            inf(q_1) sup(q_2) sup(q_3)
            ];
        facei = [
            1 2 6 5;
            2 3 7 6;
            3 4 8 7;
            4 1 5 8;
            1 2 3 4;
            5 6 7 8] + listindex * 8;
          
        vert(listindex * 8 + 1 : listindex * 8 + 8, :) = verti;
        face(listindex * 6 + 1: listindex * 6 + 6, :) = facei;        

        sphere_radius_list(listindex+1) = sphere_radius;
        width_cube_list(listindex+1) = width_cube;        
        listindex = listindex + 1;
    end
end

% Plotting
figure;
cVect = [sphere_radius_list,sphere_radius_list,sphere_radius_list,sphere_radius_list,sphere_radius_list,sphere_radius_list];
cVect = reshape(cVect.',1,[])';
colormap(jet(100));
patch('Vertices',vert,'Faces',face,...
      'FaceVertexCData',cVect,'FaceColor','flat','EdgeColor','none');
hcbar = colorbar;
xlabel('${q}_1$','interpreter','latex','FontSize', 20);
ylabel('${q}_2$','interpreter','latex','FontSize', 20);
print(gcf,'./tolsetAx_twolinkmanip_vel_sphere.png','-dpng','-r300');
title('Inner approximation of inscribed sphere at origin');

figure;
cVect = [width_cube_list,width_cube_list,width_cube_list,width_cube_list,width_cube_list,width_cube_list];
cVect = reshape(cVect.',1,[])';
colormap(jet(100));
patch('Vertices',vert,'Faces',face,...
      'FaceVertexCData',cVect,'FaceColor','flat','EdgeColor','none');
hcbar = colorbar;
xlabel('${q}_1$','interpreter','latex','FontSize', 20);
ylabel('${q}_2$','interpreter','latex','FontSize', 20);
print(gcf,'./tolsetAx_twolinkmanip_vel_cube.png','-dpng','-r300');
title('Inner approximation of inscribed cube at origin');
