eps = infsup(-0.001,0.001);
l_1 = 0.65 +eps;
l_2 = 1.11 +eps;

xc = [0,0]';
qd = [infsup(-1,1),infsup(-1,1)]';

% q = [infsup(-pi,pi);infsup(-pi,pi)];
q = [infsup( 0.5890,    0.7854);infsup(0.1963,    0.3927)];

delta_q = diam(q);
count = 1;
res_q = delta_q/count;
box_list_q1 = nan(4,count*count);
box_list_q2 = nan(4,count*count);
sphere_radius_list = nan(count*count,1);
width_cube_list = nan(count*count,1);
listindex = 1;
for q_1i=1:count
    for q_2i=1:count
        q_1 = infsup(delta_q(1)*(q_1i/count) + inf(q(1)) - res_q(1),...
            delta_q(1)*(q_1i/count) + inf(q(1)));
        q_2 = infsup(delta_q(2)*(q_2i/count) + inf(q(2)) - res_q(2),...
            delta_q(2)*(q_2i/count) + inf(q(2)));
        
        Jq = [-l_1 *sin(q_1) - l_2 *sin(q_1 + q_2), -l_2*sin(q_1 + q_2);
                    l_1 *cos(q_1) + l_2 *cos(q_1 + q_2), l_2*cos(q_1 + q_2)];    

        [sphere_radius, width_cube] = linear_transform_inscribed_sphere(Jq, qd, xc, false);
    
        qbox = create_box(2,inf([q_1,q_2]),sup([q_1,q_2]));
        box_list_q1(:,listindex) = qbox(:,1);
        box_list_q2(:,listindex) = qbox(:,2);
        sphere_radius_list(listindex) = sphere_radius;
        width_cube_list(listindex) = width_cube;        
        listindex = listindex + 1;
    end
end

% Plotting
figure;
cVect = sphere_radius_list;
colormap(jet(100));
patch(box_list_q1,box_list_q2,cVect,'EdgeColor','none')
hcbar = colorbar;
xlabel('$\dot{q}_1$','interpreter','latex','FontSize', 20);
ylabel('$\dot{q}_2$','interpreter','latex','FontSize', 20);
print(gcf,'./tolsetAx_twolinkmanip_vel_sphere.png','-dpng','-r300');
title('Inner approximation of inscribed sphere at origin');

figure;
cVect = width_cube_list;
colormap(jet(100));
patch(box_list_q1,box_list_q2,cVect,'EdgeColor','none')
hcbar = colorbar;
xlabel('$\dot{q}_1$','interpreter','latex','FontSize', 20);
ylabel('$\dot{q}_2$','interpreter','latex','FontSize', 20);
print(gcf,'./tolsetAx_twolinkmanip_vel_cube.png','-dpng','-r300');
title('Inner approximation of inscribed cube at origin');
