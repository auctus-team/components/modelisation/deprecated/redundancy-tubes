function plotbox(vertex,color,edgecolor,alpha)
% PlotBox.m
% Coded by: Josh Pickard
% Date: March 6, 2014
%
% Purpose: plot a 2D or 3D colored box
%
% Modifications:

if size(vertex,2)==2
    if all(sum(abs(vertex(:,2)-vertex(1,2)))<0.1)
        scatter(vertex(1,1),vertex(1,2),'filled','MarkerFaceColor',color);
    else
        fill(vertex(1:4,1)',vertex(1:4,2)','k','FaceColor',color,'EdgeColor',edgecolor,'FaceAlpha',alpha);
    end
else
    if all(sum(abs(vertex(:,3)-vertex(1,3)))<0.1)
        scatter3(vertex(1,1),vertex(1,2),vertex(1,3),'filled','MarkerFaceColor',color);
    else
        vertices = vertex';
        % plot planes
        vertices_plotting = vertices(:,1:4); % top
        fill3(vertices_plotting(1,:),vertices_plotting(2,:),vertices_plotting(3,:),'k','FaceColor',color,'EdgeColor',edgecolor,'FaceAlpha',alpha);
        hold on;
        vertices_plotting = vertices(:,5:8); % bottom
        fill3(vertices_plotting(1,:),vertices_plotting(2,:),vertices_plotting(3,:),'k','FaceColor',color,'EdgeColor',edgecolor,'FaceAlpha',alpha);
        vertices_plotting = vertices(:,[1,2,7,8]); % front
        fill3(vertices_plotting(1,:),vertices_plotting(2,:),vertices_plotting(3,:),'k','FaceColor',color,'EdgeColor',edgecolor,'FaceAlpha',alpha);
        vertices_plotting = vertices(:,[3,4,5,6]); % back
        fill3(vertices_plotting(1,:),vertices_plotting(2,:),vertices_plotting(3,:),'k','FaceColor',color,'EdgeColor',edgecolor,'FaceAlpha',alpha);
        vertices_plotting = vertices(:,[1,4,5,8]); % left
        fill3(vertices_plotting(1,:),vertices_plotting(2,:),vertices_plotting(3,:),'k','FaceColor',color,'EdgeColor',edgecolor,'FaceAlpha',alpha);
        vertices_plotting = vertices(:,[2,3,6,7]); % right
        fill3(vertices_plotting(1,:),vertices_plotting(2,:),vertices_plotting(3,:),'k','FaceColor',color,'EdgeColor',edgecolor,'FaceAlpha',alpha);
    end
    
end