function plot_spherical_polygon(phi_theta)
    % Definition of each polgon segment vertices on the sphere (arbitary for
    % testing purposes, will need experiments)
    phiswing  = phi_theta(:,1)';
    thetaswing = phi_theta(:,2)';
    npoint      = ones(length(thetaswing)-1,1)*75;

    %swingxz twisty
    vqrel = [];
    vthetaswing = [];
    vu = [];
    phi = [];
    for i = 1:length(npoint)
        u1 = [sin(phiswing(i)), 0, cos(phiswing(i))]';
        q1 = quatexp(u1*thetaswing(i)/2);

        u2 = [sin(phiswing(i+1)), 0, cos(phiswing(i+1))]';
        q2 = quatexp(u2*thetaswing(i+1)/2);
        t = (0:npoint(i))/npoint(i);

        for j = 1:npoint(i)
            qrel = slerp(q1, q2, t(j));
            vqrel = [vqrel, qrel];

            [u, the]    = quat2axisangle(qrel);
            vthetaswing = [vthetaswing, the];
            vu          = [vu, u];
            phi         = [phi, atan2(u(1), u(3))];
        end

    end
    phi = unwrap(phi);

    %% graphic representation of joint boundaries with polygons
    a          = [0,0,-1,0]'; % y axis
    n          = length(phi);
    aprime     = zeros(4, n);
    for i = 1:n
        swingaxis    = [sin(phi(i)), 0, cos(phi(i))];
        q            = axisangle2quat(swingaxis, vthetaswing(i));
        aprime(:,i)  = quatprod(q, quatprod(a, quatconj(q)));
    end

    plot3(aprime(2,:), aprime(3,:), aprime(4,:), 'co', 'Linewidth', 5); hold on;
    [X,Y,Z] = sphere(30);
    h = surf(X,Y,Z);
    set(h, 'Facealpha', 0.1, 'LineStyle', '-', 'LineWidth', 1);
    axis equal
    grid on
    xlabel('x');
    ylabel('y');
    zlabel('z');