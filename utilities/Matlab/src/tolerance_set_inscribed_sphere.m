function [radius,width_cube, x_vertices] = tolerance_set_inscribed_sphere(A, b, xc, showplot)
    %%
    % radius = tolerance_set_inscribed_sphere(A, b, xc, showplot)
    %
    % Joshua K. Pickard
    % November 4, 2019
    %
    % Identifies an inner approximation for the largest sphere centered at 
    % xc contained inside the tolerance solution set:
    % tolset = {x | forall A in [A], exists b in [b], Ax=b}
    % using the n-sphere method of Pickard et al. Applicable to square, 
    % underdetermined and overdetermined interval linear systems. 
    %
    % Requires INTLAB (http://www.ti3.tu-harburg.de/rump/intlab/)
    %
    % params:
    % A - matrix or interval matrix
    % b - interval vector
    % xc - center of sphere 
    % showplot - flag to enable plotting (not supported when A is an interval matrix)
    %
    % output:
    % radius - the radius of the inscribed sphere centered at xc
    %
    % To run:
    % Overdetermined systems:
    % r = tolerance_set_inscribed_sphere(intval(rand(7,3)), infsup(-100*rand(7,1),100*rand(7,1)),[0,0,0]',false)
    % r = tolerance_set_inscribed_sphere(rand(7,3), infsup(-100*rand(7,1),100*rand(7,1)),[0,0,0]',true)
    % r = tolerance_set_inscribed_sphere(rand(3,2), infsup(-100*rand(3,1),100*rand(3,1)),[0,0]',true)
    % Underdetermined systems:
    % r = tolerance_set_inscribed_sphere((rand(2,3)), infsup(-10*ones(2,1),10*ones(2,1)),[0,0,0]',true)
    % r = tolerance_set_inscribed_sphere((rand(1,2)), infsup(-10*ones(1,1),10*ones(1,1)),[0,0]',true)
    % Square systems:
    % r = tolerance_set_inscribed_sphere((rand(3,3)), infsup(-100*ones(3,1),100*ones(3,1)),[0,0,0]',true)
    % r = tolerance_set_inscribed_sphere((rand(2,2)), infsup(-100*ones(2,1),100*ones(2,1)),[0,0]',true)
    %
    % r = tolerance_set_inscribed_sphere([0.8947   0.6707   0.2409; 0.3348   0.3899   0.6958]',[infsup(-74,95),infsup(-24,20),infsup(-22,33)]',[0,0]',true)
    %
    % Modifications:
    % - Joshua K. Pickard, November 6, 2019 : Add plotting for
    % underdetermined systems.
    
    %%
    % Obtain maximum inscribed n-sphere centered at xc using method of Pickard
    %
    b_min = inf(b);
    b_max = sup(b);
    b_rad = rad(b);
    b_mid = mid(b); 

    % Check if xc is a tolerance solution 
    % (J. Rohn, Solvability of systems of interval linear equations and 
    % inequalities, Springer US, Boston, MA, 2006, pp. 35-77 (2006).) 
    if isa(A,'intval')
        if any(abs(mid(A)*xc - b_mid) > -rad(A)*abs(xc) + b_rad)
            error("Error: xc not a tolerance solution");
        end
    else
        if any(abs(A*xc - b_mid) > -A*abs(xc) + b_rad)
            error("Error: xc not a tolerance solution");
        end
    end

    % Compute radius of inscribed sphere centered at xc
    r_list = nan(size(A,1),1);        
    for i=1:size(A,1)
        r_sphere = min((A(i,:)*xc - b_min(i)) / norm((A(i,:)),2), (-A(i,:)*xc + b_max(i)) / norm((A(i,:)),2));
        if isa(A,'intval')
            r_list(i) = inf(r_sphere);
        else
            r_list(i) = r_sphere;
        end
    end
    radius = max(0,min(r_list));
    
    % Compute half-width of inscribed cube centered at xc
    width_list = nan(size(A,1),1);
    for i=1:size(A,1)
        w_cube = min((b_rad(i) - abs(b_mid(i) - A(i,:)*xc))/ norm((A(i,:)),1));
        if isa(A,'intval')
            width_list(i) = inf(w_cube);
        else
            width_list(i) = w_cube;
        end
    end
    width_cube = max(0,min(width_list));

    %%
    % Obtain the polytope for a square or overdetermined system 
    % (Chiacchio, P. , Bouffard‐Vercelli, Y. and Pierrot, F. (1997), Force 
    % polytope and force ellipsoid for redundant manipulators. J. Robotic 
    % Syst., 14: 613-620)   
%     close all;        
    b_vertices = [];
    x_vertices = [];
    A_mid = mid(A);
    m = size(A_mid,2);
    n = size(A_mid,1);   
    
    if isa(A,'intval') && showplot
        figure;        
        if m==3
            % Consider all vertex combinations of A
            for row=1:size(A,1)
                a_ = A(row,1);
                b_ = A(row,2);
                c_ = A(row,3);
                d1 = b_min(row);
                d2 = b_max(row);
                z1 = @(x,y) (d1 - inf(a_).*x - inf(b_).*y)/inf(c_); 
                z2 = @(x,y) (d2 - inf(a_).*x - inf(b_).*y)/inf(c_); 
                z3 = @(x,y) (d1 - sup(a_).*x - sup(b_).*y)/sup(c_); 
                z4 = @(x,y) (d2 - sup(a_).*x - sup(b_).*y)/sup(c_); 
                fsurf(z1);     
                hold on;
                fsurf(z2);
                fsurf(z3);
                fsurf(z4);
            end
            [x,y,z] = sphere;
            x = x*radius + xc(1);
            y = y*radius + xc(2);
            z = z*radius + xc(3);
            surf(x,y,z, 'Facecolor','g');
        elseif m==2
            for row=1:size(A,1)
                a_ = A(row,1);
                b_ = A(row,2);
                d1 = b_min(row);
                d2 = b_max(row);
                y1_1 = @(x) (d1 - inf(a_).*x)/inf(b_); 
                y1_2 = @(x) (d1 - sup(a_).*x)/sup(b_); 
                y2_1 = @(x) (d2 - inf(a_).*x)/inf(b_); 
                y2_2 = @(x) (d2 - sup(a_).*x)/sup(b_); 
                fplot(y1_1,'b');
                hold on;
                fplot(y1_2,'b');
                fplot(y2_1,'b');
                fplot(y2_2,'b');
            end
            th = 0:pi/50:2*pi;
            x = radius * cos(th) + xc(1);
            y = radius * sin(th) + xc(2);
            plot(x,y, 'g');  
        end
        title('x plot');
        xlabel('x');
        ylabel('y');
        zlabel('z');
        axis equal;
    elseif isa(A,'double') && showplot   
        if n>=m
            % Compute SVD of mid(A)
            [U,~,~] = svd(A_mid);

            % Get column space from A_mid (first r columns)
            r = rank(A_mid);
            U1 = U(:,1:r);

            % Create matrix A_mid and vector b
            A_mid_ = zeros(2*n, m);
            b_  = zeros(2*n, 1);
            for i=1:m
                for j=1:n
                    if b_max(j) >= 0
                        A_mid_(j,i) = U1(j,i);
                    else
                        A_mid_(j,i) = -U1(j,i);
                    end
                    b_(j) = abs(b_max(j));

                    if b_min(j) >= 0
                        A_mid_(j+n,i) = U1(j,i);
                    else
                        A_mid_(j+n,i) = -U1(j,i);
                    end
                    b_(j+n) = abs(b_min(j));
                end
            end
            A_mid__ = [A_mid_, eye(2*n, 2*n)];

            % Remove all combinations of m columns from A_mid to get (2n x 2n) matrix A_mid_reduced
            col_combs = nchoosek(m+1:m+2*n, 2*n-m);            
            for comb=1:size(col_combs,1)
                col_comb = col_combs(comb,:);

                % Get A_mid_reduced
                A_mid_reduced = A_mid__(:,[1:m,col_comb]);

                % Check rank of A_mid_reduced
                full_rank = rank(A_mid_reduced)==2*n;

                if full_rank
                    % Solve for x_reduced
                    x_reduced = inv(A_mid_reduced) * b_;

                    % Check if m+1:2n are positive
                    positive_slack_vars = all(x_reduced(m+1:2*n)>=0);

                    if positive_slack_vars            
                        % Compute torque vertex
                        b_vertex = U1 * x_reduced(1:m);                        
                        b_vertices = [b_vertices,b_vertex];

                        try 
                            A_mid_inv = inv(A_mid);
                        catch
                            A_mid_inv = inv(A_mid' * A_mid) * A_mid';
                        end

                        % Compute force vertex
                        x_vertex = A_mid_inv * b_vertex;
                        x_vertices = [x_vertices,x_vertex];
                    end
                end
            end
        end
        
        % Plotting x
        figure;
        if n>=m
            if size(x_vertices,1)==3
                % Compute convex hull of x_vertices ('QJ' avoids errors)
                k_x_vertices = convhulln(x_vertices(:,:)', {'QJ','QR0'});
                trisurf(k_x_vertices,x_vertices(1,:), x_vertices(2,:), x_vertices(3,:),'Edgecolor','b', 'FaceColor','c','FaceAlpha',0.1);
                hold on;
                [x,y,z] = sphere;
                x = x*radius + xc(1);
                y = y*radius + xc(2);
                z = z*radius + xc(3);
                surf(x,y,z, 'Facecolor','g');
            elseif size(x_vertices,1)==2
                % Compute convex hull of x_vertices
                k_x_vertices = boundary(x_vertices(1,:)', x_vertices(2,:)',0);
                fill(x_vertices(1,k_x_vertices), x_vertices(2,k_x_vertices), 'b');
                alpha(0.3);
                hold on;
                th = 0:pi/50:2*pi;
                x = radius * cos(th) + xc(1);
                y = radius * sin(th) + xc(2);
                fill(x,y, 'g');  
                alpha(0.3);
                
                XY = [-width_cube+xc(1),width_cube+xc(2);
                    -width_cube+xc(1),width_cube+xc(2);
                    width_cube+xc(1),width_cube+xc(2);
                    width_cube+xc(1),-width_cube+xc(2);
                    -width_cube+xc(1),-width_cube+xc(2)];
                
                fill(XY(:,1),XY(:,2),'m');
                alpha(0.6);
            else
                warning("Can only plot v for dimensions 2 or 3");
            end
        else
            if m==3
                for row=1:size(A_mid,1)
                    a_ = A_mid(row,1);
                    b_ = A_mid(row,2);
                    c_ = A_mid(row,3);
                    d1 = b_min(row);
                    d2 = b_max(row);
                    z1 = @(x,y) (d1 - a_.*x - b_.*y)/c_; 
                    z2 = @(x,y) (d2 - a_.*x - b_.*y)/c_; 
                    fsurf(z1);
                    hold on;
                    fsurf(z2);
                end
                [x,y,z] = sphere;
                x = x*radius + xc(1);
                y = y*radius + xc(2);
                z = z*radius + xc(3);
                surf(x,y,z, 'Facecolor','g');                
            elseif m==2
                for row=1:size(A_mid,1)
                    a_ = A_mid(row,1);
                    b_ = A_mid(row,2);
                    d1 = b_min(row);
                    d2 = b_max(row);
                    y1 = @(x) (d1 - a_.*x)/b_; 
                    y2 = @(x) (d2 - a_.*x)/b_; 
                    fplot(y1,'b');
                    hold on;
                    fplot(y2,'b');
                end
                th = 0:pi/50:2*pi;
                x = radius * cos(th) + xc(1);
                y = radius * sin(th) + xc(2);
                plot(x,y, 'g');   
            end
        end
        title('x plot');
        xlabel('x');
        ylabel('y');
        zlabel('z');
        axis equal;
        
        % Plotting b
        figure;
        if size(b_vertices,1)==3
            box_b = create_box(size(b,1),inf(b),sup(b));
            plotbox(box_b,'r','k',0.3);
            hold on;
            % Compute convex hull of b_vertices ('QJ' avoids errors)
            if size(x_vertices,1)==3
                k_b_vertices = convhulln(b_vertices(:,:)', {'QJ','QR0'});
                trisurf(k_b_vertices,b_vertices(1,:), b_vertices(2,:), b_vertices(3,:),'Edgecolor','b', 'FaceColor','none','FaceAlpha',0.1);
            elseif size(x_vertices,1)==2
                % Order based on closest points
                b_vertices_ordered = [b_vertices(:,1)];
                b_vertices_list = b_vertices(:,2:end);
                while size(b_vertices_list,2)>0
                    [V_start,I_start] = min(vecnorm(b_vertices_list - b_vertices_ordered(:,1),2));
                    [V_end,I_end] = min(vecnorm(b_vertices_list - b_vertices_ordered(:,end),2));
                    if V_end <= V_start
                        b_vertices_ordered = [b_vertices_ordered,b_vertices_list(:,I_end)];
                        b_vertices_list(:,I_end) = [];
                    else
                        b_vertices_ordered = [b_vertices_list(:,I_start),b_vertices_ordered];
                        b_vertices_list(:,I_start) = [];
                    end
                end
                b_vertices_ordered = [b_vertices_ordered,b_vertices_ordered(:,1)];
                plot3(b_vertices_ordered(1,:), b_vertices_ordered(2,:),b_vertices_ordered(3,:), 'b');
            end
        elseif size(b_vertices,1)==2
            box_b = create_box(size(b,1),inf(b),sup(b));
            plotbox(box_b,'r','k',0.3);
            hold on;
            % Compute convex hull of x_vertices
            k_b_vertices = boundary(b_vertices(1,:)', b_vertices(2,:)',0);
            plot(b_vertices(1,k_b_vertices), b_vertices(2,k_b_vertices), 'b');
        else
            warning("Can only plot b for dimensions 2 or 3");
        end
        title('b plot');
        xlabel('x');
        ylabel('y');
        zlabel('z');
        axis equal;
    end
