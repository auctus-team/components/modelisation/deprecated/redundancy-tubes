close all;
clear;

eps = infsup(-0.001,0.001);
l_1 = 0.65 +eps;
l_2 = 1.11 +eps;
l_3 = 0.3 +eps;

xc = [0,0]';
b = [infsup(-1,1),infsup(-1,1),infsup(-1,1)]';

q = [infsup(-pi,pi);infsup(-pi,pi)];
% q = [infsup(-1,1);infsup(-1,1)];

delta_q = diam(q);
count = 100;
res_q = delta_q/count;

vert = nan(8*count*count, 3);
face = nan(6*count*count, 4);
sphere_radius_list = nan(count*count,1);
listindex = 0;
normA2 = nan(3,1)*intval(nan);
for q_1i=1:count
    for q_2i=1:count
        q_1 = infsup(delta_q(1)*(q_1i/count) + inf(q(1)) - res_q(1),...
            delta_q(1)*(q_1i/count) + inf(q(1)));
        q_2 = infsup(delta_q(2)*(q_2i/count) + inf(q(2)) - res_q(2),...
            delta_q(2)*(q_2i/count) + inf(q(2)));
        q_3 = infsup(0.99,1.01);
        
        q12 = q_1+q_2;
        sq12 = sin(q12);
        cq12 = cos(q12);
        q123 = q_1+q_2+q_3;
        sq123 = sin(q123);
        cq123 = cos(q123);
                
        A = [-sq123*l_3-sq12*l_2-sin(q_1)*l_1, -sq123*l_3-sq12*l_2, -sq123*l_3;
              cq123*l_3+cq12*l_2+cos(q_1)*l_1, cq123*l_3+cq12*l_2, cq123*l_3]';
        
        %%
        % Obtain maximum inscribed n-sphere centered at xc using method of Pickard
        %
        for i=1:size(A,1)
            normA2(i) = norm(A(i,:),2);
        end
        Axc = A*xc;
        r_list = inf(min((Axc - inf(b)) ./ normA2, (-Axc + sup(b)) ./ normA2));
        sphere_radius = max(0,min(r_list));
          
        vert(listindex * 8 + 1 : listindex * 8 + 8, :) = [
            inf(q_1) inf(q_2) inf(q_3);
            sup(q_1) inf(q_2) inf(q_3);
            sup(q_1) sup(q_2) inf(q_3);
            inf(q_1) sup(q_2) inf(q_3);
            inf(q_1) inf(q_2) sup(q_3);
            sup(q_1) inf(q_2) sup(q_3);
            sup(q_1) sup(q_2) sup(q_3);
            inf(q_1) sup(q_2) sup(q_3)
            ];
        face(listindex * 6 + 1: listindex * 6 + 6, :) = [
            1 2 6 5;
            2 3 7 6;
            3 4 8 7;
            4 1 5 8;
            1 2 3 4;
            5 6 7 8] + listindex * 8;       

        sphere_radius_list(listindex+1) = sphere_radius;  
        listindex = listindex + 1;
    end
end

% Plotting
figure;
cVect = [sphere_radius_list,sphere_radius_list,sphere_radius_list,sphere_radius_list,sphere_radius_list,sphere_radius_list];
cVect = reshape(cVect.',1,[])';
colormap(jet(100));
patch('Vertices',vert,'Faces',face,...
      'FaceVertexCData',cVect,'FaceColor','flat','EdgeColor','none');
hcbar = colorbar;
xlabel('${q}_1$','interpreter','latex','FontSize', 20);
ylabel('${q}_2$','interpreter','latex','FontSize', 20);
print(gcf,'./tolsetAx_twolinkmanip_force_sphere.png','-dpng','-r300');
title('Inner approximation of inscribed sphere at origin');
