eps = infsup(-0.001,0.001);
l_1 = 0.65 +eps;
l_2 = 1.11 +eps;

xc = [0,0]';
qd = [infsup(-1,1),infsup(-1,1)]';

q = [infsup(-pi,pi);infsup(-pi,pi)];

q_list = {q};
box_list_q1 = nan(4,count*count);
box_list_q2 = nan(4,count*count);
sphere_radius_list = nan(count*count,1);
width_cube_list = nan(count*count,1);

listindex = 1;
while ~isempty(q_list)
    q = q_list{1};
    q_list(1) = [];
    
    q_1 = q(1);
    q_2 = q(2);
        
    Jq = [-l_1 *sin(q_1) - l_2 *sin(q_1 + q_2), -l_2*sin(q_1 + q_2);
                l_1 *cos(q_1) + l_2 *cos(q_1 + q_2), l_2*cos(q_1 + q_2)];    

    [sphere_radius, width_cube] = linear_transform_inscribed_sphere(Jq, qd, xc, false);

    if sphere_radius>=0.1
        qbox = create_box(2,inf([q_1,q_2]),sup([q_1,q_2]));
        box_list_q1(:,listindex) = qbox(:,1);
        box_list_q2(:,listindex) = qbox(:,2);
        sphere_radius_list(listindex) = 1;
        width_cube_list(listindex) = 1;        
        listindex = listindex + 1;
    else
        if max(rad(q)) >= 0.05
            [~,I] = max(rad(q));
            q_b1 = q;
            q_b2 = q;         
            q_b1(I) = infsup(inf(q(I)),mid(q(I)));
            q_b2(I) = infsup(mid(q(I)),sup(q(I)));           
            
            q_list{end+1} = q_b1;
            q_list{end+1} = q_b2;
        else
            qbox = create_box(2,inf([q_1,q_2]),sup([q_1,q_2]));
            box_list_q1(:,listindex) = qbox(:,1);
            box_list_q2(:,listindex) = qbox(:,2);
            sphere_radius_list(listindex) = 0;
            width_cube_list(listindex) = 0;  
            listindex = listindex + 1;
%             disp([infsup([q_1,q_2]),'too small']);
        end
    end
end

% Plotting
figure;
cVect = sphere_radius_list;
colormap(jet(100));
patch(box_list_q1,box_list_q2,cVect,'EdgeColor','none')
% hcbar = colorbar;
xlabel('$\dot{q}_1$','interpreter','latex','FontSize', 20);
ylabel('$\dot{q}_2$','interpreter','latex','FontSize', 20);
print(gcf,'./tolsetAx_twolinkmanip_vel_sphere.png','-dpng','-r300');
title('Inner approximation of inscribed sphere at origin');
