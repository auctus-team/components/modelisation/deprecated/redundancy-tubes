% create_box.m
% Created by: Josh Pickard
% Date: July 17, 2013
%
% Purpose: Used to create a box from a set of lower and upper bounds
%

function V = create_box(dim,lb,ub)
    Vnum = 2^dim;
    V = zeros(Vnum, dim);
    for i=0:Vnum-1
        Bin=dec2bin(i,dim);
        Gray=bin2gray(Bin);
        V(i+1,:)=Gray-'0';
        for j=1:dim
            if V(i+1,j)==0
                V(i+1,j)=lb(j);
            else
                 V(i+1,j)=ub(j);
            end
        end
    end
end