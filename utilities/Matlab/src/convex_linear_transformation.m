% % underdetermined system
% x = infsup(-1*ones(2,1),1*ones(2,1));
% A = [
%     0.8947    0.6707    0.2409
%     0.3348    0.3899    0.6958]';
% overdetermined system
x = infsup(-1*ones(3,1),1*ones(3,1));
A = [
    0.8947    0.6707    0.2409
    0.3348    0.3899    0.6958];

rank(A)

A_int = intval(A)+infsup(-0.01,0.01);
box_x = create_box(size(x,1),inf(x),sup(x));
b_vertices = A * box_x';
b_int_vertices = A_int * box_x';

% plot x
close all;
figure;
plotbox(box_x,'r','k',0.3);
hold on;
if length(x)==3
    plot3(box_x(:,1),box_x(:,2),box_x(:,3),'bo');
elseif length(x)==2
    plot(box_x(:,1),box_x(:,2),'bo');
end
xlabel('$\Delta{q}_1$','interpreter','latex','FontSize', 20);
ylabel('$\Delta{q}_2$','interpreter','latex','FontSize', 20);
zlabel('$\Delta{q}_3$','interpreter','latex','FontSize', 20);
axis equal;

% plot b_vertices
figure;
if size(A,1)==2
    k_b_vertices = boundary(b_vertices(1,:)', b_vertices(2,:)',0);
%     fill(b_vertices(1,k_b_vertices), b_vertices(2,k_b_vertices), 'b','Edgecolor','k');
    plot(b_vertices(1,k_b_vertices), b_vertices(2,k_b_vertices), 'k');
    alpha(0.3);
    hold on;
    plot(b_vertices(1,:),b_vertices(2,:),'bo');
elseif size(A,1)==3
    if length(x)==3
        faces = [
            1,2,3,4;
            1,4,5,8;
            3,4,5,6;
            1,2,7,8;
            2,3,6,7;
            5,6,7,8];
    elseif length(x)==2
        faces = [
            1,2,3,4];
    end
    for face=1:size(faces,1)
        b_vertices(1,faces(face,:))
        fill3(b_vertices(1,faces(face,:)),b_vertices(2,faces(face,:)),b_vertices(3,faces(face,:)),'b','Edgecolor','k', 'FaceAlpha',0.3);
        hold on;
    end
    plot3(b_vertices(1,:),b_vertices(2,:),b_vertices(3,:),'bo');
end
xlabel('$\Delta{x}$','interpreter','latex','FontSize', 20);
ylabel('$\Delta{y}$','interpreter','latex','FontSize', 20);
zlabel('$\Delta{z}$','interpreter','latex','FontSize', 20);
axis equal;
ha = annotation('textbox',[0.5 0.5 0.4 0.2], 'Interpreter', 'latex', 'LineStyle', 'none');
% cs = '$\Sigma_{\forall\exists}\left({\bf A}, [{\bf x}]\right)$';
cs = '$\Sigma_{\forall\exists}\left(\mathrm{mid}([{\bf A}]), [{\bf x}]\right)$';
set(ha, 'String', cs,'FontSize', 20);

r_list = [];
for i=1:length(x)
    num = (rad(x) - abs(pinv(mid(A_int))) * rad(A_int) * mag(x));
    r_list(i) = (num(i)) / (rad(x(i)));
end
r = min(r_list);
y = mid(x) + r * infsup(-1,1)*ones(length(x),1);
box_y = create_box(size(y,1),inf(y),sup(y));
b_vertices = mid(A) * box_y';

% plot b_vertices
% figure;
if size(A,1)==2
    k_b_vertices = boundary(b_vertices(1,:)', b_vertices(2,:)',0);
    fill(b_vertices(1,k_b_vertices), b_vertices(2,k_b_vertices), 'b');
    alpha(0.3);
    hold on;
    plot(b_vertices(1,:),b_vertices(2,:),'bo');
elseif size(A,1)==3
    if length(x)==3
        faces = [
            1,2,3,4;
            1,4,5,8;
            3,4,5,6;
            1,2,7,8;
            2,3,6,7;
            5,6,7,8];
    elseif length(x)==2
        faces = [
            1,2,3,4];
    end
    for face=1:size(faces,1)
        b_vertices(1,faces(face,:))
        fill3(b_vertices(1,faces(face,:)),b_vertices(2,faces(face,:)),b_vertices(3,faces(face,:)),'b','Edgecolor','k', 'FaceAlpha',0.3);
        hold on;
    end
    plot3(b_vertices(1,:),b_vertices(2,:),b_vertices(3,:),'bo');
end
xlabel('$\Delta{x}$','interpreter','latex','FontSize', 20);
ylabel('$\Delta{y}$','interpreter','latex','FontSize', 20);
zlabel('$\Delta{z}$','interpreter','latex','FontSize', 20);
axis equal;
ha = annotation('textbox',[0.5 0.5 0.4 0.2], 'Interpreter', 'latex', 'LineStyle', 'none');
cs = '$\Sigma_{\forall\exists}\left([{\bf A}], [{\bf x}]\right)$';
set(ha, 'String', cs,'FontSize', 20);

% % plot b_int_vertices
% figure;
% if size(A,1)==2
%     Vnum = 2^size(A,1);
%     b_int_vert = nan(size(b_int_vertices));
%     PG_intersect = [];
%     for i=0:Vnum-1
%         Bin=dec2bin(i,size(A,1));
%         for j=1:size(A,1)
%             if Bin(j)=='0'
%                 b_int_vert(j,:)=inf(b_int_vertices(j,:));
%             else
%                 b_int_vert(j,:)=sup(b_int_vertices(j,:));
%             end
%         end
%         k_b_vertices = boundary(b_int_vert(1,:)', b_int_vert(2,:)',0);
%         PG = polyshape(b_int_vert(1,k_b_vertices), b_int_vert(2,k_b_vertices));
%         if isempty(PG_intersect)
%            PG_intersect = PG;
%         else
%             PG_intersect = intersect(PG_intersect,PG);
%         end
%         plot(b_int_vert(1,k_b_vertices), b_int_vert(2,k_b_vertices), 'k');
%         hold on;
%         plot(b_int_vert(1,:),b_int_vert(2,:),'bo');
%     end
%     plot(PG_intersect,'Facecolor','b');
%     alpha(0.3);
% elseif size(A,1)==3
%     Vnum = 2^size(A,1);
%     b_int_vert = nan(size(b_int_vertices));
%     PG_intersect = [];
%     for i=0:Vnum-1
%         Bin=dec2bin(i,size(A,1));
%         for j=1:size(A,1)
%             if Bin(j)=='0'
%                 b_int_vert(j,:)=inf(b_int_vertices(j,:));
%             else
%                 b_int_vert(j,:)=sup(b_int_vertices(j,:));
%             end
%         end
%         k_b_vertices = boundary(b_int_vert(1,:)', b_int_vert(2,:)', b_int_vert(3,:)',0);
%         PG = polyshape(b_int_vert(1,k_b_vertices), b_int_vert(2,k_b_vertices), b_int_vert(3,k_b_vertices));
%         if isempty(PG_intersect)
%            PG_intersect = PG;
%         else
%             PG_intersect = intersect(PG_intersect,PG);
%         end
%         plot3(b_int_vert(1,k_b_vertices), b_int_vert(2,k_b_vertices), b_int_vert(3,k_b_vertices), 'k');
%         hold on;
%         plot3(b_int_vert(1,:),b_int_vert(2,:),b_int_vert(3,:),'bo');
%     end
%     plot(PG_intersect,'Facecolor','b');
%     alpha(0.3);
% end
% xlabel('$\Delta{x}$','interpreter','latex','FontSize', 20);
% ylabel('$\Delta{y}$','interpreter','latex','FontSize', 20);
% zlabel('$\Delta{z}$','interpreter','latex','FontSize', 20);
% axis equal;