close all;

% A = [infsup(1,2), infsup(-2/3,1/2); infsup(-2/3,1/2), infsup(1,2)];
% b = [infsup(-1,1);infsup(-1,1)];

A = [0.8947   0.6707   0.2409; 0.3348   0.3899   0.6958]'+infsup(-0.01,0.01);
b = [infsup(-74,95),infsup(-24,20),infsup(-22,33)]';
% r = tolerance_set_inscribed_sphere(mid(A),b,[0,0]',true);

zeroA = zeros(size(A));
Ain = [sup(A) , -inf(A);
       -inf(A) , sup(A);
       -eye(2*size(A,2),2*size(A,2))
];
bin = [sup(b);
       -inf(b);
       zeros(2*size(A,2),1)
];

vert = con2vert(Ain, bin, [0.1,0.1,0.1,0.1]'); 
x = vert(:,1:2) - vert(:,3:4);

xc = [0,0]';
[radius,width_cube,~] = tolerance_set_inscribed_sphere(A,b,[0,0]',false);
[~,~,x_vertices] = tolerance_set_inscribed_sphere(mid(A),b,[0,0]',true);
close all;

figure;
% plot(x(:,1),x(:,2),'bo');
hold on;
k = boundary(x(:,1),x(:,2),0);
fill(x(k,1), x(k,2), 'k', 'FaceColor','b','FaceAlpha',0.3);
axis equal;

k_x_vertices = boundary(x_vertices(1,:)', x_vertices(2,:)',0);
fill(x_vertices(1,k_x_vertices), x_vertices(2,k_x_vertices), 'k', 'FaceColor','none');

th = 0:pi/50:2*pi;
x = radius * cos(th) + xc(1);
y = radius * sin(th) + xc(2);
fill(x,y, 'g'); 
alpha(0.3);

XY = [-width_cube+xc(1),width_cube+xc(2);
                    -width_cube+xc(1),width_cube+xc(2);
                    width_cube+xc(1),width_cube+xc(2);
                    width_cube+xc(1),-width_cube+xc(2);
                    -width_cube+xc(1),-width_cube+xc(2)];                
fill(XY(:,1),XY(:,2),'m');
alpha(0.3);
