% Add src files to path
addpath('../src/');

%% Plot force enclosures
Fenc = load('/tmp/force_enclosures.txt');

close all;
figure;
hold on;
% for index=1:length(Fenc)     
%     % get force enclosure
%     f_i = Fenc(index,:);
%     f_box = create_box(3,f_i([1,3,5]),f_i([2,4,6]));
%     plotbox(f_box,'g','k',0.5); 
% end

%% Plot common force capabilities
V = load('/tmp/common_force_capabilities.txt');

K = convhulln(V, {'QJ', 'Pp'});
trisurf(K,V(:,1),V(:,2),V(:,3),'Facecolor','g', 'Edgecolor','k','FaceAlpha',0.5); 

radii = [];
for index=1:size(K,1)
    % get facet points
    facet_points = V([K(index,:)],:); 

    % determine hyperplane equation
    p1 = facet_points(1,:);
    p2 = facet_points(2,:);
    p3 = facet_points(3,:);
    normal = cross(p1 - p2, p1 - p3);
    normal = normal / norm(normal);
    d = abs(p1(1)*normal(1) + p1(2)*normal(2) + p1(3)*normal(3));
    radii = [radii, d];
end
isotropic_f = min(radii);
[x,y,z] = sphere;
x = x*isotropic_f;
y = y*isotropic_f;
z = z*isotropic_f;
surf(x,y,z, 'Facecolor','b');

disp(["Common maximum isotropic force:", isotropic_f]);
xlabel('$f_x$ (N)','interpreter','latex')
ylabel('$f_y$ (N)','interpreter','latex')
zlabel('$f_z$ (N)','interpreter','latex')
view(12.8159,28.9808);
axis equal;