# Continous integration
[![pipeline status](https://gitlab.inria.fr/auctus/redundancy-tubes/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/redundancy-tubes/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus:redundancy-tubes)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:redundancy-tubes)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:redundancy-tubes&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:redundancy-tubes)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:redundancy-tubes&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:redundancy-tubes)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:redundancy-tubes&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:redundancy-tubes)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:redundancy-tubes&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:redundancy-tubes)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:redundancy-tubes&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:redundancy-tubes)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:redundancy-tubes&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:redundancy-tubes)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Aredundancy-tubes
- Documentation : https://auctus.gitlabpages.inria.fr/redundancy-tubes

# Informations
redundancy-tubes